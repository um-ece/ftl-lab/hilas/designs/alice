// SPDX-FileCopyrightText: 2020 Efabless Corporation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// SPDX-License-Identifier: Apache-2.0

`default_nettype none
/*
 *-------------------------------------------------------------
 *
 * user_analog_project_wrapper
 *
 * This wrapper enumerates all of the pins available to the
 * user for the user analog project.
 *
 *-------------------------------------------------------------
 */

module user_analog_project_wrapper (
`ifdef USE_POWER_PINS
    inout vdda1,	// User area 1 3.3V supply
    inout vdda2,	// User area 2 3.3V supply
    inout vssa1,	// User area 1 analog ground
    inout vssa2,	// User area 2 analog ground
    inout vccd1,	// User area 1 1.8V supply
    inout vccd2,	// User area 2 1.8v supply
    inout vssd1,	// User area 1 digital ground
    inout vssd2,	// User area 2 digital ground
`endif

    // Wishbone Slave ports (WB MI A)
    input wb_clk_i,
    input wb_rst_i,
    input wbs_stb_i,
    input wbs_cyc_i,
    input wbs_we_i,
    input [3:0] wbs_sel_i,
    input [31:0] wbs_dat_i,
    input [31:0] wbs_adr_i,
    output wbs_ack_o,
    output [31:0] wbs_dat_o,

    // Logic Analyzer Signals
    input  [127:0] la_data_in,
    output [127:0] la_data_out,
    input  [127:0] la_oenb,

    /* GPIOs.  There are 27 GPIOs, on either side of the analog.
     * These have the following mapping to the GPIO padframe pins
     * and memory-mapped registers, since the numbering remains the
     * same as caravel but skips over the analog I/O:
     *
     * io_in/out/oeb/in_3v3 [26:14]  <--->  mprj_io[37:25]
     * io_in/out/oeb/in_3v3 [13:0]   <--->  mprj_io[13:0]	
     *
     * When the GPIOs are configured by the Management SoC for
     * user use, they have three basic bidirectional controls:
     * in, out, and oeb (output enable, sense inverted).  For
     * analog projects, a 3.3V copy of the signal input is
     * available.  out and oeb must be 1.8V signals.
     */

    input  [`MPRJ_IO_PADS-`ANALOG_PADS-1:0] io_in,
    input  [`MPRJ_IO_PADS-`ANALOG_PADS-1:0] io_in_3v3,
    output [`MPRJ_IO_PADS-`ANALOG_PADS-1:0] io_out,
    output [`MPRJ_IO_PADS-`ANALOG_PADS-1:0] io_oeb,

    /* Analog (direct connection to GPIO pad---not for high voltage or
     * high frequency use).  The management SoC must turn off both
     * input and output buffers on these GPIOs to allow analog access.
     * These signals may drive a voltage up to the value of VDDIO
     * (3.3V typical, 5.5V maximum).
     * 
     * Note that analog I/O is not available on the 7 lowest-numbered
     * GPIO pads, and so the analog_io indexing is offset from the
     * GPIO indexing by 7, as follows:
     *
     * gpio_analog/noesd [17:7]  <--->  mprj_io[35:25]
     * gpio_analog/noesd [6:0]   <--->  mprj_io[13:7]	
     *
     */
    
    inout [`MPRJ_IO_PADS-`ANALOG_PADS-10:0] gpio_analog,
    inout [`MPRJ_IO_PADS-`ANALOG_PADS-10:0] gpio_noesd,

    /* Analog signals, direct through to pad.  These have no ESD at all,
     * so ESD protection is the responsibility of the designer.
     *
     * user_analog[10:0]  <--->  mprj_io[24:14]
     *
     */
    inout [`ANALOG_PADS-1:0] io_analog,

    /* Additional power supply ESD clamps, one per analog pad.  The
     * high side should be connected to a 3.3-5.5V power supply.
     * The low side should be connected to ground.
     *
     * clamp_high[2:0]   <--->  mprj_io[20:18]
     * clamp_low[2:0]    <--->  mprj_io[20:18]
     *
     */
    inout [2:0] io_clamp_high,
    inout [2:0] io_clamp_low,

    // Independent clock (on independent integer divider)
    input   user_clock2,

    // User maskable interrupt signals
    output [2:0] user_irq
);

/*--------------------------------------*/
/* User project is instantiated  here   */
/*--------------------------------------*/

user_analog_project_wrapper_ALICE mprj (

    `ifdef USE_POWER_PINS
        //Power Connections
        .VDDA1(vdda1),
        .VDDA2(vdda2),
        .VSSA1(vssa1),
        .VSSA2(vssa2),
        .VCCD1(vccd1),
        .VCCD2(vccd2),

         

         
    `endif

    //GPIO NOESD PINS
    .GPIONOESD00(gpio_noesd[0]),
    .GPIONOESD04(gpio_noesd[4]),
    .GPIONOESD05(gpio_noesd[5]),
    .GPIONOESD06(gpio_noesd[6]),
    .GPIONOESD15(gpio_noesd[15]),


    //Analog
    .ANALOG00(io_analog[0]),
    .ANALOG01(io_analog[1]),
    .ANALOG02(io_analog[2]),
    .ANALOG03(io_analog[3]),
    .ANALOG04(io_analog[4]),
    .ANALOG05(io_analog[5]),
    .ANALOG06(io_analog[6]),
    .ANALOG07(io_analog[7]),
    .ANALOG08(io_analog[8]),
    .ANALOG09(io_analog[9]),
    .ANALOG10(io_analog[10]),

     //Wishbone interface CLK
     .WB_CLK(wbs_clk_i),

    //Logic Analyzer OUT
    .LADATAOUT00(la_data_out[0]),
    .LADATAOUT01(la_data_out[1]),
    .LADATAOUT02(la_data_out[2]),
    .LADATAOUT03(la_data_out[3]),
    .LADATAOUT04(la_data_out[4]),
    .LADATAOUT05(la_data_out[5]),
    .LADATAOUT06(la_data_out[6]),
    .LADATAOUT07(la_data_out[7]),
    .LADATAOUT08(la_data_out[8]),
    .LADATAOUT09(la_data_out[9]),
    .LADATAOUT10(la_data_out[10]),
    .LADATAOUT11(la_data_out[11]),
    .LADATAOUT12(la_data_out[12]),
    .LADATAOUT13(la_data_out[13]),
    .LADATAOUT14(la_data_out[14]),
    .LADATAOUT15(la_data_out[15]),
    .LADATAOUT16(la_data_out[16]),
    .LADATAOUT17(la_data_out[17]),
    .LADATAOUT18(la_data_out[18]),
    .LADATAOUT19(la_data_out[19]),
    .LADATAOUT20(la_data_out[20]),
    .LADATAOUT21(la_data_out[21]),
    .LADATAOUT22(la_data_out[22]),
    .LADATAOUT23(la_data_out[23]),
    .LADATAOUT24(la_data_out[24]),	
    .LADATAOUT25(la_data_out[25]),	
    .LADATAOUT26(la_data_out[26]),	
    .LADATAOUT27(la_data_out[27]),	
    .LADATAOUT28(la_data_out[28]),	
    .LADATAOUT29(la_data_out[29]),	
    .LADATAOUT30(la_data_out[30]),	
    .LADATAOUT31(la_data_out[31]),	
    .LADATAOUT32(la_data_out[32]),	
    .LADATAOUT33(la_data_out[33]),	
    .LADATAOUT34(la_data_out[34]),	
    .LADATAOUT35(la_data_out[35]),	
    .LADATAOUT36(la_data_out[36]),	
    .LADATAOUT37(la_data_out[37]),	
    .LADATAOUT38(la_data_out[38]),	
    .LADATAOUT39(la_data_out[39]),	
    .LADATAOUT40(la_data_out[40]),	
    .LADATAOUT41(la_data_out[41]),	
    .LADATAOUT42(la_data_out[42]),	
    .LADATAOUT43(la_data_out[43]),	
    .LADATAOUT44(la_data_out[44]),	
    .LADATAOUT45(la_data_out[45]),	
    .LADATAOUT46(la_data_out[46]),	
    .LADATAOUT47(la_data_out[47]),	
    .LADATAOUT48(la_data_out[48]),	
    .LADATAOUT49(la_data_out[49]),	
    .LADATAOUT50(la_data_out[50]),	
    .LADATAOUT51(la_data_out[51]),	
    .LADATAOUT52(la_data_out[52]),	
    .LADATAOUT53(la_data_out[53]),	
    .LADATAOUT54(la_data_out[54]),	
    .LADATAOUT55(la_data_out[55]),	
    .LADATAOUT56(la_data_out[56]),	
    .LADATAOUT57(la_data_out[57]),	
    .LADATAOUT58(la_data_out[58]),	
    .LADATAOUT59(la_data_out[59]),	
    .LADATAOUT60(la_data_out[60]),	
    .LADATAOUT61(la_data_out[61]),	
    .LADATAOUT62(la_data_out[62]),	
    .LADATAOUT63(la_data_out[63]),	
    .LADATAOUT64(la_data_out[64]),	
    .LADATAOUT65(la_data_out[65]),	
    .LADATAOUT80(la_data_out[80]),	
    .LADATAOUT81(la_data_out[81]),	
    .LADATAOUT82(la_data_out[82]),	
    .LADATAOUT83(la_data_out[83]),	
    .LADATAOUT84(la_data_out[84]),	
    .LADATAOUT85(la_data_out[85]),	
    .LADATAOUT86(la_data_out[86]),	
    .LADATAOUT87(la_data_out[87]),	
    .LADATAOUT88(la_data_out[88]),	
    .LADATAOUT89(la_data_out[89]),	
    .LADATAOUT90(la_data_out[90]),	
    .LADATAOUT91(la_data_out[91]),	
    .LADATAOUT92(la_data_out[92]),	
    .LADATAOUT93(la_data_out[93]),	
    .LADATAOUT94(la_data_out[94]),	
    .LADATAOUT95(la_data_out[95]),	
    .LADATAOUT96(la_data_out[96]),	
    .LADATAOUT97(la_data_out[97]),	
    .LADATAOUT98(la_data_out[98]),	
    .LADATAOUT99(la_data_out[99]),	
    .LADATAOUT100(la_data_out[100]),	
    .LADATAOUT101(la_data_out[101]),	
    .LADATAOUT102(la_data_out[102]),	
    .LADATAOUT103(la_data_out[103]),	
    .LADATAOUT104(la_data_out[104]),	
    .LADATAOUT105(la_data_out[105]),	
    .LADATAOUT106(la_data_out[106]),	
    .LADATAOUT107(la_data_out[107]),	
    .LADATAOUT108(la_data_out[108]),	
    .LADATAOUT109(la_data_out[109]),	
    .LADATAOUT110(la_data_out[110]),	
    .LADATAOUT111(la_data_out[111]),	
    .LADATAOUT112(la_data_out[112]),	
    .LADATAOUT113(la_data_out[113]),	
    .LADATAOUT114(la_data_out[114]),	
    .LADATAOUT115(la_data_out[115]),	
    .LADATAOUT116(la_data_out[116]),	
    .LADATAOUT117(la_data_out[117]),
    .LADATAOUT118(la_data_out[118]),	
    .LADATAOUT119(la_data_out[119]),	
    .LADATAOUT120(la_data_out[120]),	
    .LADATAOUT121(la_data_out[121]),	
    .LADATAOUT122(la_data_out[122]),	
    .LADATAOUT123(la_data_out[123]),	
    .LADATAOUT124(la_data_out[124]),	
    .LADATAOUT125(la_data_out[125]),	
    .LADATAOUT126(la_data_out[126]),	
    .LADATAOUT127(la_data_out[127]),		

 
    //Logic Analyzer IN

    .LADATAIN15(la_data_in[15]),
    .LADATAIN65(la_data_in[65]),
    .LADATAIN66(la_data_in[66]),
    .LADATAIN67(la_data_in[67]),
    .LADATAIN68(la_data_in[68]),
    .LADATAIN69(la_data_in[69]),
    .LADATAIN70(la_data_in[70]),
    .LADATAIN71(la_data_in[71]),
    .LADATAIN72(la_data_in[72]),
    .LADATAIN73(la_data_in[73]),
    .LADATAIN74(la_data_in[74]),
    .LADATAIN75(la_data_in[75]),
    .LADATAIN76(la_data_in[76]),
    .LADATAIN77(la_data_in[77]),
    .LADATAIN78(la_data_in[78]),
    .LADATAIN79(la_data_in[79]),
    .LADATAIN85(la_data_in[85]),
    .LADATAIN119(la_data_in[119]),

);

endmodule

`default_nettype wire
