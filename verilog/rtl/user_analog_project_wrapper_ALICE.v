module user_analog_project_wrapper_Alice (
    
    GPIONOESD00,
    GPIONOESD04,
    GPIONOESD05,
    GPIONOESD06,
    GPIONOESD15,
    
    WB_CLK,
    
    VDDA1,
    VCCD1,
    VSSA1,
    VDDA2,
    VCCD2,
    VSSA2,


    
    ANALOG10,
    ANALOG09,
    ANALOG08,
    ANALOG07,
    ANALOG06,
    ANALOG05,
    ANALOG04,
    ANALOG03,
    ANALOG02,
    ANALOG01,
    ANALOG00,
    
    
    LADATAOUT00,
    LADATAOUT01,
    LADATAOUT02,
    LADATAOUT03,
    LADATAOUT04,
    LADATAOUT05,
    LADATAOUT06,
    LADATAOUT07,
    LADATAOUT08,
    LADATAOUT09,
    LADATAOUT10,
    LADATAOUT11,
    LADATAOUT12,
    LADATAOUT13,
    LADATAOUT14,
    LADATAOUT15,
    LADATAOUT16,
    LADATAOUT17,
    LADATAOUT18,
    LADATAOUT19,
    LADATAOUT20,
    LADATAOUT21,
    LADATAOUT22,
    LADATAOUT23,
    LADATAOUT24,
    LADATAOUT25,
    LADATAOUT26,
    LADATAOUT27,
    LADATAOUT28,
    LADATAOUT29,
    LADATAOUT30,
    LADATAOUT31,
    LADATAOUT32,
    LADATAOUT33,
    LADATAOUT34,
    LADATAOUT35,
    LADATAOUT36,
    LADATAOUT37,
    LADATAOUT38,
    LADATAOUT39,
    LADATAOUT40,
    LADATAOUT41,
    LADATAOUT42,
    LADATAOUT43,
    LADATAOUT44,
    LADATAOUT45,
    LADATAOUT46,
    LADATAOUT47,
    LADATAOUT48,
    LADATAOUT49,
    LADATAOUT50,
    LADATAOUT51,
    LADATAOUT52,
    LADATAOUT53,
    LADATAOUT54,
    LADATAOUT55,
    LADATAOUT56,
    LADATAOUT57,
    LADATAOUT58,
    LADATAOUT59,
    LADATAOUT60,
    LADATAOUT61,
    LADATAOUT62,
    LADATAOUT63,
    LADATAOUT64,
    LADATAOUT65,
    LADATAOUT80,
    LADATAOUT81,
    LADATAOUT82,
    LADATAOUT83,
    LADATAOUT84,
    LADATAOUT85,
    LADATAOUT86,
    LADATAOUT87,
    LADATAOUT88,
    LADATAOUT89,
    LADATAOUT90,
    LADATAOUT91,
    LADATAOUT92,
    LADATAOUT93,
    LADATAOUT94,
    LADATAOUT95,
    LADATAOUT96,
    LADATAOUT97,
    LADATAOUT98,
    LADATAOUT99,
    LADATAOUT100,
    LADATAOUT101,
    LADATAOUT102,
    LADATAOUT103,
    LADATAOUT104,
    LADATAOUT105,
    LADATAOUT106,
    LADATAOUT107,
    LADATAOUT108,
    LADATAOUT109,
    LADATAOUT110,
    LADATAOUT111,
    LADATAOUT112,
    LADATAOUT113,
    LADATAOUT114,
    LADATAOUT115,
    LADATAOUT116,
    LADATAOUT117,
    LADATAOUT118,
    LADATAOUT119,
    LADATAOUT120,
    LADATAOUT121,
    LADATAOUT122,
    LADATAOUT123,
    LADATAOUT124,
    LADATAOUT125,
    LADATAOUT126,
    LADATAOUT127,

    LADATAIN15,
    LADATAIN65,
    LADATAIN66,
    LADATAIN67,
    LADATAIN68,
    LADATAIN69,
    LADATAIN70,
    LADATAIN71,
    LADATAIN72,
    LADATAIN73,
    LADATAIN74,
    LADATAIN75,
    LADATAIN76,
    LADATAIN77,
    LADATAIN78,
    LADATAIN79,
    LADATAIN85,
    LADATAIN119,
    
);

        inout GPIONOESD00;
        inout GPIONOESD04;
        inout GPIONOESD05;
        inout GPIONOESD06;
        inout GPIONOESD15;
    
        inout WB_CLK;
        
        inout VDDA1;
        inout VCCD1;
        inout VSSA1;
        inout VDDA2;
        inout VCCD2;
        inout VSSA2;

        inout ANALOG10;
        inout ANALOG09;
        inout ANALOG08;
        inout ANALOG07;
        inout ANALOG06;
        inout ANALOG05;
        inout ANALOG04;
        inout ANALOG03;
        inout ANALOG02;
        inout ANALOG01;
        inout ANALOG00;
        
        
        inout LADATAOUT1;
        inout LADATAOUT2;
        inout LADATAOUT3;
        inout LADATAOUT4;
        inout LADATAOUT5;
        inout LADATAOUT6;
        inout LADATAOUT7;
        inout LADATAOUT8;
        inout LADATAOUT9;
        inout LADATAOUT10;
        inout LADATAOUT11;
        inout LADATAOUT12;
        inout LADATAOUT13;
        inout LADATAOUT14;
        inout LADATAOUT15;
        inout LADATAOUT16;
        inout LADATAOUT17;
        inout LADATAOUT18;
        inout LADATAOUT19;
        inout LADATAOUT20;
        inout LADATAOUT21;
        inout LADATAOUT22;
        inout LADATAOUT23;
        inout LADATAOUT24;
        inout LADATAOUT25;
        inout LADATAOUT26;
        inout LADATAOUT27;
        inout LADATAOUT28;
        inout LADATAOUT29;
        inout LADATAOUT30;
        inout LADATAOUT31;
        inout LADATAOUT32;
        inout LADATAOUT33;
        inout LADATAOUT34;
        inout LADATAOUT35;
        inout LADATAOUT36;
        inout LADATAOUT37;
        inout LADATAOUT38;
        inout LADATAOUT39;
        inout LADATAOUT40;
        inout LADATAOUT41;
        inout LADATAOUT42;
        inout LADATAOUT43;
        inout LADATAOUT44;
        inout LADATAOUT45;
        inout LADATAOUT46;
        inout LADATAOUT47;
        inout LADATAOUT48;
        inout LADATAOUT49;
        inout LADATAOUT50;
        inout LADATAOUT51;
        inout LADATAOUT52;
        inout LADATAOUT53;
        inout LADATAOUT54;
        inout LADATAOUT55;
        inout LADATAOUT56;
        inout LADATAOUT57;
        inout LADATAOUT58;
        inout LADATAOUT59;
        inout LADATAOUT60;
        inout LADATAOUT61;
        inout LADATAOUT62;
        inout LADATAOUT63;
        inout LADATAOUT64;
        inout LADATAOUT65;
        inout LADATAOUT80;
        inout LADATAOUT81;
        inout LADATAOUT82;
        inout LADATAOUT83;
        inout LADATAOUT84;
        inout LADATAOUT85;
        inout LADATAOUT86;
        inout LADATAOUT87;
        inout LADATAOUT88;
        inout LADATAOUT89;
        inout LADATAOUT90;
        inout LADATAOUT91;
        inout LADATAOUT92;
        inout LADATAOUT93;
        inout LADATAOUT94;
        inout LADATAOUT95;
        inout LADATAOUT96;
        inout LADATAOUT97;
        inout LADATAOUT98;
        inout LADATAOUT99;
        inout LADATAOUT100;
        inout LADATAOUT101;
        inout LADATAOUT102;
        inout LADATAOUT103;
        inout LADATAOUT104;
        inout LADATAOUT105;
        inout LADATAOUT106;
        inout LADATAOUT107;
        inout LADATAOUT108;
        inout LADATAOUT109;
        inout LADATAOUT110;
        inout LADATAOUT111;
        inout LADATAOUT112;
        inout LADATAOUT113;
        inout LADATAOUT114;
        inout LADATAOUT115;
        inout LADATAOUT116;
        inout LADATAOUT117;
        inout LADATAOUT118;
        inout LADATAOUT119;
        inout LADATAOUT120;
        inout LADATAOUT121;
        inout LADATAOUT122;
        inout LADATAOUT123;
        inout LADATAOUT124;
        inout LADATAOUT125;
        inout LADATAOUT126;
        inout LADATAOUT127;

        inout LADATAIN15;
        inout LADATAIN65;
        inout LADATAIN66;
        inout LADATAIN67;
        inout LADATAIN68;
        inout LADATAIN69;
        inout LADATAIN70;
        inout LADATAIN71;
        inout LADATAIN72;
        inout LADATAIN73;
        inout LADATAIN74;
        inout LADATAIN75;
        inout LADATAIN76;
        inout LADATAIN77;
        inout LADATAIN78;
        inout LADATAIN79;
        inout LADATAIN85;
        inout LADATAIN119;

     
endmodule


