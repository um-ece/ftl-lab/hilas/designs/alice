v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -580 230 -550 230 { lab=#net1}
N -490 230 -470 230 { lab=#net2}
N -430 230 -400 230 { lab=#net2}
N -470 230 -430 230 { lab=#net2}
N -440 260 -280 260 { lab=#net2}
N -440 320 -280 320 { lab=#net3}
N -400 230 -380 230 { lab=#net2}
N -380 230 -380 260 { lab=#net2}
N -260 230 -230 230 { lab=#net2}
N -120 260 40 260 { lab=#net4}
N -120 320 40 320 { lab=#net5}
N -60 230 -60 260 { lab=#net4}
N -380 230 -260 230 { lab=#net2}
N 130 230 150 230 { lab=#net6}
N 190 230 220 230 { lab=#net6}
N 150 230 190 230 { lab=#net6}
N 180 260 340 260 { lab=#net6}
N 180 320 340 320 { lab=#net3}
N 220 230 240 230 { lab=#net6}
N 240 230 240 260 { lab=#net6}
N -60 230 70 230 { lab=#net4}
N 430 230 450 230 { lab=#net7}
N 490 230 520 230 { lab=#net7}
N 450 230 490 230 { lab=#net7}
N 480 260 640 260 { lab=#net7}
N 480 320 640 320 { lab=#net5}
N 520 230 540 230 { lab=#net7}
N 540 230 540 260 { lab=#net7}
N 240 230 370 230 { lab=#net6}
N -120 260 40 260 { lab=#net4}
N -120 320 40 320 { lab=#net5}
N -60 230 -60 260 { lab=#net4}
N -170 230 -60 230 { lab=#net4}
N 540 230 650 230 { lab=#net7}
N 730 230 750 230 { lab=#net8}
N 790 230 820 230 { lab=#net8}
N 750 230 790 230 { lab=#net8}
N 780 260 940 260 { lab=#net8}
N 780 320 940 320 { lab=#net3}
N 820 230 840 230 { lab=#net8}
N 840 230 840 260 { lab=#net8}
N 960 230 990 230 { lab=#net8}
N 1100 260 1260 260 { lab=#net9}
N 1100 320 1260 320 { lab=#net5}
N 1160 230 1160 260 { lab=#net9}
N 840 230 960 230 { lab=#net8}
N 1350 230 1370 230 { lab=#net10}
N 1410 230 1440 230 { lab=#net10}
N 1370 230 1410 230 { lab=#net10}
N 1400 260 1560 260 { lab=#net10}
N 1400 320 1560 320 { lab=#net3}
N 1440 230 1460 230 { lab=#net10}
N 1460 230 1460 260 { lab=#net10}
N 1160 230 1290 230 { lab=#net9}
N 1650 230 1670 230 { lab=#net11}
N 1710 230 1740 230 { lab=#net11}
N 1670 230 1710 230 { lab=#net11}
N 1700 260 1860 260 { lab=#net11}
N 1700 320 1860 320 { lab=#net5}
N 1740 230 1760 230 { lab=#net11}
N 1760 230 1760 260 { lab=#net11}
N 1460 230 1590 230 { lab=#net10}
N 1100 260 1260 260 { lab=#net9}
N 1100 320 1260 320 { lab=#net5}
N 1160 230 1160 260 { lab=#net9}
N 1050 230 1160 230 { lab=#net9}
N 1760 230 1870 230 { lab=#net11}
N 650 230 670 230 { lab=#net7}
N 1940 230 1960 230 { lab=#net12}
N 2000 230 2030 230 { lab=#net12}
N 1960 230 2000 230 { lab=#net12}
N 1990 260 2150 260 { lab=#net12}
N 1990 320 2150 320 { lab=VGND}
N 2030 230 2050 230 { lab=#net12}
N 2050 230 2050 260 { lab=#net12}
N 1870 230 1880 230 { lab=#net11}
N 750 620 850 620 { lab=#net3}
N 850 550 850 620 { lab=#net3}
N -350 550 850 550 { lab=#net3}
N -350 320 -350 550 { lab=#net3}
N 240 320 240 550 { lab=#net3}
N 880 320 880 550 { lab=#net3}
N 1480 320 1480 550 { lab=#net3}
N 850 550 1480 550 { lab=#net3}
N 750 600 790 600 { lab=#net5}
N 790 500 790 600 { lab=#net5}
N -30 500 790 500 { lab=#net5}
N -30 320 -30 500 { lab=#net5}
N 570 320 570 500 { lab=#net5}
N 790 500 1180 500 { lab=#net5}
N 1180 320 1180 500 { lab=#net5}
N 1180 500 1780 500 { lab=#net5}
N 1780 320 1780 500 { lab=#net5}
N 420 660 450 660 { lab=VGND}
N 420 660 420 750 { lab=VGND}
N 420 750 2030 750 { lab=VGND}
N 2030 320 2030 750 { lab=VGND}
N -800 70 -780 70 { lab=VGND}
N -800 150 -790 150 { lab=VGND}
N -800 70 -800 150 { lab=VGND}
N -720 70 -710 70 { lab=#net1}
N -710 70 -710 150 { lab=#net1}
N -730 150 -710 150 { lab=#net1}
N -660 260 -640 260 { lab=#net1}
N -640 260 -640 350 { lab=#net1}
N -650 350 -640 350 { lab=#net1}
N -730 260 -720 260 { lab=VPWR}
N -730 260 -730 350 { lab=VPWR}
N -730 350 -710 350 { lab=VPWR}
N -710 130 -620 130 { lab=#net1}
N -620 130 -620 280 { lab=#net1}
N -640 280 -620 280 { lab=#net1}
N -620 230 -580 230 { lab=#net1}
N -840 390 -680 390 { lab=VTUNENABLE}
N -840 30 -840 390 { lab=VTUNENABLE}
N -840 30 -750 30 { lab=VTUNENABLE}
N -760 220 -690 220 { lab=#net13}
N -760 190 -760 220 { lab=#net13}
N -960 190 -960 240 { lab=#net13}
N -1010 160 -1000 160 { lab=VTUNENABLE}
N -1010 160 -1010 270 { lab=VTUNENABLE}
N -1010 270 -1000 270 { lab=VTUNENABLE}
N -960 160 -940 160 { lab=VPWR}
N -940 110 -940 160 { lab=VPWR}
N -960 110 -940 110 { lab=VPWR}
N -960 110 -960 130 { lab=VPWR}
N -750 70 -750 100 { lab=VPWR}
N -950 100 -750 100 { lab=VPWR}
N -950 100 -950 110 { lab=VPWR}
N -690 260 -690 280 { lab=VPWR}
N -810 280 -690 280 { lab=VPWR}
N -810 100 -810 280 { lab=VPWR}
N -960 270 -940 270 { lab=VGND}
N -940 270 -940 320 { lab=VGND}
N -960 320 -940 320 { lab=VGND}
N -960 300 -960 320 { lab=VGND}
N -680 340 -680 350 { lab=VGND}
N -950 340 -680 340 { lab=VGND}
N -950 320 -950 340 { lab=VGND}
N -760 140 -760 150 { lab=VGND}
N -830 140 -760 140 { lab=VGND}
N -830 140 -830 340 { lab=VGND}
N -960 210 -760 210 { lab=#net13}
N -1060 620 450 620 { lab=VTUNENABLE}
N -1060 230 -1060 620 { lab=VTUNENABLE}
N -1060 230 -1010 230 { lab=VTUNENABLE}
N -1060 390 -840 390 { lab=VTUNENABLE}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} -520 230 1 0 {name=D6
model=diode_pd2nw_11v0
area=6.831}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} -440 290 0 0 {name=C19 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} -280 290 2 0 {name=C20 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} -200 230 1 0 {name=D7
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} -120 290 0 0 {name=C21 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 40 290 2 0 {name=C22 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 100 230 1 0 {name=D8
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 180 290 0 0 {name=C23 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 340 290 2 0 {name=C24 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 400 230 1 0 {name=D9
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 480 290 0 0 {name=C25 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 640 290 2 0 {name=C26 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 700 230 1 0 {name=D10
model=diode_pd2nw_11v0
area=6.831}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 780 290 0 0 {name=C27 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 940 290 2 0 {name=C28 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 1020 230 1 0 {name=D11
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 1100 290 0 0 {name=C29 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 1260 290 2 0 {name=C30 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 1320 230 1 0 {name=D12
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 1400 290 0 0 {name=C31 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 1560 290 2 0 {name=C32 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 1620 230 1 0 {name=D13
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 1700 290 0 0 {name=C33 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 1860 290 2 0 {name=C34 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 1910 230 1 0 {name=D14
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 1990 290 0 0 {name=C35 model=cap_mim_m3_1 W=250 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 2150 290 2 0 {name=C36 model=cap_mim_m3_2 W=250 L=5 MF=1 spiceprefix=X}
C {/home/hilas/Documents/work/alice/xschem/sky130_hilas_clk_gen.sym} 600 630 0 0 {name=x1}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -760 170 3 0 {name=M1
L=0.20
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -680 370 3 0 {name=M2
L=0.20
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8.sym} -750 50 1 0 {name=M3
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8.sym} -690 240 1 0 {name=M4
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -980 270 0 0 {name=M5
L=0.20
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8.sym} -980 160 0 0 {name=M6
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {ipin.sym} -730 310 0 0 {name=p1 lab=VPWR}
C {ipin.sym} -800 110 0 0 {name=p2 lab=VGND}
C {ipin.sym} -950 340 0 0 {name=p3 lab=VGND}
C {ipin.sym} -960 110 0 0 {name=p4 lab=VPWR}
C {ipin.sym} 420 680 0 0 {name=p5 lab=VGND}
C {ipin.sym} 450 600 0 0 {name=p6 lab=VPWR}
C {ipin.sym} -1060 450 0 0 {name=p7 lab=VTUNENABLE}
C {ipin.sym} 450 640 0 0 {name=p8 lab=TUNCLK}
C {noconn.sym} 750 640 2 0 {name=l1}
C {noconn.sym} 750 660 2 0 {name=l2}
