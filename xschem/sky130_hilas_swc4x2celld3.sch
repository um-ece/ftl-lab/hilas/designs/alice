v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 790 -910 820 -910 { lab=#net1}
N 750 -860 750 -850 { lab=GATE1}
N 750 -850 790 -850 { lab=GATE1}
N 630 -910 790 -910 { lab=#net1}
N 630 -850 670 -850 { lab=VTUN}
N 630 -860 630 -850 { lab=VTUN}
N 900 -1040 900 -830 { lab=COL1}
N 440 -800 940 -800 { lab=ROW4}
N 900 -830 900 -780 { lab=COL1}
N 860 -880 860 -800 { lab=ROW4}
N 860 -940 900 -940 { lab=COL1}
N 750 -850 750 -780 { lab=GATE1}
N 750 -1040 750 -850 { lab=GATE1}
N 670 -860 670 -850 { lab=VTUN}
N 670 -850 670 -780 { lab=VTUN}
N 670 -1040 670 -850 { lab=VTUN}
N 790 -1170 820 -1170 { lab=#net2}
N 750 -1120 750 -1110 { lab=GATE1}
N 750 -1110 790 -1110 { lab=GATE1}
N 630 -1170 790 -1170 { lab=#net2}
N 630 -1110 670 -1110 { lab=VTUN}
N 630 -1120 630 -1110 { lab=VTUN}
N 900 -1300 900 -1090 { lab=COL1}
N 440 -1060 940 -1060 { lab=ROW3}
N 900 -1090 900 -1040 { lab=COL1}
N 860 -1140 860 -1060 { lab=ROW3}
N 860 -1200 900 -1200 { lab=COL1}
N 750 -1110 750 -1040 { lab=GATE1}
N 750 -1300 750 -1110 { lab=GATE1}
N 670 -1120 670 -1110 { lab=VTUN}
N 670 -1110 670 -1040 { lab=VTUN}
N 670 -1300 670 -1110 { lab=VTUN}
N 790 -1430 820 -1430 { lab=#net3}
N 750 -1380 750 -1370 { lab=GATE1}
N 750 -1370 790 -1370 { lab=GATE1}
N 630 -1430 790 -1430 { lab=#net3}
N 630 -1370 670 -1370 { lab=VTUN}
N 630 -1380 630 -1370 { lab=VTUN}
N 900 -1560 900 -1350 { lab=COL1}
N 440 -1320 940 -1320 { lab=ROW2}
N 900 -1350 900 -1300 { lab=COL1}
N 860 -1400 860 -1320 { lab=ROW2}
N 860 -1460 900 -1460 { lab=COL1}
N 750 -1370 750 -1300 { lab=GATE1}
N 750 -1560 750 -1370 { lab=GATE1}
N 670 -1380 670 -1370 { lab=VTUN}
N 670 -1370 670 -1300 { lab=VTUN}
N 670 -1560 670 -1370 { lab=VTUN}
N 790 -1690 820 -1690 { lab=#net4}
N 750 -1640 750 -1630 { lab=GATE1}
N 750 -1630 790 -1630 { lab=GATE1}
N 630 -1690 790 -1690 { lab=#net4}
N 630 -1630 670 -1630 { lab=VTUN}
N 630 -1640 630 -1630 { lab=VTUN}
N 900 -1820 900 -1610 { lab=COL1}
N 440 -1580 940 -1580 { lab=ROW1}
N 900 -1610 900 -1560 { lab=COL1}
N 860 -1660 860 -1580 { lab=ROW1}
N 860 -1720 900 -1720 { lab=COL1}
N 750 -1630 750 -1560 { lab=GATE1}
N 750 -1820 750 -1630 { lab=GATE1}
N 670 -1640 670 -1630 { lab=VTUN}
N 670 -1630 670 -1560 { lab=VTUN}
N 670 -1820 670 -1630 { lab=VTUN}
N 420 -1580 450 -1580 { lab=ROW1}
N 420 -1320 450 -1320 { lab=ROW2}
N 420 -1060 450 -1060 { lab=ROW3}
N 420 -800 450 -800 { lab=ROW4}
N 900 -1830 900 -1810 { lab=COL1}
N 750 -1830 750 -1810 { lab=GATE1}
N 670 -1830 670 -1810 { lab=VTUN}
N 860 -910 880 -910 { lab=#net5}
N 860 -1170 880 -1170 { lab=#net6}
N 860 -1430 880 -1430 { lab=#net7}
N 860 -1690 880 -1690 { lab=#net8}
N 900 -1830 920 -1830 { lab=COL1}
N 670 -1840 670 -1830 { lab=VTUN}
N 670 -1840 680 -1840 { lab=VTUN}
N 1290 -910 1320 -910 { lab=#net9}
N 1250 -860 1250 -850 { lab=GATE2}
N 1250 -850 1290 -850 { lab=GATE2}
N 1130 -910 1290 -910 { lab=#net9}
N 1130 -850 1170 -850 { lab=VTUN}
N 1130 -860 1130 -850 { lab=VTUN}
N 1400 -1040 1400 -830 { lab=COL2}
N 940 -800 1440 -800 { lab=ROW4}
N 1400 -830 1400 -780 { lab=COL2}
N 1360 -880 1360 -800 { lab=ROW4}
N 1360 -940 1400 -940 { lab=COL2}
N 1250 -850 1250 -780 { lab=GATE2}
N 1250 -1040 1250 -850 { lab=GATE2}
N 1170 -860 1170 -850 { lab=VTUN}
N 1170 -850 1170 -780 { lab=VTUN}
N 1170 -1040 1170 -850 { lab=VTUN}
N 1290 -1170 1320 -1170 { lab=#net11}
N 1250 -1120 1250 -1110 { lab=GATE2}
N 1250 -1110 1290 -1110 { lab=GATE2}
N 1130 -1170 1290 -1170 { lab=#net11}
N 1130 -1110 1170 -1110 { lab=VTUN}
N 1130 -1120 1130 -1110 { lab=VTUN}
N 1400 -1300 1400 -1090 { lab=COL2}
N 940 -1060 1440 -1060 { lab=ROW3}
N 1400 -1090 1400 -1040 { lab=COL2}
N 1360 -1140 1360 -1060 { lab=ROW3}
N 1360 -1200 1400 -1200 { lab=COL2}
N 1250 -1110 1250 -1040 { lab=GATE2}
N 1250 -1300 1250 -1110 { lab=GATE2}
N 1170 -1120 1170 -1110 { lab=VTUN}
N 1170 -1110 1170 -1040 { lab=VTUN}
N 1170 -1300 1170 -1110 { lab=VTUN}
N 1290 -1430 1320 -1430 { lab=#net13}
N 1250 -1380 1250 -1370 { lab=GATE2}
N 1250 -1370 1290 -1370 { lab=GATE2}
N 1130 -1430 1290 -1430 { lab=#net13}
N 1130 -1370 1170 -1370 { lab=VTUN}
N 1130 -1380 1130 -1370 { lab=VTUN}
N 1400 -1560 1400 -1350 { lab=COL2}
N 940 -1320 1440 -1320 { lab=ROW2}
N 1400 -1350 1400 -1300 { lab=COL2}
N 1360 -1400 1360 -1320 { lab=ROW2}
N 1360 -1460 1400 -1460 { lab=COL2}
N 1250 -1370 1250 -1300 { lab=GATE2}
N 1250 -1560 1250 -1370 { lab=GATE2}
N 1170 -1380 1170 -1370 { lab=VTUN}
N 1170 -1370 1170 -1300 { lab=VTUN}
N 1170 -1560 1170 -1370 { lab=VTUN}
N 1290 -1690 1320 -1690 { lab=#net15}
N 1250 -1640 1250 -1630 { lab=GATE2}
N 1250 -1630 1290 -1630 { lab=GATE2}
N 1130 -1690 1290 -1690 { lab=#net15}
N 1130 -1630 1170 -1630 { lab=VTUN}
N 1130 -1640 1130 -1630 { lab=VTUN}
N 1400 -1820 1400 -1610 { lab=COL2}
N 940 -1580 1440 -1580 { lab=ROW1}
N 1400 -1610 1400 -1560 { lab=COL2}
N 1360 -1660 1360 -1580 { lab=ROW1}
N 1360 -1720 1400 -1720 { lab=COL2}
N 1250 -1630 1250 -1560 { lab=GATE2}
N 1250 -1820 1250 -1630 { lab=GATE2}
N 1170 -1640 1170 -1630 { lab=VTUN}
N 1170 -1630 1170 -1560 { lab=VTUN}
N 1170 -1820 1170 -1630 { lab=VTUN}
N 1400 -1830 1400 -1810 { lab=COL2}
N 1250 -1830 1250 -1810 { lab=GATE2}
N 1360 -910 1380 -910 { lab=#net8}
N 1360 -1170 1380 -1170 { lab=#net8}
N 1360 -1430 1380 -1430 { lab=#net8}
N 1360 -1690 1380 -1690 { lab=#net8}
N 1400 -1830 1420 -1830 { lab=COL2}
N 1170 -1870 1170 -1820 { lab=VTUN}
N 670 -1870 1170 -1870 { lab=VTUN}
N 670 -1870 670 -1840 { lab=VTUN}
N 340 -1780 370 -1780 { lab=VGND}
N 340 -1780 340 -1730 { lab=VGND}
N 880 -1690 900 -1690 {}
N 1380 -1690 1400 -1690 {}
N 1380 -1430 1400 -1430 {}
N 1380 -1170 1400 -1170 {}
N 1380 -910 1400 -910 {}
N 880 -910 900 -910 {}
N 880 -1170 900 -1170 {}
N 880 -1430 900 -1430 {}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 840 -910 0 0 {name=M3
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 790 -880 0 0 {name=C1 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/cap_var_hvt.sym} 630 -880 0 1 {name=C2 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 840 -1170 0 0 {name=M2
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 790 -1140 0 0 {name=C3 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/cap_var_hvt.sym} 630 -1140 0 1 {name=C4 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 840 -1430 0 0 {name=M7
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 790 -1400 0 0 {name=C5 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/cap_var_hvt.sym} 630 -1400 0 1 {name=C6 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 840 -1690 0 0 {name=M10
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 790 -1660 0 0 {name=C7 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/cap_var_hvt.sym} 630 -1660 0 1 {name=C8 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {devices/ipin.sym} 430 -1580 0 0 {name=ROW1 lab=ROW1}
C {devices/ipin.sym} 430 -1320 0 0 {name=ROW2 lab=ROW2}
C {devices/ipin.sym} 430 -1060 0 0 {name=ROW3 lab=ROW3}
C {devices/ipin.sym} 430 -800 0 0 {name=DRAIN5 lab=ROW4}
C {devices/iopin.sym} 920 -1830 0 0 {name=COL1 lab=COL1}
C {devices/iopin.sym} 750 -1830 0 0 {name=GATE1 lab=GATE1}
C {devices/iopin.sym} 680 -1840 0 0 {name=VTUN lab=VTUN}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1340 -910 0 0 {name=M13
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 1290 -880 0 0 {name=C9 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/cap_var_hvt.sym} 1130 -880 0 1 {name=C10 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1340 -1170 0 0 {name=M16
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 1290 -1140 0 0 {name=C11 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/cap_var_hvt.sym} 1130 -1140 0 1 {name=C12 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1340 -1430 0 0 {name=M19
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 1290 -1400 0 0 {name=C13 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/cap_var_hvt.sym} 1130 -1400 0 1 {name=C14 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1340 -1690 0 0 {name=M22
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 1290 -1660 0 0 {name=C15 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/cap_var_hvt.sym} 1130 -1660 0 1 {name=C16 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {devices/iopin.sym} 1420 -1830 0 0 {name=COL2 lab=COL2}
C {devices/iopin.sym} 1250 -1830 0 0 {name=GATE2 lab=GATE2}
C {devices/iopin.sym} 370 -1780 0 0 {name=VGND lab=VGND}
C {devices/gnd.sym} 340 -1730 0 0 {name=l1 lab=VGND}
