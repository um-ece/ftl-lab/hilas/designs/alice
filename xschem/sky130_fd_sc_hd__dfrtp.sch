v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 420 -1420 420 -1360 { lab=#net1}
N 350 -1450 380 -1450 { lab=D}
N 350 -1450 350 -1330 { lab=D}
N 350 -1330 380 -1330 { lab=D}
N 520 -1450 550 -1450 { lab=#net1}
N 520 -1450 520 -1350 { lab=#net1}
N 520 -1350 530 -1350 { lab=#net1}
N 610 -1450 620 -1450 { lab=#net2}
N 620 -1450 620 -1350 { lab=#net2}
N 590 -1350 620 -1350 { lab=#net2}
N 420 -1450 450 -1450 { lab=VPWR}
N 450 -1480 450 -1450 { lab=VPWR}
N 420 -1480 450 -1480 { lab=VPWR}
N 420 -1330 440 -1330 { lab=VSUBS}
N 440 -1330 440 -1300 { lab=VSUBS}
N 420 -1300 440 -1300 { lab=VSUBS}
N 420 -1390 520 -1390 { lab=#net1}
N 620 -1440 700 -1440 { lab=#net2}
N 760 -1440 770 -1440 { lab=#net3}
N 860 -1420 960 -1420 { lab=#net3}
N 960 -1430 960 -1420 { lab=#net3}
N 770 -1420 860 -1420 { lab=#net3}
N 770 -1440 770 -1420 { lab=#net3}
N 620 -1350 670 -1350 { lab=#net2}
N 670 -1350 670 -1340 { lab=#net2}
N 670 -1340 680 -1340 { lab=#net2}
N 740 -1340 780 -1340 { lab=#net4}
N 780 -1350 780 -1340 { lab=#net4}
N 780 -1350 870 -1350 { lab=#net4}
N 870 -1290 870 -1270 { lab=#net5}
N 810 -1450 820 -1450 { lab=#net6}
N 810 -1450 810 -1320 { lab=#net6}
N 810 -1320 830 -1320 { lab=#net6}
N 1000 -1460 1000 -1240 { lab=RESET_B}
N 910 -1240 1000 -1240 { lab=RESET_B}
N 860 -1510 860 -1480 { lab=VPWR}
N 860 -1510 960 -1510 { lab=VPWR}
N 960 -1510 960 -1490 { lab=VPWR}
N 1140 -1430 1140 -1370 { lab=#net6}
N 1070 -1460 1100 -1460 { lab=#net2}
N 1070 -1460 1070 -1340 { lab=#net2}
N 1070 -1340 1100 -1340 { lab=#net2}
N 1140 -1460 1170 -1460 { lab=VPWR}
N 1170 -1490 1170 -1460 { lab=VPWR}
N 1140 -1490 1170 -1490 { lab=VPWR}
N 1140 -1340 1160 -1340 { lab=VSUBS}
N 1160 -1340 1160 -1310 { lab=VSUBS}
N 1140 -1310 1160 -1310 { lab=VSUBS}
N 1260 -1460 1290 -1460 { lab=#net6}
N 1260 -1460 1260 -1360 { lab=#net6}
N 1260 -1360 1270 -1360 { lab=#net6}
N 1350 -1460 1360 -1460 { lab=#net7}
N 1360 -1460 1360 -1360 { lab=#net7}
N 1330 -1360 1360 -1360 { lab=#net7}
N 1140 -1400 1260 -1400 { lab=#net6}
N 620 -1350 620 -1200 { lab=#net2}
N 620 -1200 620 -1150 { lab=#net2}
N 620 -1150 1070 -1150 { lab=#net2}
N 1070 -1340 1070 -1150 { lab=#net2}
N 810 -1320 810 -1190 { lab=#net6}
N 810 -1190 1190 -1190 { lab=#net6}
N 1190 -1400 1190 -1190 { lab=#net6}
N 1490 -1440 1490 -1420 { lab=#net8}
N 1490 -1360 1490 -1320 { lab=#net7}
N 1490 -1260 1490 -1240 { lab=#net9}
N 1530 -1470 1610 -1470 { lab=#net10}
N 1610 -1470 1610 -1210 { lab=#net10}
N 1530 -1210 1610 -1210 { lab=#net10}
N 1750 -1430 1750 -1400 { lab=#net10}
N 1750 -1400 1870 -1400 { lab=#net10}
N 1870 -1440 1870 -1400 { lab=#net10}
N 1750 -1530 1750 -1490 { lab=VPWR}
N 1750 -1530 1870 -1530 { lab=VPWR}
N 1870 -1530 1870 -1500 { lab=VPWR}
N 1810 -1400 1810 -1360 { lab=#net10}
N 1810 -1300 1810 -1280 { lab=#net11}
N 1910 -1470 1940 -1470 { lab=#net7}
N 1940 -1470 1940 -1330 { lab=#net7}
N 1850 -1330 1940 -1330 { lab=#net7}
N 1360 -1410 1390 -1410 { lab=#net7}
N 1390 -1410 1390 -1340 { lab=#net7}
N 1390 -1340 1490 -1340 { lab=#net7}
N 1390 -1340 1390 -1120 { lab=#net7}
N 1390 -1120 1940 -1120 { lab=#net7}
N 1940 -1340 1940 -1120 { lab=#net7}
N 1680 -1460 1710 -1460 { lab=RESET_B}
N 1680 -1460 1680 -1250 { lab=RESET_B}
N 1680 -1250 1770 -1250 { lab=RESET_B}
N 1680 -1250 1680 -1070 { lab=RESET_B}
N 1010 -1070 1680 -1070 { lab=RESET_B}
N 1010 -1250 1010 -1070 { lab=RESET_B}
N 1000 -1250 1010 -1250 { lab=RESET_B}
N 2070 -1380 2070 -1320 { lab=Q}
N 2000 -1410 2030 -1410 { lab=#net10}
N 2000 -1410 2000 -1290 { lab=#net10}
N 2000 -1290 2030 -1290 { lab=#net10}
N 2070 -1410 2100 -1410 { lab=VPWR}
N 2100 -1440 2100 -1410 { lab=VPWR}
N 2070 -1440 2100 -1440 { lab=VPWR}
N 2070 -1290 2090 -1290 { lab=VSUBS}
N 2090 -1290 2090 -1260 { lab=VSUBS}
N 2070 -1260 2090 -1260 { lab=VSUBS}
N 2090 -1560 2090 -1440 { lab=VPWR}
N 1820 -1560 2090 -1560 { lab=VPWR}
N 1820 -1560 1820 -1530 { lab=VPWR}
N 1490 -1560 1820 -1560 { lab=VPWR}
N 1490 -1560 1490 -1500 { lab=VPWR}
N 1140 -1560 1140 -1490 { lab=VPWR}
N 1460 -1470 1500 -1470 { lab=VPWR}
N 1460 -1560 1460 -1470 { lab=VPWR}
N 1430 -1390 1500 -1390 { lab=VPWR}
N 1430 -1560 1430 -1390 { lab=VPWR}
N 1750 -1460 1780 -1460 { lab=VPWR}
N 1780 -1530 1780 -1460 { lab=VPWR}
N 1820 -1470 1870 -1470 { lab=VPWR}
N 1820 -1530 1820 -1470 { lab=VPWR}
N 910 -1560 1140 -1560 { lab=VPWR}
N 910 -1560 910 -1510 { lab=VPWR}
N -30 -1430 -30 -1370 { lab=clkb}
N -100 -1460 -70 -1460 { lab=CLK_IN}
N -100 -1460 -100 -1340 { lab=CLK_IN}
N -100 -1340 -70 -1340 { lab=CLK_IN}
N -30 -1460 0 -1460 { lab=VPWR}
N 0 -1490 0 -1460 { lab=VPWR}
N -30 -1490 0 -1490 { lab=VPWR}
N -30 -1340 -10 -1340 { lab=VSUBS}
N -10 -1340 -10 -1310 { lab=VSUBS}
N -30 -1310 -10 -1310 { lab=VSUBS}
N 140 -1430 140 -1370 { lab=clk}
N 70 -1460 100 -1460 { lab=clkb}
N 70 -1460 70 -1340 { lab=clkb}
N 70 -1340 100 -1340 { lab=clkb}
N 140 -1460 170 -1460 { lab=VPWR}
N 170 -1490 170 -1460 { lab=VPWR}
N 140 -1490 170 -1490 { lab=VPWR}
N 140 -1340 160 -1340 { lab=VSUBS}
N 160 -1340 160 -1310 { lab=VSUBS}
N 140 -1310 160 -1310 { lab=VSUBS}
N -30 -1400 70 -1400 { lab=clkb}
N -30 -1560 910 -1560 { lab=VPWR}
N -30 -1560 -30 -1490 { lab=VPWR}
N 140 -1560 140 -1490 { lab=VPWR}
N 420 -1560 420 -1480 { lab=VPWR}
N 1810 -1250 1830 -1250 { lab=VSUBS}
N 1830 -1250 1830 -1220 { lab=VSUBS}
N 1810 -1220 1830 -1220 { lab=VSUBS}
N 1460 -1210 1490 -1210 { lab=VSUBS}
N 1460 -1210 1460 -1170 { lab=VSUBS}
N 1460 -1170 1490 -1170 { lab=VSUBS}
N 1490 -1180 1490 -1170 { lab=VSUBS}
N 2070 -1260 2070 -1000 { lab=VSUBS}
N 1820 -1000 2070 -1000 { lab=VSUBS}
N 1820 -1220 1820 -1000 { lab=VSUBS}
N 1760 -1330 1810 -1330 { lab=VSUBS}
N 1760 -1330 1760 -1000 { lab=VSUBS}
N 1760 -1000 1820 -1000 { lab=VSUBS}
N 1440 -1000 1760 -1000 { lab=VSUBS}
N 1470 -1170 1470 -1000 { lab=VSUBS}
N 1440 -1290 1490 -1290 { lab=VSUBS}
N 1440 -1290 1440 -1000 { lab=VSUBS}
N 1320 -1460 1320 -1440 { lab=VPWR}
N 1270 -1440 1320 -1440 { lab=VPWR}
N 1270 -1560 1270 -1440 { lab=VPWR}
N 1300 -1380 1300 -1360 { lab=VSUBS}
N 1240 -1380 1300 -1380 { lab=VSUBS}
N 1240 -1380 1240 -1000 { lab=VSUBS}
N 1240 -1000 1440 -1000 { lab=VSUBS}
N 1150 -1000 1240 -1000 { lab=VSUBS}
N 1150 -1310 1150 -1000 { lab=VSUBS}
N 860 -1450 890 -1450 { lab=VPWR}
N 890 -1510 890 -1450 { lab=VPWR}
N 930 -1460 960 -1460 { lab=VPWR}
N 930 -1510 930 -1460 { lab=VPWR}
N 860 -1000 1160 -1000 { lab=VSUBS}
N 870 -1320 910 -1320 { lab=VSUBS}
N 850 -1240 870 -1240 { lab=VSUBS}
N 850 -1240 850 -1000 { lab=VSUBS}
N 850 -1000 860 -1000 { lab=VSUBS}
N 910 -1320 940 -1320 { lab=VSUBS}
N 940 -1320 940 -1000 { lab=VSUBS}
N 580 -1450 580 -1410 { lab=VPWR}
N 580 -1410 730 -1410 { lab=VPWR}
N 730 -1440 730 -1410 { lab=VPWR}
N 660 -1560 660 -1410 { lab=VPWR}
N 560 -1370 560 -1350 { lab=VSUBS}
N 560 -1370 710 -1370 { lab=VSUBS}
N 710 -1370 710 -1340 { lab=VSUBS}
N 640 -1370 640 -1000 { lab=VSUBS}
N 640 -1000 850 -1000 { lab=VSUBS}
N 850 -1210 870 -1210 { lab=VSUBS}
N 420 -1300 420 -1000 { lab=VSUBS}
N 420 -1000 640 -1000 { lab=VSUBS}
N -30 -1310 -30 -1010 { lab=VSUBS}
N -30 -1010 -30 -1000 { lab=VSUBS}
N -30 -1000 420 -1000 { lab=VSUBS}
N 150 -1310 150 -1000 { lab=VSUBS}
N 1270 -1560 1490 -1560 { lab=VPWR}
N 1140 -1560 1270 -1560 { lab=VPWR}
N 1860 -1400 1860 -1370 { lab=#net10}
N 1860 -1370 2000 -1370 { lab=#net10}
N 1610 -1370 1860 -1370 { lab=#net10}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 400 -1450 0 0 {name=M1
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 400 -1330 0 0 {name=M2
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 580 -1470 1 0 {name=M3
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 730 -1460 1 0 {name=M4
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 560 -1330 3 0 {name=M5
L=0.15
W=0.36
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 710 -1320 3 0 {name=M6
L=0.15
W=0.36
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 840 -1450 0 0 {name=M7
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 980 -1460 2 0 {name=M8
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 850 -1320 0 0 {name=M9
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 890 -1240 2 0 {name=M10
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 1120 -1460 0 0 {name=M11
L=0.15
W=0.84
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 1120 -1340 0 0 {name=M12
L=0.15
W=0.64
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 1320 -1480 1 0 {name=M13
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 1300 -1340 3 0 {name=M14
L=0.15
W=0.36
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 1510 -1470 2 0 {name=M15
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 1510 -1390 2 0 {name=M16
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 1510 -1290 2 0 {name=M17
L=0.15
W=0.36
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 1510 -1210 2 0 {name=M18
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 1890 -1470 2 0 {name=M19
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 1730 -1460 0 0 {name=M20
L=0.15
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 1830 -1330 2 0 {name=M21
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 1790 -1250 0 0 {name=M22
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 2050 -1410 0 0 {name=M23
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 2050 -1290 0 0 {name=M24
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -50 -1460 0 0 {name=M25
L=0.15
W=0.64
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -50 -1340 0 0 {name=M26
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 120 -1460 0 0 {name=M27
L=0.15
W=0.64
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 120 -1340 0 0 {name=M28
L=0.15
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {ipin.sym} -100 -1400 0 0 {name=p1 lab=CLK_IN}
C {lab_pin.sym} 20 -1400 1 0 {name=l1 sig_type=std_logic lab=clkb}
C {lab_pin.sym} 730 -1480 1 0 {name=l2 sig_type=std_logic lab=clkb}
C {lab_pin.sym} 560 -1310 3 0 {name=l3 sig_type=std_logic lab=clkb}
C {lab_pin.sym} 1320 -1500 1 0 {name=l4 sig_type=std_logic lab=clkb}
C {lab_pin.sym} 1530 -1290 2 0 {name=l5 sig_type=std_logic lab=clkb}
C {lab_pin.sym} 140 -1400 2 0 {name=l6 sig_type=std_logic lab=clk}
C {lab_pin.sym} 580 -1490 1 0 {name=l7 sig_type=std_logic lab=clk}
C {lab_pin.sym} 710 -1300 3 0 {name=l8 sig_type=std_logic lab=clk}
C {lab_pin.sym} 1300 -1320 3 0 {name=l9 sig_type=std_logic lab=clk}
C {lab_pin.sym} 1530 -1390 2 0 {name=l10 sig_type=std_logic lab=clk}
C {ipin.sym} 350 -1390 0 0 {name=p2 lab=D}
C {opin.sym} 2070 -1350 0 0 {name=p3 lab=Q}
C {ipin.sym} 1000 -1370 0 0 {name=p4 lab=RESET_B}
C {ipin.sym} -30 -1550 0 0 {name=p5 lab=VPWR}
C {ipin.sym} -30 -1010 0 0 {name=p6 lab=VSUBS}
