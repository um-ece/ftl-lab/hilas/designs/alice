v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 1100 -340 1100 -310 { lab=VPWR}
N 650 -340 1100 -340 { lab=VPWR}
N 650 -340 650 -310 { lab=VPWR}
N 680 -330 680 -310 { lab=#net1}
N 680 -330 1130 -330 { lab=#net1}
N 1130 -330 1130 -310 { lab=#net1}
N 620 -350 620 -310 { lab=VINJ}
N 620 -350 1070 -350 { lab=VINJ}
N 1070 -350 1070 -310 { lab=VINJ}
N 730 -210 790 -210 { lab=#net2}
N 730 -180 790 -180 { lab=#net3}
N 420 -370 420 -310 { lab=VTUN}
N 420 -370 870 -370 { lab=VTUN}
N 870 -370 870 -310 { lab=VTUN}
N 730 -240 790 -240 { lab=#net4}
N 340 -420 340 -270 { lab=VOUT1}
N 340 -420 1230 -420 { lab=VOUT1}
N 1230 -420 1230 -240 { lab=VOUT1}
N 1180 -240 1230 -240 { lab=VOUT1}
N 590 -350 620 -350 { lab=VINJ}
N 320 -240 340 -240 { lab=VIN}
N 320 -210 340 -210 { lab=DRAIN1}
N 320 -180 340 -180 { lab=DRAIN2}
N 720 -310 720 -280 { lab=#net5}
N 720 -280 730 -280 { lab=#net5}
N 730 -280 730 -270 { lab=#net5}
N 320 -330 390 -330 { lab=VINbuff1}
N 390 -330 390 -310 { lab=VINbuff1}
N 440 -450 490 -450 { lab=COLSEL1}
N 490 -450 490 -310 { lab=COLSEL1}
N 450 -390 450 -310 { lab=GATE1}
N 410 -390 450 -390 { lab=GATE1}
N 470 -500 540 -500 { lab=GATE2}
N 540 -500 540 -310 { lab=GATE2}
N 540 -540 580 -540 { lab=COLSEL2}
N 580 -540 580 -310 { lab=COLSEL2}
N 1180 -300 1180 -270 { lab=#net6}
N 770 -410 840 -410 { lab=VINbuff2}
N 840 -410 840 -390 { lab=VINbuff2}
N 890 -530 940 -530 { lab=COLSEL3}
N 940 -530 940 -390 { lab=COLSEL3}
N 900 -470 900 -390 { lab=GATE3}
N 860 -470 900 -470 { lab=GATE3}
N 920 -580 990 -580 { lab=GATE3}
N 990 -580 990 -390 { lab=GATE3}
N 990 -620 1030 -620 { lab=COLSEL4}
N 1030 -620 1030 -390 { lab=COLSEL4}
N 840 -390 840 -310 { lab=VINbuff2}
N 1230 -240 1270 -240 { lab=VOUT1}
N 750 -270 790 -270 { lab=#net7}
N 750 -270 750 -100 { lab=#net7}
N 900 -390 900 -310 { lab=GATE3}
N 940 -390 940 -310 { lab=COLSEL3}
N 990 -390 990 -310 { lab=GATE3}
N 1030 -390 1030 -310 { lab=COLSEL4}
N 640 -390 650 -390 { lab=VPWR}
N 650 -390 650 -340 { lab=VPWR}
N 680 -440 680 -330 { lab=#net1}
N 310 -370 420 -370 { lab=VTUN}
C {devices/iopin.sym} 600 -350 2 0 {name=p1 lab=VINJ}
C {devices/iopin.sym} 330 -240 2 0 {name=p2 lab=VIN}
C {devices/iopin.sym} 330 -210 2 0 {name=p3 lab=DRAIN1
}
C {devices/iopin.sym} 330 -180 2 0 {name=p4 lab=DRAIN2}
C {devices/iopin.sym} 330 -330 2 0 {name=p5 lab=VINbuff1}
C {devices/iopin.sym} 710 -310 0 0 {name=p6 lab=VOUTbuff1}
C {devices/iopin.sym} 420 -390 2 0 {name=p7 lab=GATE1}
C {devices/iopin.sym} 450 -450 2 0 {name=p8 lab=COLSEL1}
C {devices/iopin.sym} 480 -500 2 0 {name=p9 lab=GATE2}
C {devices/iopin.sym} 550 -540 2 0 {name=p10 lab=COLSEL2}
C {devices/iopin.sym} 1170 -300 0 0 {name=p11 lab=VOUTbuff2}
C {devices/iopin.sym} 780 -410 2 0 {name=p13 lab=VINbuff2}
C {devices/iopin.sym} 870 -470 2 0 {name=p14 lab=GATE3}
C {devices/iopin.sym} 900 -530 2 0 {name=p15 lab=COLSEL3}
C {devices/iopin.sym} 930 -580 2 0 {name=p16 lab=GATE3}
C {devices/iopin.sym} 1000 -620 2 0 {name=p17 lab=COLSEL4}
C {devices/iopin.sym} 760 -100 2 0 {name=p12 lab=Voutd2}
C {devices/iopin.sym} 1260 -240 0 0 {name=p18 lab=VOUT1}
C {devices/iopin.sym} 650 -390 2 0 {name=p19 lab=VPWR}
C {devices/iopin.sym} 690 -440 2 0 {name=p20 lab=VGND}
C {devices/iopin.sym} 320 -370 2 0 {name=p21 lab=VTUN}
C {sky130_hilas_Alice_Delayblock_TA.sym} 210 -40 0 0 {name=x1}
C {sky130_hilas_Alice_Delayblock_TA.sym} 660 -40 0 0 {name=x2}
