v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 220 110 230 110 { lab=INPUT}
N 220 110 220 220 { lab=INPUT}
N 220 220 240 220 { lab=INPUT}
N 290 110 320 110 { lab=OUTPUT}
N 320 110 320 220 { lab=OUTPUT}
N 300 220 320 220 { lab=OUTPUT}
N 110 150 110 200 { lab=#net1}
N 40 120 70 120 { lab=SELECT}
N 40 120 40 230 { lab=SELECT}
N 40 230 70 230 { lab=SELECT}
N 110 120 130 120 { lab=SELECT}
N 110 80 110 90 { lab=SELECT}
N 260 110 260 120 { lab=SELECT}
N 110 230 130 230 { lab=VGND}
N 130 230 130 270 { lab=VGND}
N 110 270 130 270 { lab=VGND}
N 110 260 110 270 { lab=VGND}
N 270 210 270 220 { lab=VGND}
N 130 210 270 210 { lab=VGND}
N 130 210 130 230 { lab=VGND}
N 110 180 180 180 { lab=#net1}
N 180 180 180 270 { lab=#net1}
N 180 270 270 270 { lab=#net1}
N 270 260 270 270 { lab=#net1}
N 40 60 40 120 { lab=SELECT}
N 40 60 260 60 { lab=SELECT}
N 260 60 260 70 { lab=SELECT}
N 130 120 130 130 { lab=SELECT}
N 110 80 130 80 { lab=SELECT}
N 130 80 130 120 { lab=SELECT}
N 130 130 260 130 { lab=SELECT}
N 260 120 260 130 { lab=SELECT}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 270 240 3 0 {name=M1
L=0.5
W=50
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 260 90 1 0 {name=M2
L=0.5
W=50
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 40 180 0 0 {name=p1 lab=SELECT}
C {ipin.sym} 110 270 0 0 {name=p3 lab=VGND}
C {ipin.sym} 220 160 0 0 {name=p4 lab=INPUT}
C {opin.sym} 320 160 0 0 {name=p5 lab=OUTPUT}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/pfet_g5v0d10v5.sym} 90 120 0 0 {name=M4
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 90 230 0 0 {name=M3
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 110 80 0 0 {name=p2 lab=VINJ}
