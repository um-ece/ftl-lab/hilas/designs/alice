v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 2330 -860 2330 -820 { lab=#net1}
N 2210 -820 2210 -770 { lab=#net1}
N 2210 -710 2210 -680 { lab=#net2}
N 2210 -860 2210 -820 { lab=#net1}
N 2090 -830 2210 -830 { lab=#net1}
N 2210 -820 2330 -820 { lab=#net1}
N 2090 -860 2090 -830 { lab=#net1}
N 2210 -620 2210 -580 { lab=#net3}
N 2210 -520 2210 -490 { lab=GND}
N 2090 -950 2090 -920 { lab=VDD}
N 2090 -950 2110 -950 { lab=VDD}
N 2110 -950 2330 -950 { lab=VDD}
N 2330 -950 2330 -920 { lab=VDD}
N 2210 -950 2210 -920 { lab=VDD}
N 2210 -550 2240 -550 { lab=GND}
N 2240 -550 2240 -500 { lab=GND}
N 2210 -500 2240 -500 { lab=GND}
N 2210 -650 2240 -650 { lab=GND}
N 2240 -650 2240 -550 { lab=GND}
N 2210 -740 2240 -740 { lab=GND}
N 2240 -740 2240 -650 { lab=GND}
N 2210 -890 2230 -890 { lab=VDD}
N 2230 -950 2230 -890 { lab=VDD}
N 2090 -890 2110 -890 { lab=VDD}
N 2110 -950 2110 -890 { lab=VDD}
N 2330 -890 2350 -890 { lab=VDD}
N 2350 -950 2350 -890 { lab=VDD}
N 2330 -950 2350 -950 { lab=VDD}
N 2150 -740 2170 -740 { lab=A}
N 2150 -650 2170 -650 { lab=B}
N 2150 -550 2170 -550 { lab=C}
N 2030 -890 2050 -890 { lab=A}
N 2150 -890 2170 -890 { lab=B}
N 2270 -890 2290 -890 { lab=C}
N 2210 -490 2210 -470 { lab=GND}
N 2180 -990 2180 -950 { lab=VDD}
N 2470 -780 2470 -720 { lab=M0}
N 2390 -810 2430 -810 { lab=#net1}
N 2390 -810 2390 -690 { lab=#net1}
N 2390 -690 2430 -690 { lab=#net1}
N 2210 -790 2390 -790 { lab=#net1}
N 2470 -950 2470 -840 { lab=VDD}
N 2350 -950 2470 -950 { lab=VDD}
N 2470 -660 2470 -500 { lab=GND}
N 2240 -500 2470 -500 { lab=GND}
N 2470 -750 2550 -750 { lab=M0}
N 3110 -880 3110 -840 { lab=#net4}
N 2990 -840 2990 -790 { lab=#net4}
N 2990 -730 2990 -700 { lab=#net5}
N 2990 -880 2990 -840 { lab=#net4}
N 2870 -850 2990 -850 { lab=#net4}
N 2990 -840 3110 -840 { lab=#net4}
N 2870 -880 2870 -850 { lab=#net4}
N 2990 -640 2990 -600 { lab=#net6}
N 2990 -540 2990 -510 { lab=GND}
N 2870 -970 2870 -940 { lab=VDD}
N 2870 -970 2890 -970 { lab=VDD}
N 2890 -970 3110 -970 { lab=VDD}
N 3110 -970 3110 -940 { lab=VDD}
N 2990 -970 2990 -940 { lab=VDD}
N 2990 -570 3020 -570 { lab=GND}
N 3020 -570 3020 -520 { lab=GND}
N 2990 -520 3020 -520 { lab=GND}
N 2990 -670 3020 -670 { lab=GND}
N 3020 -670 3020 -570 { lab=GND}
N 2990 -760 3020 -760 { lab=GND}
N 3020 -760 3020 -670 { lab=GND}
N 2990 -910 3010 -910 { lab=VDD}
N 3010 -970 3010 -910 { lab=VDD}
N 2870 -910 2890 -910 { lab=VDD}
N 2890 -970 2890 -910 { lab=VDD}
N 3110 -910 3130 -910 { lab=VDD}
N 3130 -970 3130 -910 { lab=VDD}
N 3110 -970 3130 -970 { lab=VDD}
N 2930 -760 2950 -760 { lab=#net7}
N 2930 -670 2950 -670 { lab=A}
N 2930 -570 2950 -570 { lab=C}
N 2810 -910 2830 -910 { lab=A}
N 2930 -910 2950 -910 { lab=#net7}
N 3050 -910 3070 -910 { lab=C}
N 2990 -510 2990 -490 { lab=GND}
N 2960 -1010 2960 -970 { lab=VDD}
N 3250 -800 3250 -740 { lab=M1}
N 3170 -830 3210 -830 { lab=#net4}
N 3170 -830 3170 -710 { lab=#net4}
N 3170 -710 3210 -710 { lab=#net4}
N 2990 -810 3170 -810 { lab=#net4}
N 3250 -970 3250 -860 { lab=VDD}
N 3130 -970 3250 -970 { lab=VDD}
N 3250 -680 3250 -520 { lab=GND}
N 3020 -520 3250 -520 { lab=GND}
N 3250 -770 3330 -770 { lab=M1}
N 2780 -690 2780 -630 { lab=#net7}
N 2700 -720 2740 -720 { lab=B}
N 2700 -720 2700 -600 { lab=B}
N 2700 -600 2740 -600 { lab=B}
N 2780 -970 2780 -750 { lab=VDD}
N 2780 -970 2870 -970 { lab=VDD}
N 2780 -570 2780 -510 { lab=GND}
N 2780 -510 2990 -510 { lab=GND}
N 2470 -690 2510 -690 { lab=GND}
N 2510 -690 2510 -630 { lab=GND}
N 2470 -630 2510 -630 { lab=GND}
N 2470 -810 2510 -810 { lab=VDD}
N 2510 -870 2510 -810 { lab=VDD}
N 2470 -870 2510 -870 { lab=VDD}
N 2460 -220 2460 -180 { lab=#net8}
N 2340 -180 2340 -130 { lab=#net8}
N 2340 -70 2340 -40 { lab=#net9}
N 2340 -220 2340 -180 { lab=#net8}
N 2220 -190 2340 -190 { lab=#net8}
N 2340 -180 2460 -180 { lab=#net8}
N 2220 -220 2220 -190 { lab=#net8}
N 2340 20 2340 60 { lab=#net10}
N 2340 120 2340 150 { lab=GND}
N 2220 -310 2220 -280 { lab=VDD}
N 2220 -310 2240 -310 { lab=VDD}
N 2240 -310 2460 -310 { lab=VDD}
N 2460 -310 2460 -280 { lab=VDD}
N 2340 -310 2340 -280 { lab=VDD}
N 2340 90 2370 90 { lab=GND}
N 2370 90 2370 140 { lab=GND}
N 2340 140 2370 140 { lab=GND}
N 2340 -10 2370 -10 { lab=GND}
N 2370 -10 2370 90 { lab=GND}
N 2340 -100 2370 -100 { lab=GND}
N 2370 -100 2370 -10 { lab=GND}
N 2340 -250 2360 -250 { lab=VDD}
N 2360 -310 2360 -250 { lab=VDD}
N 2220 -250 2240 -250 { lab=VDD}
N 2240 -310 2240 -250 { lab=VDD}
N 2460 -250 2480 -250 { lab=VDD}
N 2480 -310 2480 -250 { lab=VDD}
N 2460 -310 2480 -310 { lab=VDD}
N 2280 -100 2300 -100 { lab=#net11}
N 2280 -10 2300 -10 { lab=B}
N 2280 90 2300 90 { lab=C}
N 2160 -250 2180 -250 { lab=#net11}
N 2280 -250 2300 -250 { lab=B}
N 2400 -250 2420 -250 { lab=C}
N 2340 150 2340 170 { lab=GND}
N 2310 -350 2310 -310 { lab=VDD}
N 2600 -140 2600 -80 { lab=M2}
N 2520 -170 2560 -170 { lab=#net8}
N 2520 -170 2520 -50 { lab=#net8}
N 2520 -50 2560 -50 { lab=#net8}
N 2340 -150 2520 -150 { lab=#net8}
N 2600 -310 2600 -200 { lab=VDD}
N 2480 -310 2600 -310 { lab=VDD}
N 2600 -20 2600 140 { lab=GND}
N 2370 140 2600 140 { lab=GND}
N 2600 -110 2680 -110 { lab=M2}
N 2130 -30 2130 30 { lab=#net11}
N 2050 -60 2090 -60 { lab=A}
N 2050 -60 2050 60 { lab=A}
N 2050 60 2090 60 { lab=A}
N 2130 -310 2130 -90 { lab=VDD}
N 2130 -310 2220 -310 { lab=VDD}
N 2130 90 2130 150 { lab=GND}
N 2130 150 2340 150 { lab=GND}
N 3340 -220 3340 -180 { lab=#net12}
N 3220 -180 3220 -130 { lab=#net12}
N 3220 -70 3220 -40 { lab=#net13}
N 3220 -220 3220 -180 { lab=#net12}
N 3100 -190 3220 -190 { lab=#net12}
N 3220 -180 3340 -180 { lab=#net12}
N 3100 -220 3100 -190 { lab=#net12}
N 3220 20 3220 60 { lab=#net14}
N 3220 120 3220 150 { lab=GND}
N 3100 -310 3100 -280 { lab=VDD}
N 3100 -310 3120 -310 { lab=VDD}
N 3120 -310 3340 -310 { lab=VDD}
N 3340 -310 3340 -280 { lab=VDD}
N 3220 -310 3220 -280 { lab=VDD}
N 3220 90 3250 90 { lab=GND}
N 3250 90 3250 140 { lab=GND}
N 3220 140 3250 140 { lab=GND}
N 3220 -10 3250 -10 { lab=GND}
N 3250 -10 3250 90 { lab=GND}
N 3220 -100 3250 -100 { lab=GND}
N 3250 -100 3250 -10 { lab=GND}
N 3220 -250 3240 -250 { lab=VDD}
N 3240 -310 3240 -250 { lab=VDD}
N 3100 -250 3120 -250 { lab=VDD}
N 3120 -310 3120 -250 { lab=VDD}
N 3340 -250 3360 -250 { lab=VDD}
N 3360 -310 3360 -250 { lab=VDD}
N 3340 -310 3360 -310 { lab=VDD}
N 3160 -100 3180 -100 { lab=#net15}
N 3160 -10 3180 -10 { lab=#net16}
N 3160 90 3180 90 { lab=C}
N 3040 -250 3060 -250 { lab=#net15}
N 3160 -250 3180 -250 { lab=#net16}
N 3280 -250 3300 -250 { lab=C}
N 3220 150 3220 170 { lab=GND}
N 3190 -350 3190 -310 { lab=VDD}
N 3480 -140 3480 -80 { lab=M3}
N 3400 -170 3440 -170 { lab=#net12}
N 3400 -170 3400 -50 { lab=#net12}
N 3400 -50 3440 -50 { lab=#net12}
N 3220 -150 3400 -150 { lab=#net12}
N 3480 -310 3480 -200 { lab=VDD}
N 3360 -310 3480 -310 { lab=VDD}
N 3480 -20 3480 140 { lab=GND}
N 3250 140 3480 140 { lab=GND}
N 3480 -110 3560 -110 { lab=M3}
N 3480 -50 3520 -50 { lab=GND}
N 3520 -50 3520 10 { lab=GND}
N 3480 10 3520 10 { lab=GND}
N 3480 -170 3520 -170 { lab=VDD}
N 3520 -230 3520 -170 { lab=VDD}
N 3480 -230 3520 -230 { lab=VDD}
N 2130 -60 2150 -60 { lab=VDD}
N 2150 -120 2150 -60 { lab=VDD}
N 2130 -120 2150 -120 { lab=VDD}
N 2130 60 2150 60 { lab=GND}
N 2150 60 2150 120 { lab=GND}
N 2130 120 2150 120 { lab=GND}
N 2780 -720 2800 -720 { lab=VDD}
N 2800 -770 2800 -720 { lab=VDD}
N 2780 -770 2800 -770 { lab=VDD}
N 2780 -600 2810 -600 { lab=GND}
N 2810 -600 2810 -540 { lab=GND}
N 2780 -540 2810 -540 { lab=GND}
N 3250 -830 3270 -830 { lab=VDD}
N 3270 -890 3270 -830 { lab=VDD}
N 3250 -890 3270 -890 { lab=VDD}
N 3250 -710 3280 -710 { lab=GND}
N 3280 -710 3280 -640 { lab=GND}
N 3250 -640 3280 -640 { lab=GND}
N 2780 -670 2860 -670 { lab=#net7}
N 2860 -760 2860 -670 { lab=#net7}
N 2860 -760 2930 -760 { lab=#net7}
N 2130 -10 2200 -10 { lab=#net11}
N 2200 -100 2200 -10 { lab=#net11}
N 2200 -100 2280 -100 { lab=#net11}
N 2600 -170 2640 -170 { lab=VDD}
N 2640 -240 2640 -170 { lab=VDD}
N 2600 -240 2640 -240 { lab=VDD}
N 2600 -50 2630 -50 { lab=GND}
N 2630 -50 2630 20 { lab=GND}
N 2600 20 2630 20 { lab=GND}
N 2920 -180 2920 -120 { lab=#net15}
N 2840 -210 2880 -210 { lab=A}
N 2840 -210 2840 -90 { lab=A}
N 2840 -90 2880 -90 { lab=A}
N 2920 -210 2940 -210 { lab=VDD}
N 2940 -260 2940 -210 { lab=VDD}
N 2920 -260 2940 -260 { lab=VDD}
N 2920 120 2920 180 { lab=#net16}
N 2840 90 2880 90 { lab=B}
N 2840 90 2840 210 { lab=B}
N 2840 210 2880 210 { lab=B}
N 2920 90 2940 90 { lab=VDD}
N 2940 40 2940 90 { lab=VDD}
N 2920 210 2950 210 { lab=GND}
N 2950 210 2950 270 { lab=GND}
N 2920 270 2950 270 { lab=GND}
N 2920 -300 2920 -240 { lab=VDD}
N 2920 -300 3100 -300 { lab=VDD}
N 2160 -250 2160 -140 { lab=#net11}
N 2160 -140 2200 -140 { lab=#net11}
N 2200 -140 2220 -140 { lab=#net11}
N 2220 -140 2220 -100 { lab=#net11}
N 2930 -910 2930 -760 { lab=#net7}
N 2920 -150 3020 -150 { lab=#net15}
N 3020 -250 3020 -150 { lab=#net15}
N 3020 -250 3040 -250 { lab=#net15}
N 3020 -100 3160 -100 { lab=#net15}
N 3020 -150 3020 -100 { lab=#net15}
N 2940 -210 2940 40 { lab=VDD}
N 2920 -90 2960 -90 { lab=GND}
N 2960 -90 2960 210 { lab=GND}
N 2950 210 2960 210 { lab=GND}
N 2920 30 2920 60 { lab=VDD}
N 2920 30 2940 30 { lab=VDD}
N 2920 -60 2920 -30 { lab=GND}
N 2920 -30 2960 -30 { lab=GND}
N 2920 140 3020 140 { lab=#net16}
N 3020 -10 3020 140 { lab=#net16}
N 3020 -10 3160 -10 { lab=#net16}
N 3150 -250 3160 -250 { lab=#net16}
N 3150 -250 3150 -120 { lab=#net16}
N 3150 -120 3150 -60 { lab=#net16}
N 3150 -60 3150 -10 { lab=#net16}
N 2950 270 3310 270 { lab=GND}
N 3310 140 3310 270 { lab=GND}
N 2920 240 2920 270 { lab=GND}
C {sky130_fd_pr/nfet_01v8.sym} 2190 -650 0 0 {name=M1
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2190 -890 0 0 {name=M2
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2070 -890 0 0 {name=M3
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2310 -890 0 0 {name=M4
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2190 -740 0 0 {name=M5
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2190 -550 0 0 {name=M6
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 2150 -740 0 0 {name=l1 sig_type=std_logic lab=A}
C {devices/lab_pin.sym} 2150 -650 0 0 {name=l2 sig_type=std_logic lab=B}
C {devices/lab_pin.sym} 2150 -550 0 0 {name=l3 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 2030 -890 0 0 {name=l4 sig_type=std_logic lab=A}
C {devices/lab_pin.sym} 2150 -890 0 0 {name=l5 sig_type=std_logic lab=B}
C {devices/lab_pin.sym} 2270 -890 0 0 {name=l6 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 2210 -470 0 0 {name=l7 sig_type=std_logic lab=GND}
C {devices/lab_pin.sym} 2180 -990 0 0 {name=l8 sig_type=std_logic lab=VDD}
C {sky130_fd_pr/nfet_01v8.sym} 2450 -690 0 0 {name=M7
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2450 -810 0 0 {name=M8
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {devices/lab_pin.sym} 2550 -750 2 0 {name=l9 sig_type=std_logic lab=M0}
C {sky130_fd_pr/nfet_01v8.sym} 2970 -670 0 0 {name=M9
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2970 -910 0 0 {name=M10
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2850 -910 0 0 {name=M11
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3090 -910 0 0 {name=M12
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2970 -760 0 0 {name=M13
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2970 -570 0 0 {name=M14
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 2930 -670 0 0 {name=l10 sig_type=std_logic lab=A}
C {devices/lab_pin.sym} 2700 -660 0 0 {name=l11 sig_type=std_logic lab=B}
C {devices/lab_pin.sym} 2930 -570 0 0 {name=l12 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 2810 -910 0 0 {name=l13 sig_type=std_logic lab=A}
C {devices/lab_pin.sym} 3050 -910 0 0 {name=l15 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 2990 -490 0 0 {name=l16 sig_type=std_logic lab=GND}
C {devices/lab_pin.sym} 2960 -1010 0 0 {name=l17 sig_type=std_logic lab=VDD}
C {sky130_fd_pr/nfet_01v8.sym} 3230 -710 0 0 {name=M15
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3230 -830 0 0 {name=M16
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {devices/lab_pin.sym} 3330 -770 2 0 {name=l18 sig_type=std_logic lab=M1}
C {sky130_fd_pr/nfet_01v8.sym} 2760 -600 0 0 {name=M17
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2760 -720 0 0 {name=M18
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2320 -10 0 0 {name=M19
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2320 -250 0 0 {name=M20
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2200 -250 0 0 {name=M21
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2440 -250 0 0 {name=M22
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2320 -100 0 0 {name=M23
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2320 90 0 0 {name=M24
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 2050 0 0 0 {name=l19 sig_type=std_logic lab=A}
C {devices/lab_pin.sym} 2280 -10 0 0 {name=l20 sig_type=std_logic lab=B}
C {devices/lab_pin.sym} 2280 90 0 0 {name=l21 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 2280 -250 0 0 {name=l23 sig_type=std_logic lab=B}
C {devices/lab_pin.sym} 2400 -250 0 0 {name=l24 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 2340 170 0 0 {name=l25 sig_type=std_logic lab=GND}
C {devices/lab_pin.sym} 2310 -350 0 0 {name=l26 sig_type=std_logic lab=VDD}
C {sky130_fd_pr/nfet_01v8.sym} 2580 -50 0 0 {name=M25
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2580 -170 0 0 {name=M26
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {devices/lab_pin.sym} 2680 -110 2 0 {name=l27 sig_type=std_logic lab=M2}
C {sky130_fd_pr/nfet_01v8.sym} 2110 60 0 0 {name=M27
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2110 -60 0 0 {name=M28
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3200 -10 0 0 {name=M29
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3200 -250 0 0 {name=M30
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3080 -250 0 0 {name=M31
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3320 -250 0 0 {name=M32
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3200 -100 0 0 {name=M33
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3200 90 0 0 {name=M34
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 2840 -150 0 0 {name=l28 sig_type=std_logic lab=A}
C {devices/lab_pin.sym} 3160 90 0 0 {name=l30 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 3280 -250 0 0 {name=l33 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 3220 170 0 0 {name=l34 sig_type=std_logic lab=GND}
C {devices/lab_pin.sym} 3190 -350 0 0 {name=l35 sig_type=std_logic lab=VDD}
C {sky130_fd_pr/nfet_01v8.sym} 3460 -50 0 0 {name=M35
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3460 -170 0 0 {name=M36
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {devices/lab_pin.sym} 3560 -110 2 0 {name=l36 sig_type=std_logic lab=M3}
C {sky130_fd_pr/nfet_01v8.sym} 2900 -90 0 0 {name=M37
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 2900 -210 0 0 {name=M38
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {devices/lab_pin.sym} 2840 150 0 0 {name=l38 sig_type=std_logic lab=B}
C {sky130_fd_pr/nfet_01v8.sym} 2900 210 0 0 {name=M39
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2900 90 0 0 {name=M40
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
