v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 410 -80 450 -80 { lab=#net1}
N 410 -80 410 30 { lab=#net1}
N 510 -80 510 30 { lab=SIGOUT}
N 490 30 510 30 { lab=SIGOUT}
N 410 30 430 30 { lab=#net1}
N 230 -180 270 -180 { lab=VGSEL}
N 230 -180 230 -70 { lab=VGSEL}
N 330 -180 330 -70 { lab=#net1}
N 310 -70 330 -70 { lab=#net1}
N 230 -70 250 -70 { lab=VGSEL}
N 240 90 280 90 { lab=VGUNSEL}
N 240 90 240 200 { lab=VGUNSEL}
N 340 90 340 200 { lab=#net1}
N 320 200 340 200 { lab=#net1}
N 240 200 260 200 { lab=VGUNSEL}
N 700 -100 700 -90 { lab=SIGIN}
N 700 -100 810 -100 { lab=SIGIN}
N 700 -30 810 -30 { lab=SIGOUT}
N 810 -40 810 -30 { lab=SIGOUT}
N 750 -250 750 -100 { lab=SIGIN}
N 510 0 750 -0 { lab=SIGOUT}
N 750 -30 750 -0 { lab=SIGOUT}
N 750 0 750 140 { lab=SIGOUT}
N 330 -110 370 -110 { lab=#net1}
N 370 -110 370 -20 { lab=#net1}
N 370 -20 410 -20 { lab=#net1}
N 340 140 370 140 { lab=#net1}
N 370 -20 370 140 { lab=#net1}
N 70 -130 220 -130 { lab=VGSEL}
N 220 -130 230 -130 { lab=VGSEL}
N 80 150 240 150 { lab=VGUNSEL}
N 300 -180 300 -160 { lab=VINJ}
N 300 -160 790 -160 { lab=VINJ}
N 790 -160 790 -70 { lab=VINJ}
N 790 -70 810 -70 { lab=VINJ}
N 480 -80 480 -50 { lab=VINJ}
N 480 -50 560 -50 { lab=VINJ}
N 560 -160 560 -50 { lab=VINJ}
N 610 -160 610 90 { lab=VINJ}
N 280 -90 280 -70 { lab=VGND}
N 180 -90 280 -90 { lab=VGND}
N 180 -90 180 280 { lab=VGND}
N 180 280 700 280 { lab=VGND}
N 700 280 710 280 { lab=VGND}
N 710 -60 710 280 { lab=VGND}
N 700 -60 710 -60 { lab=VGND}
N 460 10 460 30 { lab=VGND}
N 400 10 460 10 { lab=VGND}
N 400 10 400 280 { lab=VGND}
N 290 180 290 200 { lab=VGND}
N 290 180 360 180 { lab=VGND}
N 360 180 360 280 { lab=VGND}
N 310 90 310 100 { lab=VINJ}
N 310 100 610 100 { lab=VINJ}
N 610 90 610 100 { lab=VINJ}
N 280 -30 280 10 { lab=SelControl}
N 280 10 310 10 { lab=SelControl}
N 310 10 310 50 { lab=SelControl}
N 140 10 280 10 { lab=SelControl}
N 460 70 870 70 { lab=PROG}
N 870 -70 870 70 { lab=PROG}
N 850 -70 870 -70 { lab=PROG}
N 480 -120 660 -120 { lab=RUN}
N 660 -120 660 -60 { lab=RUN}
N 660 -120 870 -120 { lab=RUN}
N 140 -220 140 10 { lab=SelControl}
N 110 -220 140 -220 { lab=SelControl}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 480 -100 1 0 {name=M1
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 460 50 3 0 {name=M6
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 300 -200 1 0 {name=M2
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 280 -50 3 0 {name=M3
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 310 70 1 0 {name=M4
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 290 220 3 0 {name=M5
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 830 -70 2 0 {name=M7
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 680 -60 0 0 {name=M8
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 70 -130 0 0 {name=p1 lab=VGSEL}
C {ipin.sym} 80 150 0 0 {name=p2 lab=VGUNSEL}
C {ipin.sym} 110 -220 0 0 {name=p3 lab=SelControl}
C {ipin.sym} 290 240 0 0 {name=p4 lab=SelControlB}
C {ipin.sym} 300 -220 0 0 {name=p5 lab=SelControlB}
C {ipin.sym} 870 -50 2 0 {name=p6 lab=PROG}
C {ipin.sym} 870 -120 2 0 {name=p7 lab=RUN}
C {ipin.sym} 750 -250 0 0 {name=p8 lab=SIGIN}
C {opin.sym} 750 140 0 0 {name=p9 lab=SIGOUT}
C {ipin.sym} 560 -140 0 0 {name=p10 lab=VINJ}
C {ipin.sym} 710 250 0 0 {name=p11 lab=VGND}
