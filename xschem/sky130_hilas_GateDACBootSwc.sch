v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 10 -60 10 -50 { lab=VGSEL}
N 10 -60 100 -60 { lab=VGSEL}
N 100 0 100 10 { lab=VGUNSEL}
N 10 10 100 10 { lab=VGUNSEL}
N 10 50 10 60 { lab=VGUNSEL}
N 10 50 100 50 { lab=VGUNSEL}
N 100 110 100 120 { lab=VINJ}
N 10 120 100 120 { lab=VINJ}
N 230 -60 230 -50 { lab=VGSEL}
N 230 -60 320 -60 { lab=VGSEL}
N 320 0 320 10 { lab=DAC}
N 230 10 320 10 { lab=DAC}
N 240 50 240 60 { lab=VGUNSEL}
N 240 50 330 50 { lab=VGUNSEL}
N 330 110 330 120 { lab=#net1}
N 240 120 330 120 { lab=#net1}
N 50 10 50 50 { lab=VGUNSEL}
N 60 -80 60 -60 { lab=VGSEL}
N 60 -80 280 -80 { lab=VGSEL}
N 280 -80 280 -60 { lab=VGSEL}
N -100 -80 60 -80 { lab=VGSEL}
N -100 30 50 30 { lab=VGUNSEL}
N 50 30 280 30 { lab=VGUNSEL}
N 280 30 280 50 { lab=VGUNSEL}
N 240 140 240 150 { lab=#net1}
N 240 140 330 140 { lab=#net1}
N 330 200 330 210 { lab=VFGBOOT_IN}
N 240 210 330 210 { lab=VFGBOOT_IN}
N 280 120 280 140 { lab=#net1}
N 300 10 300 30 { lab=DAC}
N 300 30 420 30 { lab=DAC}
N 40 120 40 140 { lab=VINJ}
N 40 140 90 140 { lab=VINJ}
N 290 210 290 230 { lab=VFGBOOT_IN}
N 290 230 410 230 { lab=VFGBOOT_IN}
N 140 -120 140 -30 { lab=RUN}
N 370 80 390 80 { lab=RUN}
N 390 -120 390 80 { lab=RUN}
N 140 -120 390 -120 { lab=RUN}
N 180 -150 180 -120 { lab=RUN}
N 40 240 40 260 { lab=#net2}
N -20 210 -0 210 { lab=FGBOOTCONTROL}
N -20 210 -20 290 { lab=FGBOOTCONTROL}
N -20 290 -0 290 { lab=FGBOOTCONTROL}
N 40 250 190 250 { lab=#net2}
N 190 180 190 250 { lab=#net2}
N 190 180 200 180 { lab=#net2}
N 370 170 390 170 { lab=FGBOOTCONTROL}
N 390 170 390 360 { lab=FGBOOTCONTROL}
N -40 360 390 360 { lab=FGBOOTCONTROL}
N -40 260 -40 360 { lab=FGBOOTCONTROL}
N -40 260 -20 260 { lab=FGBOOTCONTROL}
N 40 290 60 290 { lab=VGND}
N 60 290 60 320 { lab=VGND}
N 40 320 60 320 { lab=VGND}
N 40 210 60 210 { lab=VINJ}
N 60 180 60 210 { lab=VINJ}
N 40 180 60 180 { lab=VINJ}
N 140 80 150 80 { lab=PROG}
N 150 80 150 320 { lab=PROG}
N 150 320 180 320 { lab=PROG}
N 150 290 480 290 { lab=PROG}
N 480 -30 480 290 { lab=PROG}
N 360 -30 480 -30 { lab=PROG}
N 170 -20 190 -20 { lab=RUN}
N 170 -120 170 -20 { lab=RUN}
N 180 90 200 90 { lab=PROG}
N 170 90 180 90 { lab=PROG}
N 150 90 170 90 { lab=PROG}
N -70 90 -30 90 { lab=RUN}
N -70 -120 -70 90 { lab=RUN}
N -70 -120 140 -120 { lab=RUN}
N -50 -20 -30 -20 { lab=PROG}
N -50 -20 -50 170 { lab=PROG}
N -50 170 150 170 { lab=PROG}
N 50 140 50 180 { lab=VINJ}
N 10 -20 30 -20 { lab=VINJ}
N 30 -20 30 90 { lab=VINJ}
N 10 90 30 90 { lab=VINJ}
N 30 90 30 120 { lab=VINJ}
N 230 -20 260 -20 { lab=VINJ}
N 260 -20 260 180 { lab=VINJ}
N 240 180 260 180 { lab=VINJ}
N 240 90 260 90 { lab=VINJ}
N 170 130 260 130 { lab=VINJ}
N 170 130 170 140 { lab=VINJ}
N 90 140 170 140 { lab=VINJ}
N 80 -30 100 -30 { lab=VGND}
N 80 -30 80 320 { lab=VGND}
N 60 320 80 320 { lab=VGND}
N 80 80 100 80 { lab=VGND}
N 300 -30 320 -30 { lab=VGND}
N 290 -30 300 -30 { lab=VGND}
N 290 -30 290 170 { lab=VGND}
N 290 170 330 170 { lab=VGND}
N 290 80 330 80 { lab=VGND}
N 310 170 310 340 { lab=VGND}
N 60 340 310 340 { lab=VGND}
N 60 320 60 340 { lab=VGND}
N 280 -80 350 -80 { lab=GateSel_Pin}
N 350 -80 410 -80 {}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/pfet_g5v0d10v5.sym} -10 -20 0 0 {name=M1
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/nfet_g5v0d10v5.sym} 120 -30 2 0 {name=M2
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/pfet_g5v0d10v5.sym} -10 90 0 0 {name=M3
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/nfet_g5v0d10v5.sym} 120 80 2 0 {name=M4
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/pfet_g5v0d10v5.sym} 210 -20 0 0 {name=M5
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/nfet_g5v0d10v5.sym} 340 -30 2 0 {name=M6
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/pfet_g5v0d10v5.sym} 220 90 0 0 {name=M7
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/nfet_g5v0d10v5.sym} 350 80 2 0 {name=M8
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/pfet_g5v0d10v5.sym} 220 180 0 0 {name=M9
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/nfet_g5v0d10v5.sym} 350 170 2 0 {name=M10
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} -100 -80 0 0 {name=p1 lab=VGSEL}
C {ipin.sym} -100 30 0 0 {name=p2 lab=VGUNSEL}
C {ipin.sym} 420 30 2 0 {name=p3 lab=DAC}
C {ipin.sym} 90 140 2 0 {name=p4 lab=VINJ}
C {ipin.sym} 410 230 2 0 {name=p5 lab=VFGBOOT_IN}
C {ipin.sym} 180 -150 2 0 {name=p6 lab=RUN}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/pfet_g5v0d10v5.sym} 20 210 0 0 {name=M11
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/nfet_g5v0d10v5.sym} 20 290 0 0 {name=M12
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 390 310 2 0 {name=p7 lab=FGBOOTCONTROL}
C {ipin.sym} 180 320 2 0 {name=p8 lab=PROG}
C {ipin.sym} 310 340 2 0 {name=p9 lab=VGND}
C {ipin.sym} 410 -80 2 0 {name=p10 lab=GateSel_Pin}
