v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 20 130 20 230 { lab=GATE}
N 20 230 230 230 { lab=GATE}
N 230 110 230 230 { lab=GATE}
N 230 110 240 110 { lab=GATE}
N 280 50 280 80 { lab=VPWR}
N 230 50 280 50 { lab=VPWR}
N 280 140 280 180 { lab=VOUT}
N 280 180 300 180 { lab=VOUT}
N 60 130 150 130 { lab=#net1}
N 150 40 150 130 { lab=#net1}
N 150 40 330 40 { lab=#net1}
N 330 40 330 110 { lab=#net1}
N 280 110 330 110 { lab=#net1}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/pfet_01v8.sym} 40 130 0 0 {name=M1[0:27]
L=0.39
W=0.39
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/.xschem/xschemSky130/sky130_fd_pr/pfet_01v8.sym} 260 110 0 0 {name=M2
L=0.39
W=12.48
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {ipin.sym} 230 50 0 0 {name=p1 lab=VPWR}
C {opin.sym} 300 180 0 0 {name=p2 lab=VOUT}
C {ipin.sym} 20 180 0 0 {name=p3 lab=GATE}
