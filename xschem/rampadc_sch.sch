v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 2300 -560 2300 -510 { lab=#net1}
N 2300 -450 2300 -410 { lab=#net2}
N 2300 -410 2350 -410 { lab=#net2}
N 2350 -410 2350 -400 { lab=#net2}
N 2240 -410 2300 -410 { lab=#net2}
N 2240 -410 2240 -400 { lab=#net2}
N 2240 -370 2350 -370 { lab=#net2}
N 2270 -410 2270 -370 { lab=#net2}
N 2300 -480 2330 -480 { lab=#net1}
N 2330 -530 2330 -480 { lab=#net1}
N 2300 -530 2330 -530 { lab=#net1}
N 2240 -340 2240 -300 { lab=#net3}
N 2240 -240 2240 -210 { lab=VGND}
N 2240 -270 2270 -270 { lab=VGND}
N 2270 -270 2270 -230 { lab=VGND}
N 2240 -230 2270 -230 { lab=VGND}
N 2180 -270 2200 -270 { lab=#net3}
N 2180 -320 2180 -270 { lab=#net3}
N 2180 -320 2240 -320 { lab=#net3}
N 2350 -340 2350 -310 { lab=Vramp}
N 2350 -310 2500 -310 { lab=Vramp}
N 2370 -310 2370 -290 { lab=Vramp}
N 2370 -230 2370 -210 { lab=VGND}
N 2240 -210 2440 -210 { lab=VGND}
N 2300 -650 2300 -620 { lab=VDD}
N 2300 -590 2330 -590 { lab=VDD}
N 2330 -630 2330 -590 { lab=VDD}
N 2300 -630 2330 -630 { lab=VDD}
N 2300 -660 2450 -660 { lab=VDD}
N 2300 -660 2300 -650 { lab=VDD}
N 2470 -450 2470 -420 { lab=Q}
N 2160 -420 2470 -420 { lab=Q}
N 2160 -420 2160 -370 { lab=Q}
N 2160 -370 2200 -370 { lab=Q}
N 2450 -450 2450 -370 { lab=Q_N}
N 2390 -370 2450 -370 { lab=Q_N}
N 2190 -590 2260 -590 { lab=Pbias}
N 2170 -590 2190 -590 { lab=Pbias}
N 2500 -310 2510 -310 { lab=Vramp}
N 2510 -340 2510 -310 { lab=Vramp}
N 2440 -210 2550 -210 { lab=VGND}
N 2450 -660 2520 -660 { lab=VDD}
N 2520 -660 2520 -440 { lab=VDD}
N 2060 -450 2090 -450 { lab=VGND}
N 2090 -450 2090 -400 { lab=VGND}
N 2060 -400 2090 -400 { lab=VGND}
N 2060 -420 2060 -400 { lab=VGND}
N 2060 -210 2240 -210 { lab=VGND}
N 2060 -400 2060 -210 { lab=VGND}
N 2020 -490 2020 -450 { lab=#net4}
N 2020 -490 2060 -490 { lab=#net4}
N 2060 -490 2060 -480 { lab=#net4}
N 2060 -510 2060 -490 { lab=#net4}
N 2060 -540 2080 -540 { lab=VDD}
N 2080 -590 2080 -540 { lab=VDD}
N 2060 -590 2080 -590 { lab=VDD}
N 2060 -590 2060 -570 { lab=VDD}
N 2060 -660 2060 -590 { lab=VDD}
N 2060 -660 2300 -660 { lab=VDD}
N 2000 -540 2020 -540 { lab=Pbias}
N 2000 -610 2000 -540 { lab=Pbias}
N 1970 -610 2000 -610 { lab=Pbias}
N 2000 -610 2170 -610 { lab=Pbias}
N 2170 -610 2170 -590 { lab=Pbias}
N 2060 -500 2190 -500 { lab=#net4}
N 2190 -500 2190 -480 { lab=#net4}
N 2190 -480 2260 -480 { lab=#net4}
N 3250 -730 3250 -670 { lab=#net5}
N 3190 -760 3210 -760 { lab=MeasureADC}
N 3190 -760 3190 -640 { lab=MeasureADC}
N 3190 -640 3210 -640 { lab=MeasureADC}
N 3250 -610 3250 -590 { lab=VGND}
N 3250 -810 3250 -790 { lab=VDD}
N 3610 -660 3610 -630 { lab=#net6}
N 3610 -570 3610 -540 { lab=#net7}
N 3610 -480 3610 -470 { lab=#net8}
N 3610 -470 3610 -450 { lab=#net8}
N 3790 -660 3790 -630 { lab=#net9}
N 3790 -570 3790 -540 { lab=#net7}
N 3790 -480 3790 -470 { lab=#net10}
N 3790 -470 3790 -450 { lab=#net10}
N 3610 -560 3790 -560 { lab=#net7}
N 3790 -550 3940 -550 { lab=#net7}
N 4000 -570 4000 -510 { lab=#net11}
N 3940 -600 3960 -600 { lab=#net7}
N 3940 -600 3940 -480 { lab=#net7}
N 3940 -480 3960 -480 { lab=#net7}
N 4000 -450 4000 -430 { lab=VGND}
N 4000 -650 4000 -630 { lab=VDD}
N 4180 -570 4180 -510 { lab=#net12}
N 4120 -600 4140 -600 { lab=#net11}
N 4120 -600 4120 -480 { lab=#net11}
N 4120 -480 4140 -480 { lab=#net11}
N 4180 -450 4180 -430 { lab=VGND}
N 4180 -650 4180 -630 { lab=VDD}
N 4380 -570 4380 -510 { lab=Q_N}
N 4320 -600 4340 -600 { lab=#net12}
N 4320 -600 4320 -480 { lab=#net12}
N 4320 -480 4340 -480 { lab=#net12}
N 4380 -450 4380 -430 { lab=VGND}
N 4380 -650 4380 -630 { lab=VDD}
N 4180 -310 4180 -250 { lab=Q}
N 4120 -340 4140 -340 { lab=#net11}
N 4120 -340 4120 -220 { lab=#net11}
N 4120 -220 4140 -220 { lab=#net11}
N 4180 -190 4180 -170 { lab=VGND}
N 4180 -390 4180 -370 { lab=VDD}
N 4000 -540 4120 -540 { lab=#net11}
N 4180 -540 4320 -540 { lab=#net12}
N 4070 -540 4070 -270 { lab=#net11}
N 4070 -270 4120 -270 { lab=#net11}
N 3040 -420 3040 -360 { lab=#net13}
N 2980 -450 3000 -450 { lab=VDD}
N 2980 -450 2980 -330 { lab=VDD}
N 2980 -330 3000 -330 { lab=VDD}
N 3040 -300 3040 -280 { lab=VGND}
N 3040 -500 3040 -480 { lab=VDD}
N 3250 -700 3500 -700 { lab=#net5}
N 3500 -700 3500 -690 { lab=#net5}
N 3500 -690 3570 -690 { lab=#net5}
N 3500 -690 3500 -420 { lab=#net5}
N 3500 -420 3570 -420 { lab=#net5}
N 3220 -410 3220 -350 { lab=#net14}
N 3160 -440 3180 -440 { lab=#net13}
N 3160 -440 3160 -320 { lab=#net13}
N 3160 -320 3180 -320 { lab=#net13}
N 3220 -290 3220 -270 { lab=VGND}
N 3220 -490 3220 -470 { lab=VDD}
N 3040 -390 3160 -390 { lab=#net13}
N 3220 -380 3430 -380 { lab=#net14}
N 3430 -510 3430 -380 { lab=#net14}
N 3430 -510 3570 -510 { lab=#net14}
N 3430 -380 3700 -380 { lab=#net14}
N 3700 -600 3700 -380 { lab=#net14}
N 3700 -600 3750 -600 { lab=#net14}
N 3140 -560 3140 -390 { lab=#net13}
N 3140 -560 3450 -560 { lab=#net13}
N 3450 -600 3450 -560 { lab=#net13}
N 3450 -600 3570 -600 { lab=#net13}
N 3720 -510 3750 -510 { lab=#net13}
N 3720 -510 3720 -340 { lab=#net13}
N 3340 -340 3720 -340 { lab=#net13}
N 3340 -560 3340 -340 { lab=#net13}
N 2930 -390 2980 -390 { lab=VDD}
N 3150 -700 3190 -700 { lab=MeasureADC}
N 2900 -390 2930 -390 { lab=VDD}
N 4100 -750 4100 -540 { lab=#net11}
N 3730 -750 4100 -750 { lab=#net11}
N 3730 -750 3730 -690 { lab=#net11}
N 3730 -690 3750 -690 { lab=#net11}
N 3730 -690 3730 -420 { lab=#net11}
N 3730 -420 3750 -420 { lab=#net11}
N 4180 -270 4280 -270 { lab=Q}
N 4380 -540 4440 -540 { lab=Q_N}
N 4280 -270 4300 -270 { lab=Q}
N 3040 -280 3040 -270 { lab=VGND}
N 3040 -270 3220 -270 { lab=VGND}
N 3610 -390 3610 -270 { lab=VGND}
N 3220 -270 3610 -270 { lab=VGND}
N 3790 -390 3790 -270 { lab=VGND}
N 3610 -270 3790 -270 { lab=VGND}
N 4000 -430 4000 -270 { lab=VGND}
N 3790 -270 4000 -270 { lab=VGND}
N 4010 -170 4180 -170 { lab=VGND}
N 4000 -170 4010 -170 { lab=VGND}
N 4000 -270 4000 -170 { lab=VGND}
N 4180 -430 4380 -430 { lab=VGND}
N 4380 -430 4380 -180 { lab=VGND}
N 4380 -180 4380 -170 { lab=VGND}
N 4180 -170 4380 -170 { lab=VGND}
N 3250 -830 3250 -810 { lab=VDD}
N 3250 -830 3800 -830 { lab=VDD}
N 3790 -830 3790 -720 { lab=VDD}
N 3610 -830 3610 -720 { lab=VDD}
N 4000 -830 4000 -650 { lab=VDD}
N 3800 -830 4000 -830 { lab=VDD}
N 4180 -830 4180 -650 { lab=VDD}
N 4000 -830 4180 -830 { lab=VDD}
N 4380 -830 4380 -650 { lab=VDD}
N 4180 -830 4380 -830 { lab=VDD}
N 2950 -270 3040 -270 { lab=VGND}
N 3040 -830 3250 -830 { lab=VDD}
N 3040 -830 3040 -500 { lab=VDD}
N 3220 -510 3220 -490 { lab=VDD}
N 3040 -510 3220 -510 { lab=VDD}
N 2980 -830 3040 -830 { lab=VDD}
N 2940 -830 2980 -830 { lab=VDD}
N 2930 -830 2940 -830 { lab=VDD}
N 3250 -760 3290 -760 { lab=VDD}
N 3290 -830 3290 -760 { lab=VDD}
N 3250 -640 3270 -640 { lab=VGND}
N 3270 -640 3270 -590 { lab=VGND}
N 3250 -590 3270 -590 { lab=VGND}
N 3220 -440 3250 -440 { lab=VDD}
N 3040 -330 3060 -330 { lab=VGND}
N 3060 -330 3060 -270 { lab=VGND}
N 3040 -450 3060 -450 { lab=VDD}
N 3060 -510 3060 -450 { lab=VDD}
N 3610 -420 3630 -420 { lab=VGND}
N 3630 -420 3630 -360 { lab=VGND}
N 3610 -360 3630 -360 { lab=VGND}
N 3610 -510 3630 -510 { lab=VGND}
N 3630 -510 3630 -420 { lab=VGND}
N 3610 -690 3650 -690 { lab=VDD}
N 3650 -750 3650 -690 { lab=VDD}
N 3610 -750 3650 -750 { lab=VDD}
N 3610 -600 3650 -600 { lab=VDD}
N 3650 -690 3650 -600 { lab=VDD}
N 3790 -690 3830 -690 { lab=VDD}
N 3830 -730 3830 -690 { lab=VDD}
N 3790 -730 3830 -730 { lab=VDD}
N 3790 -600 3830 -600 { lab=VDD}
N 3830 -690 3830 -600 { lab=VDD}
N 3790 -420 3820 -420 { lab=VGND}
N 3820 -420 3820 -360 { lab=VGND}
N 3790 -360 3820 -360 { lab=VGND}
N 3790 -510 3820 -510 { lab=VGND}
N 3820 -510 3820 -420 { lab=VGND}
N 4000 -600 4030 -600 { lab=VDD}
N 4030 -670 4030 -600 { lab=VDD}
N 4000 -670 4030 -670 { lab=VDD}
N 4000 -480 4020 -480 { lab=VGND}
N 4020 -480 4020 -420 { lab=VGND}
N 4000 -420 4020 -420 { lab=VGND}
N 4180 -600 4210 -600 { lab=VDD}
N 4210 -660 4210 -600 { lab=VDD}
N 4180 -660 4210 -660 { lab=VDD}
N 4180 -480 4220 -480 { lab=VGND}
N 4220 -480 4220 -430 { lab=VGND}
N 4180 -390 4270 -390 { lab=VDD}
N 4270 -830 4270 -390 { lab=VDD}
N 4180 -340 4210 -340 { lab=VDD}
N 4210 -390 4210 -340 { lab=VDD}
N 4180 -220 4210 -220 { lab=VGND}
N 4210 -220 4210 -170 { lab=VGND}
N 4380 -480 4420 -480 { lab=VGND}
N 4420 -480 4420 -400 { lab=VGND}
N 4380 -400 4420 -400 { lab=VGND}
N 4380 -600 4410 -600 { lab=VDD}
N 4410 -680 4410 -600 { lab=VDD}
N 4380 -680 4410 -680 { lab=VDD}
N 3250 -510 3250 -440 { lab=VDD}
N 3220 -510 3250 -510 { lab=VDD}
N 3270 -590 3270 -270 { lab=VGND}
N 3220 -320 3270 -320 { lab=VGND}
N 2520 -440 2810 -440 { lab=VDD}
N 2810 -440 2810 -390 { lab=VDD}
N 2810 -390 2900 -390 { lab=VDD}
N 2550 -210 2910 -210 { lab=VGND}
N 2910 -270 2910 -210 { lab=VGND}
N 2910 -270 2950 -270 { lab=VGND}
N 2520 -820 2520 -660 { lab=VDD}
N 2520 -820 2860 -820 { lab=VDD}
N 2860 -820 2930 -820 { lab=VDD}
N 2930 -830 2930 -820 { lab=VDD}
N 3140 -700 3150 -700 { lab=MeasureADC}
N 840 -780 880 -780 { lab=#net15}
N 860 -780 860 -720 { lab=#net15}
N 860 -720 920 -720 { lab=#net15}
N 1140 -780 1190 -780 { lab=#net16}
N 1100 -750 1100 -720 { lab=#net16}
N 1100 -720 1160 -720 { lab=#net16}
N 1160 -780 1160 -720 { lab=#net16}
N 940 -580 940 -540 { lab=#net17}
N 940 -540 1070 -540 { lab=#net17}
N 1070 -580 1070 -540 { lab=#net17}
N 1000 -540 1000 -500 { lab=#net17}
N 1000 -440 1000 -400 { lab=VGND}
N 920 -720 940 -720 { lab=#net15}
N 940 -720 940 -640 { lab=#net15}
N 1070 -720 1100 -720 { lab=#net16}
N 1070 -720 1070 -640 { lab=#net16}
N 800 -860 800 -810 { lab=VDD}
N 800 -860 1230 -860 { lab=VDD}
N 1230 -860 1230 -810 { lab=VDD}
N 1100 -860 1100 -810 { lab=VDD}
N 920 -860 920 -810 { lab=VDD}
N 780 -780 800 -780 { lab=VDD}
N 780 -830 780 -780 { lab=VDD}
N 780 -830 800 -830 { lab=VDD}
N 910 -780 940 -780 { lab=VDD}
N 940 -830 940 -780 { lab=VDD}
N 920 -830 940 -830 { lab=VDD}
N 1070 -780 1100 -780 { lab=VDD}
N 1070 -830 1070 -780 { lab=VDD}
N 1070 -830 1100 -830 { lab=VDD}
N 1230 -780 1250 -780 { lab=VDD}
N 1250 -830 1250 -780 { lab=VDD}
N 1230 -830 1250 -830 { lab=VDD}
N 800 -750 800 -350 { lab=#net18}
N 840 -320 1190 -320 { lab=#net18}
N 1230 -750 1230 -350 { lab=#net19}
N 800 -390 890 -390 { lab=#net18}
N 890 -390 890 -330 { lab=#net18}
N 890 -330 890 -320 { lab=#net18}
N 800 -290 800 -250 { lab=VGND}
N 800 -250 1230 -250 { lab=VGND}
N 1230 -290 1230 -250 { lab=VGND}
N 750 -320 800 -320 { lab=VGND}
N 750 -320 750 -250 { lab=VGND}
N 750 -250 800 -250 { lab=VGND}
N 1230 -320 1280 -320 { lab=VGND}
N 1280 -320 1280 -250 { lab=VGND}
N 1230 -250 1280 -250 { lab=VGND}
N 860 -610 900 -610 { lab=Vref}
N 1110 -610 1150 -610 { lab=Vramp}
N 1010 -250 1010 -210 { lab=VGND}
N 1000 -400 1000 -250 { lab=VGND}
N 1000 -470 1040 -470 { lab=VGND}
N 1040 -470 1040 -390 { lab=VGND}
N 1000 -390 1040 -390 { lab=VGND}
N 930 -470 960 -470 { lab=Nbias}
N 940 -610 1070 -610 { lab=VGND}
N 1060 -610 1060 -250 { lab=VGND}
N 920 -750 920 -720 { lab=#net15}
N 970 -890 970 -860 { lab=VDD}
N 1430 -620 1430 -570 { lab=#net20}
N 1350 -650 1390 -650 { lab=#net19}
N 1350 -650 1350 -540 { lab=#net19}
N 1350 -540 1390 -540 { lab=#net19}
N 1430 -650 1470 -650 { lab=VDD}
N 1470 -710 1470 -650 { lab=VDD}
N 1430 -710 1470 -710 { lab=VDD}
N 1430 -710 1430 -680 { lab=VDD}
N 1430 -540 1470 -540 { lab=VGND}
N 1470 -540 1470 -480 { lab=VGND}
N 1430 -480 1470 -480 { lab=VGND}
N 1430 -510 1430 -480 { lab=VGND}
N 1620 -620 1620 -570 { lab=freeze}
N 1540 -650 1580 -650 { lab=#net20}
N 1540 -650 1540 -540 { lab=#net20}
N 1540 -540 1580 -540 { lab=#net20}
N 1620 -650 1660 -650 { lab=VDD}
N 1660 -710 1660 -650 { lab=VDD}
N 1620 -710 1660 -710 { lab=VDD}
N 1620 -710 1620 -680 { lab=VDD}
N 1620 -540 1660 -540 { lab=VGND}
N 1660 -540 1660 -480 { lab=VGND}
N 1620 -510 1620 -480 { lab=VGND}
N 1430 -590 1540 -590 { lab=#net20}
N 1280 -250 1410 -250 { lab=VGND}
N 1410 -480 1410 -250 { lab=VGND}
N 1410 -480 1430 -480 { lab=VGND}
N 1470 -480 1620 -480 { lab=VGND}
N 1620 -480 1660 -480 { lab=VGND}
N 1230 -860 1430 -860 { lab=VDD}
N 1430 -860 1430 -710 { lab=VDD}
N 1470 -710 1620 -710 { lab=VDD}
N 1230 -590 1350 -590 { lab=#net19}
N 1410 -250 1990 -250 { lab=VGND}
N 1990 -250 1990 -210 { lab=VGND}
N 1990 -210 2060 -210 { lab=VGND}
N 1430 -860 2070 -860 { lab=VDD}
N 2070 -860 2070 -660 { lab=VDD}
N 2770 140 3110 140 { lab=VGND}
N 2980 -20 2980 80 { lab=Vramp}
N 2770 -20 2770 80 { lab=Vramp}
N 2770 -20 2980 -20 { lab=Vramp}
N 2730 60 2730 110 { lab=#net21}
N 2890 110 2940 110 { lab=Q_N}
N 2890 10 2890 110 { lab=Q_N}
N 2770 110 2800 110 { lab=VDD}
N 2800 -100 2800 110 { lab=VDD}
N 2640 -100 2800 -100 { lab=VDD}
N 2570 10 2890 10 { lab=Q_N}
N 2610 60 2730 60 { lab=#net21}
N 2610 -40 2610 60 { lab=#net21}
N 2610 90 2630 90 { lab=VGND}
N 2610 120 2610 140 { lab=VGND}
N 2570 10 2570 90 { lab=Q_N}
N 2570 -70 2570 10 { lab=Q_N}
N 2300 -100 2640 -100 { lab=VDD}
N 2610 -70 2640 -70 { lab=VDD}
N 2640 -100 2640 -70 { lab=VDD}
N 2980 110 3000 110 { lab=VGND}
N 2470 10 2570 10 { lab=Q_N}
N 2300 -130 2300 -100 { lab=VDD}
N 2010 140 2610 140 { lab=VGND}
N 2010 -210 2010 140 { lab=VGND}
N 1780 -850 1780 -150 { lab=VDD}
N 1780 -150 2300 -150 { lab=VDD}
N 2300 -150 2300 -130 { lab=VDD}
N 3180 -270 3180 -20 { lab=VGND}
N 2630 90 2630 140 { lab=VGND}
N 2610 140 2630 140 { lab=VGND}
N 3000 110 3000 160 { lab=VGND}
N 2630 160 3000 160 { lab=VGND}
N 2630 140 2630 160 { lab=VGND}
N 3180 -20 3180 140 { lab=VGND}
N 3110 140 3180 140 { lab=VGND}
N 2440 -310 2440 -290 { lab=Vramp}
N 2440 -230 2440 -210 { lab=VGND}
N 1780 -860 1780 -850 { lab=VDD}
C {sky130_fd_pr/nfet_01v8.sym} 2220 -270 0 0 {name=M1
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2280 -590 0 0 {name=M6
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2280 -480 0 0 {name=M2
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2220 -370 0 0 {name=M3
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2370 -370 0 1 {name=M4
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2370 -260 2 0 {name=C1 model=cap_mim_m3_1 W=1 L=1 MF=1 spiceprefix=X}
C {sky130_fd_pr/cap_mim_m3_2.sym} 2440 -260 0 0 {name=C2 model=cap_mim_m3_2 W=1 L=1 MF=1 spiceprefix=X}
C {sky130_fd_pr/pfet_01v8.sym} 2040 -540 0 0 {name=M5
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2040 -450 0 0 {name=M7
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3230 -640 0 0 {name=M8
L=0.15
W=.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3230 -760 0 0 {name=M12
L=0.15
W=.64
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3590 -690 0 0 {name=M9
L=0.15
W=.64
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3590 -600 0 0 {name=M10
L=0.15
W=.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3590 -510 0 0 {name=M11
L=0.15
W=.36
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3590 -420 0 0 {name=M13
L=0.15
W=.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3770 -690 0 0 {name=M14
L=0.15
W=.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3770 -600 0 0 {name=M15
L=0.15
W=.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3770 -510 0 0 {name=M16
L=0.15
W=.36
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3770 -420 0 0 {name=M17
L=0.15
W=.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3980 -480 0 0 {name=M18
L=0.15
W=.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3980 -600 0 0 {name=M19
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 4160 -480 0 0 {name=M20
L=0.15
W=.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 4160 -600 0 0 {name=M21
L=0.15
W=.64
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 4360 -480 0 0 {name=M22
L=0.15
W=.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 4360 -600 0 0 {name=M23
L=0.15
W=.65
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 4160 -220 0 0 {name=M24
L=0.15
W=.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 4160 -340 0 0 {name=M25
L=0.15
W=.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3020 -330 0 0 {name=M26
L=0.15
W=.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3020 -450 0 0 {name=M27
L=0.15
W=.64
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 3200 -320 0 0 {name=M28
L=0.15
W=.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 3200 -440 0 0 {name=M29
L=0.15
W=.64
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {devices/lab_pin.sym} 4300 -270 2 0 {name=l1 sig_type=std_logic lab=Q}
C {devices/lab_pin.sym} 2470 -450 1 0 {name=l2 sig_type=std_logic lab=Q}
C {devices/lab_pin.sym} 4440 -540 2 0 {name=l3 sig_type=std_logic lab=Q_N}
C {devices/lab_pin.sym} 2450 -450 1 0 {name=l4 sig_type=std_logic lab=Q_N}
C {sky130_fd_pr/nfet_01v8.sym} 920 -610 0 0 {name=M30
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 820 -780 0 1 {name=M31
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 900 -780 0 0 {name=M32
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1210 -780 0 0 {name=M33
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1120 -780 0 1 {name=M34
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1090 -610 0 1 {name=M35
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 980 -470 0 0 {name=M36
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 820 -320 0 1 {name=M37
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1210 -320 0 0 {name=M38
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 860 -610 0 0 {name=l6 sig_type=std_logic lab=Vref
}
C {devices/lab_pin.sym} 1150 -610 2 0 {name=l7 sig_type=std_logic lab=Vramp}
C {devices/lab_pin.sym} 1010 -210 2 0 {name=l8 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 930 -470 0 0 {name=l9 sig_type=std_logic lab=Nbias
}
C {devices/lab_pin.sym} 970 -890 2 0 {name=l10 sig_type=std_logic lab=VDD}
C {sky130_fd_pr/nfet_01v8.sym} 1410 -540 0 0 {name=M39
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1600 -540 0 0 {name=M40
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 1620 -590 2 0 {name=l11 sig_type=std_logic lab=freeze}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 1410 -650 0 0 {name=M41
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8_hvt.sym} 1600 -650 0 0 {name=M42
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1970 -610 0 0 {name=l12 sig_type=std_logic lab=Pbias}
C {devices/lab_pin.sym} 2510 -340 1 0 {name=l13 sig_type=std_logic lab=Vramp}
C {devices/lab_pin.sym} 3140 -700 0 0 {name=l14 sig_type=std_logic lab=MeasureADC}
C {sky130_fd_pr/pfet_01v8.sym} 2750 110 0 0 {name=M43
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2590 -70 0 0 {name=M44
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2590 90 0 0 {name=M45
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2960 110 0 0 {name=M46
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 2470 10 0 0 {name=l16 sig_type=std_logic lab=Q_N}
C {devices/lab_pin.sym} 2980 -20 2 0 {name=l18 sig_type=std_logic lab=Vramp}
