v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 220 20 220 50 { lab=#net1}
N 220 -10 230 -10 { lab=VPB}
N 230 -40 230 -10 { lab=VPB}
N 220 -40 230 -40 { lab=VPB}
N 220 80 230 80 { lab=VSUBS}
N 230 80 230 110 { lab=VSUBS}
N 220 110 230 110 { lab=VSUBS}
N 160 -10 180 -10 { lab=p1}
N 160 -10 160 80 { lab=p1}
N 160 80 180 80 { lab=p1}
N 410 20 410 50 { lab=CLK1}
N 410 -10 420 -10 { lab=VPB}
N 420 -40 420 -10 { lab=VPB}
N 410 -40 420 -40 { lab=VPB}
N 410 80 420 80 { lab=VSUBS}
N 420 80 420 110 { lab=VSUBS}
N 410 110 420 110 { lab=VSUBS}
N 350 -10 370 -10 { lab=#net1}
N 350 -10 350 80 { lab=#net1}
N 350 80 370 80 { lab=#net1}
N 590 10 590 40 { lab=CLK1B}
N 590 -20 600 -20 { lab=VPB}
N 600 -50 600 -20 { lab=VPB}
N 590 -50 600 -50 { lab=VPB}
N 590 70 600 70 { lab=VSUBS}
N 600 70 600 100 { lab=VSUBS}
N 590 100 600 100 { lab=VSUBS}
N 530 -20 550 -20 { lab=CLK1}
N 530 -20 530 70 { lab=CLK1}
N 530 70 550 70 { lab=CLK1}
N 220 260 220 290 { lab=#net2}
N 220 230 230 230 { lab=VPB}
N 230 200 230 230 { lab=VPB}
N 220 320 230 320 { lab=VSUBS}
N 230 320 230 350 { lab=VSUBS}
N 160 230 180 230 { lab=p2}
N 160 230 160 320 { lab=p2}
N 160 320 180 320 { lab=p2}
N 400 250 400 280 { lab=CLK2}
N 400 220 410 220 { lab=VPB}
N 400 310 410 310 { lab=VSUBS}
N 410 310 410 340 { lab=VSUBS}
N 340 220 360 220 { lab=#net2}
N 340 220 340 310 { lab=#net2}
N 340 310 360 310 { lab=#net2}
N 590 250 590 280 { lab=CLK2B}
N 590 220 600 220 { lab=VPB}
N 600 190 600 220 { lab=VPB}
N 590 310 600 310 { lab=VSUBS}
N 600 310 600 340 { lab=VSUBS}
N 530 220 550 220 { lab=CLK2}
N 530 220 530 310 { lab=CLK2}
N 530 310 550 310 { lab=CLK2}
N 220 -70 220 -40 { lab=VPB}
N 220 -70 600 -70 { lab=VPB}
N 600 -70 600 -50 { lab=VPB}
N 410 -70 410 -40 { lab=VPB}
N 220 170 220 200 { lab=VPB}
N 220 170 590 170 { lab=VPB}
N 590 170 590 190 { lab=VPB}
N 400 170 400 190 { lab=VPB}
N -570 270 -570 300 { lab=p2}
N -570 240 -560 240 { lab=VPB}
N -560 210 -560 240 { lab=VPB}
N -570 210 -560 210 { lab=VPB}
N -570 330 -560 330 { lab=VSUBS}
N -560 330 -560 360 { lab=VSUBS}
N -570 360 -560 360 { lab=VSUBS}
N -630 240 -610 240 { lab=#net3}
N -630 240 -630 330 { lab=#net3}
N -630 330 -610 330 { lab=#net3}
N -390 260 -390 290 { lab=#net4}
N -390 230 -380 230 { lab=VPB}
N -380 200 -380 230 { lab=VPB}
N -390 200 -380 200 { lab=VPB}
N -390 320 -380 320 { lab=VSUBS}
N -380 320 -380 350 { lab=VSUBS}
N -390 350 -380 350 { lab=VSUBS}
N -450 230 -430 230 { lab=p2}
N -450 230 -450 320 { lab=p2}
N -450 320 -430 320 { lab=p2}
N -200 260 -200 290 { lab=#net5}
N -200 230 -190 230 { lab=VPB}
N -190 200 -190 230 { lab=VPB}
N -200 200 -190 200 { lab=VPB}
N -200 320 -190 320 { lab=VSUBS}
N -190 320 -190 350 { lab=VSUBS}
N -200 350 -190 350 { lab=VSUBS}
N -260 230 -240 230 { lab=#net4}
N -260 230 -260 320 { lab=#net4}
N -260 320 -240 320 { lab=#net4}
N -570 180 -570 210 { lab=VPB}
N -570 180 -200 180 { lab=VPB}
N -200 180 -200 200 { lab=VPB}
N -390 180 -390 200 { lab=VPB}
N -50 260 -50 290 { lab=A1}
N -50 230 -40 230 { lab=VPB}
N -40 200 -40 230 { lab=VPB}
N -50 200 -40 200 { lab=VPB}
N -50 320 -40 320 { lab=VSUBS}
N -40 320 -40 350 { lab=VSUBS}
N -50 350 -40 350 { lab=VSUBS}
N -110 230 -90 230 { lab=#net5}
N -110 230 -110 320 { lab=#net5}
N -110 320 -90 320 { lab=#net5}
N -50 180 -50 200 { lab=VPB}
N -200 180 -50 180 { lab=VPB}
N -570 -10 -570 20 { lab=p1}
N -570 -40 -560 -40 { lab=VPB}
N -560 -70 -560 -40 { lab=VPB}
N -570 -70 -560 -70 { lab=VPB}
N -570 50 -560 50 { lab=VSUBS}
N -560 50 -560 80 { lab=VSUBS}
N -570 80 -560 80 { lab=VSUBS}
N -630 -40 -610 -40 { lab=#net6}
N -630 -40 -630 50 { lab=#net6}
N -630 50 -610 50 { lab=#net6}
N -390 -20 -390 10 { lab=#net7}
N -390 -50 -380 -50 { lab=VPB}
N -380 -80 -380 -50 { lab=VPB}
N -390 -80 -380 -80 { lab=VPB}
N -390 40 -380 40 { lab=VSUBS}
N -380 40 -380 70 { lab=VSUBS}
N -390 70 -380 70 { lab=VSUBS}
N -450 -50 -430 -50 { lab=p1}
N -450 -50 -450 40 { lab=p1}
N -450 40 -430 40 { lab=p1}
N -200 -20 -200 10 { lab=#net8}
N -200 -50 -190 -50 { lab=VPB}
N -190 -80 -190 -50 { lab=VPB}
N -200 -80 -190 -80 { lab=VPB}
N -200 40 -190 40 { lab=VSUBS}
N -190 40 -190 70 { lab=VSUBS}
N -200 70 -190 70 { lab=VSUBS}
N -260 -50 -240 -50 { lab=#net7}
N -260 -50 -260 40 { lab=#net7}
N -260 40 -240 40 { lab=#net7}
N -570 -100 -570 -70 { lab=VPB}
N -570 -100 -200 -100 { lab=VPB}
N -200 -100 -200 -80 { lab=VPB}
N -390 -100 -390 -80 { lab=VPB}
N -50 -20 -50 10 { lab=A2}
N -50 -50 -40 -50 { lab=VPB}
N -40 -80 -40 -50 { lab=VPB}
N -50 -80 -40 -80 { lab=VPB}
N -50 40 -40 40 { lab=VSUBS}
N -40 40 -40 70 { lab=VSUBS}
N -50 70 -40 70 { lab=VSUBS}
N -110 -50 -90 -50 { lab=#net8}
N -110 -50 -110 40 { lab=#net8}
N -110 40 -90 40 { lab=#net8}
N -50 -100 -50 -80 { lab=VPB}
N -200 -100 -50 -100 { lab=VPB}
N 220 110 220 130 { lab=VSUBS}
N 220 130 590 130 { lab=VSUBS}
N 590 100 590 130 { lab=VSUBS}
N 410 110 410 130 { lab=VSUBS}
N 220 350 220 370 { lab=VSUBS}
N 220 370 590 370 { lab=VSUBS}
N 590 340 590 370 { lab=VSUBS}
N 400 340 400 370 { lab=VSUBS}
N -570 80 -570 100 { lab=VSUBS}
N -570 100 -50 100 { lab=VSUBS}
N -50 70 -50 100 { lab=VSUBS}
N -200 70 -200 100 { lab=VSUBS}
N -390 70 -390 100 { lab=VSUBS}
N -570 360 -570 380 { lab=VSUBS}
N -570 380 -50 380 { lab=VSUBS}
N -50 350 -50 380 { lab=VSUBS}
N -200 350 -200 380 { lab=VSUBS}
N -390 350 -390 380 { lab=VSUBS}
N 220 40 350 40 { lab=#net1}
N 410 40 530 40 { lab=CLK1}
N 220 270 340 270 { lab=#net2}
N 400 270 530 270 { lab=CLK2}
N -200 0 -110 0 { lab=#net8}
N -200 280 -110 280 { lab=#net5}
N -390 280 -260 280 { lab=#net4}
N -390 -10 -260 -10 { lab=#net7}
N -570 -0 -450 -0 { lab=p1}
N -570 290 -450 290 { lab=p2}
N -1040 -120 -1040 -90 { lab=#net6}
N -1040 -90 -890 -90 { lab=#net6}
N -890 -120 -890 -90 { lab=#net6}
N -960 -90 -960 -60 { lab=#net6}
N -960 0 -960 30 { lab=#net9}
N -960 -30 -940 -30 { lab=VSUBS}
N -940 -30 -940 60 { lab=VSUBS}
N -960 60 -940 60 { lab=VSUBS}
N -940 60 -940 90 { lab=VSUBS}
N -960 90 -940 90 { lab=VSUBS}
N -1040 -200 -1040 -180 { lab=VPB}
N -1040 -200 -890 -200 { lab=VPB}
N -890 -200 -890 -180 { lab=VPB}
N -1040 -150 -1010 -150 { lab=VPB}
N -1010 -200 -1010 -150 { lab=VPB}
N -890 -150 -870 -150 { lab=VPB}
N -870 -200 -870 -150 { lab=VPB}
N -890 -200 -870 -200 { lab=VPB}
N -950 -150 -930 -150 { lab=#net10}
N -950 -150 -950 -110 { lab=#net10}
N -1120 -150 -1080 -150 { lab=A1}
N -1070 260 -1070 290 { lab=#net3}
N -1070 290 -920 290 { lab=#net3}
N -920 260 -920 290 { lab=#net3}
N -990 290 -990 320 { lab=#net3}
N -990 380 -990 410 { lab=#net11}
N -990 350 -970 350 { lab=VSUBS}
N -970 350 -970 440 { lab=VSUBS}
N -990 440 -970 440 { lab=VSUBS}
N -970 440 -970 470 { lab=VSUBS}
N -990 470 -970 470 { lab=VSUBS}
N -1070 180 -1070 200 { lab=VPB}
N -1070 180 -920 180 { lab=VPB}
N -920 180 -920 200 { lab=VPB}
N -1070 230 -1040 230 { lab=VPB}
N -1040 180 -1040 230 { lab=VPB}
N -920 230 -900 230 { lab=VPB}
N -900 180 -900 230 { lab=VPB}
N -920 180 -900 180 { lab=VPB}
N -980 230 -960 230 { lab=B2}
N -980 230 -980 270 { lab=B2}
N -1050 350 -1030 350 { lab=A2}
N -1150 230 -1110 230 { lab=A2}
N -890 -90 -810 -90 { lab=#net6}
N -810 -90 -810 -0 { lab=#net6}
N -810 -0 -630 0 { lab=#net6}
N -920 290 -630 290 { lab=#net3}
N -1460 320 -1460 360 { lab=B2}
N -1530 290 -1500 290 { lab=#net10}
N -1530 290 -1530 390 { lab=#net10}
N -1530 390 -1500 390 { lab=#net10}
N -1460 290 -1440 290 { lab=VPB}
N -1440 240 -1440 290 { lab=VPB}
N -1460 240 -1440 240 { lab=VPB}
N -1460 240 -1460 260 { lab=VPB}
N -1460 390 -1440 390 { lab=VSUBS}
N -1440 390 -1440 430 { lab=VSUBS}
N -1460 430 -1440 430 { lab=VSUBS}
N -1460 420 -1460 430 { lab=VSUBS}
N -50 0 10 0 { lab=A2}
N 10 0 10 140 { lab=A2}
N -1150 140 -1150 230 { lab=A2}
N -1150 140 10 140 { lab=A2}
N -50 280 60 280 { lab=A1}
N 60 -270 60 280 { lab=A1}
N -1120 -270 60 -270 { lab=A1}
N -1120 -270 -1120 -150 { lab=A1}
N 230 200 410 200 { lab=VPB}
N 410 200 600 200 { lab=VPB}
N 500 170 500 200 { lab=VPB}
N 600 340 600 360 { lab=VSUBS}
N 490 360 600 360 { lab=VSUBS}
N 490 340 490 360 { lab=VSUBS}
N 410 340 490 340 { lab=VSUBS}
N 230 360 490 360 { lab=VSUBS}
N 230 350 230 360 { lab=VSUBS}
N 320 360 320 370 { lab=VSUBS}
N 410 200 410 220 { lab=VPB}
N -500 290 -500 420 { lab=p2}
N -500 420 120 420 { lab=p2}
N 120 310 120 420 { lab=p2}
N 120 310 160 310 { lab=p2}
N -870 -200 -570 -200 { lab=VPB}
N -570 -200 -570 -100 { lab=VPB}
N -50 -100 220 -100 { lab=VPB}
N 220 -100 220 -70 { lab=VPB}
N -50 180 220 180 { lab=VPB}
N -900 180 -570 180 { lab=VPB}
N -1440 180 -1440 240 { lab=VPB}
N -1440 180 -1060 180 { lab=VPB}
N 120 -100 120 180 { lab=VPB}
N 470 40 470 60 { lab=CLK1}
N 460 270 460 290 { lab=CLK2}
N 590 30 620 30 { lab=CLK1B}
N 590 270 610 270 { lab=CLK2B}
N 470 60 470 80 { lab=CLK1}
N 470 80 490 80 { lab=CLK1}
N 460 290 460 310 { lab=CLK2}
N 460 310 480 310 { lab=CLK2}
N -1150 230 -1150 300 { lab=A2}
N -1150 300 -1150 330 { lab=A2}
N -1150 330 -1150 350 { lab=A2}
N -1150 350 -1050 350 { lab=A2}
N -1050 440 -1030 440 { lab=B2}
N -1120 440 -1050 440 { lab=B2}
N -1010 270 -980 270 { lab=B2}
N -1460 340 -1280 340 { lab=B2}
N -1280 340 -1280 440 { lab=B2}
N -1280 440 -1120 440 { lab=B2}
N -1530 60 -1000 60 { lab=#net10}
N -1530 60 -1530 290 { lab=#net10}
N -1120 -150 -1120 -80 { lab=A1}
N -1120 -80 -1120 -30 { lab=A1}
N -1120 -30 -1000 -30 { lab=A1}
N -980 -110 -950 -110 { lab=#net10}
N -1580 -210 -1580 -180 { lab=#net12}
N -1580 -180 -1430 -180 { lab=#net12}
N -1430 -210 -1430 -180 { lab=#net12}
N -1500 -180 -1500 -150 { lab=#net12}
N -1500 -90 -1500 -60 { lab=#net13}
N -1500 -120 -1480 -120 { lab=VSUBS}
N -1480 -120 -1480 -30 { lab=VSUBS}
N -1500 -30 -1480 -30 { lab=VSUBS}
N -1480 -30 -1480 0 { lab=VSUBS}
N -1500 0 -1480 0 { lab=VSUBS}
N -1580 -290 -1580 -270 { lab=A1}
N -1580 -290 -1430 -290 { lab=A1}
N -1430 -290 -1430 -270 { lab=A1}
N -1580 -240 -1550 -240 { lab=A1}
N -1550 -290 -1550 -240 { lab=A1}
N -1430 -240 -1410 -240 { lab=A1}
N -1410 -290 -1410 -240 { lab=A1}
N -1430 -290 -1410 -290 { lab=A1}
N -1490 -240 -1470 -240 { lab=ENABLE}
N -1490 -240 -1490 -200 { lab=ENABLE}
N -1430 -180 -1350 -180 { lab=#net12}
N -1520 -200 -1490 -200 { lab=ENABLE}
N -1280 -190 -1280 -150 { lab=#net10}
N -1350 -220 -1320 -220 { lab=#net12}
N -1350 -220 -1350 -120 { lab=#net12}
N -1350 -120 -1320 -120 { lab=#net12}
N -1280 -220 -1260 -220 { lab=A1}
N -1260 -270 -1260 -220 { lab=A1}
N -1280 -270 -1260 -270 { lab=A1}
N -1280 -270 -1280 -250 { lab=A1}
N -1280 -120 -1260 -120 { lab=VSUBS}
N -1260 -120 -1260 -80 { lab=VSUBS}
N -1280 -80 -1260 -80 { lab=VSUBS}
N -1280 -90 -1280 -80 { lab=VSUBS}
N -1280 -160 -1170 -160 { lab=#net10}
N -1170 -160 -1170 -100 { lab=#net10}
N -1170 -100 -980 -100 { lab=#net10}
N -980 -110 -980 -100 { lab=#net10}
N -1170 -100 -1170 -0 { lab=#net10}
N -1330 -0 -1170 -0 { lab=#net10}
N -1330 0 -1330 10 { lab=#net10}
N -1330 10 -1330 60 { lab=#net10}
N -1560 -200 -1520 -200 { lab=ENABLE}
N -1560 -200 -1560 -120 { lab=ENABLE}
N -1560 -120 -1540 -120 { lab=ENABLE}
N -1620 -240 -1620 -30 { lab=CLK}
N -1620 -30 -1540 -30 { lab=CLK}
N -1410 -290 -1280 -290 { lab=A1}
N -1280 -290 -1280 -270 { lab=A1}
N -1040 -300 -1040 -200 {}
N -1280 -300 -1040 -300 {}
N -1280 -300 -1280 -290 {}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 200 80 0 0 {name=M1
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 390 80 0 0 {name=M3
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 570 70 0 0 {name=M5
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 200 320 0 0 {name=M7
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 380 310 0 0 {name=M9
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 570 310 0 0 {name=M11
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -590 330 0 0 {name=M13
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -410 320 0 0 {name=M15
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -220 320 0 0 {name=M17
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -70 320 0 0 {name=M19
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -590 50 0 0 {name=M21
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -410 40 0 0 {name=M23
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -220 40 0 0 {name=M25
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -70 40 0 0 {name=M27
L=0.15
W=0.42
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -570 380 0 0 {name=l5 sig_type=std_logic lab=VSUBS}
C {lab_pin.sym} 220 130 0 0 {name=l7 sig_type=std_logic lab=VSUBS}
C {lab_pin.sym} 220 370 0 0 {name=l8 sig_type=std_logic lab=VSUBS}
C {lab_pin.sym} -570 10 2 0 {name=l9 sig_type=std_logic lab=p1}
C {lab_pin.sym} 160 40 0 0 {name=l10 sig_type=std_logic lab=p1}
C {lab_pin.sym} -570 280 2 0 {name=l11 sig_type=std_logic lab=p2}
C {lab_pin.sym} 160 280 0 0 {name=l12 sig_type=std_logic lab=p2}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -980 -30 0 0 {name=M29
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -980 60 0 0 {name=M30
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -940 20 2 0 {name=l14 sig_type=std_logic lab=VSUBS}
C {lab_pin.sym} -1120 -80 0 0 {name=l15 sig_type=std_logic lab=A1}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -1010 350 0 0 {name=M33
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -1010 440 0 0 {name=M34
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -970 400 2 0 {name=l18 sig_type=std_logic lab=VSUBS}
C {lab_pin.sym} -1150 300 0 0 {name=l19 sig_type=std_logic lab=A2}
C {lab_pin.sym} -1120 440 0 0 {name=l20 sig_type=std_logic lab=B2}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -1480 390 0 0 {name=M37
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -1440 420 2 0 {name=l21 sig_type=std_logic lab=VSUBS}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 200 -10 0 0 {name=M2
L=0.15
W=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 390 -10 0 0 {name=M4
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 570 -20 0 0 {name=M6
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 200 230 0 0 {name=M8
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 380 220 0 0 {name=M10
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 570 220 0 0 {name=M12
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -590 240 0 0 {name=M14
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -410 230 0 0 {name=M16
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -220 230 0 0 {name=M18
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -70 230 0 0 {name=M20
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -590 -40 0 0 {name=M22
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -410 -50 0 0 {name=M24
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -220 -50 0 0 {name=M26
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -70 -50 0 0 {name=M28
L=0.15
W=1
nf=1
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -1060 -150 0 0 {name=M31
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -910 -150 0 0 {name=M32
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -1090 230 0 0 {name=M35
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -940 230 0 0 {name=M36
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -1480 290 0 0 {name=M38
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {ipin.sym} -1620 -80 0 0 {name=p1 lab=CLK}
C {ipin.sym} -570 -200 2 0 {name=p2 lab=VPB}
C {ipin.sym} -570 100 0 0 {name=p3 lab=VSUBS}
C {opin.sym} 620 30 0 0 {name=p4 lab=CLK1B}
C {opin.sym} 490 80 0 0 {name=p5 lab=CLK1}
C {opin.sym} 610 270 0 0 {name=p6 lab=CLK2B}
C {opin.sym} 480 310 0 0 {name=p7 lab=CLK2}
C {lab_pin.sym} -1010 270 0 0 {name=l1 sig_type=std_logic lab=B2}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -1520 -120 0 0 {name=M39
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -1520 -30 0 0 {name=M40
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -1480 -70 2 0 {name=l3 sig_type=std_logic lab=VSUBS}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -1600 -240 0 0 {name=M41
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -1450 -240 0 0 {name=M42
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} -1300 -120 0 0 {name=M43
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {lab_pin.sym} -1260 -90 2 0 {name=l4 sig_type=std_logic lab=VSUBS}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} -1300 -220 0 0 {name=M44
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {ipin.sym} -1560 -150 0 0 {name=p8 lab=ENABLE}
