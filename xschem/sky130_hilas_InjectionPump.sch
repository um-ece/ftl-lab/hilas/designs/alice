v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 290 -390 320 -390 { lab=VPWR}
N 380 -390 400 -390 { lab=#net1}
N 440 -390 470 -390 { lab=#net1}
N 400 -390 440 -390 { lab=#net1}
N 430 -360 590 -360 { lab=#net1}
N 430 -300 590 -300 { lab=#net2}
N 470 -390 490 -390 { lab=#net1}
N 490 -390 490 -360 { lab=#net1}
N 600 -390 630 -390 { lab=#net1}
N 690 -390 710 -390 { lab=#net3}
N 750 -390 780 -390 { lab=#net3}
N 710 -390 750 -390 { lab=#net3}
N 740 -360 900 -360 { lab=#net3}
N 740 -300 900 -300 { lab=#net4}
N 780 -390 800 -390 { lab=#net3}
N 800 -390 800 -360 { lab=#net3}
N 800 -390 920 -390 { lab=#net3}
N 490 -390 600 -390 { lab=#net1}
N 920 -390 950 -390 { lab=#net3}
N 1010 -390 1030 -390 { lab=#net5}
N 1070 -390 1100 -390 { lab=#net5}
N 1030 -390 1070 -390 { lab=#net5}
N 1060 -360 1220 -360 { lab=#net5}
N 1060 -300 1220 -300 { lab=#net6}
N 1100 -390 1120 -390 { lab=#net5}
N 1120 -390 1120 -360 { lab=#net5}
N 1120 -390 1240 -390 { lab=#net5}
N 1240 -390 1270 -390 { lab=#net5}
N 1330 -390 1350 -390 { lab=#net7}
N 1390 -390 1420 -390 { lab=#net7}
N 1350 -390 1390 -390 { lab=#net7}
N 1380 -360 1540 -360 { lab=#net7}
N 1380 -300 1540 -300 { lab=#net8}
N 1420 -390 1440 -390 { lab=#net7}
N 1440 -390 1440 -360 { lab=#net7}
N 1440 -390 1560 -390 { lab=#net7}
N 1560 -390 1590 -390 { lab=#net7}
N 1650 -390 1670 -390 { lab=VINJ_out}
N 1710 -390 1740 -390 { lab=VINJ_out}
N 1670 -390 1710 -390 { lab=VINJ_out}
N 1700 -360 1860 -360 { lab=VINJ_out}
N 1700 -300 1860 -300 { lab=VGND}
N 1740 -390 1760 -390 { lab=VINJ_out}
N 1760 -390 1760 -360 { lab=VINJ_out}
N 1760 -390 1880 -390 { lab=VINJ_out}
N 1830 -490 1890 -490 { lab=VINJ_out}
N 1890 -490 1890 -390 { lab=VINJ_out}
N 1880 -390 1890 -390 { lab=VINJ_out}
N 1830 -450 1960 -450 { lab=VGND}
N 1960 -450 1960 -260 { lab=VGND}
N 1790 -260 1960 -260 { lab=VGND}
N 1790 -300 1790 -260 { lab=VGND}
N 1890 -390 2030 -390 { lab=VINJ_out}
N 250 -490 1530 -490 { lab=VPWR}
N 250 -490 250 -390 { lab=VPWR}
N 250 -390 290 -390 { lab=VPWR}
N 1890 -490 2050 -490 { lab=VINJ_out}
N 2210 -480 2210 -430 { lab=#net9}
N 2010 -430 2210 -430 { lab=#net9}
N 2010 -470 2010 -430 { lab=#net9}
N 1840 -470 2010 -470 { lab=#net9}
N 1830 -470 1840 -470 { lab=#net9}
N 1930 -480 2050 -480 { lab=VPWR}
N 1930 -510 1930 -480 { lab=VPWR}
N 1510 -510 1930 -510 { lab=VPWR}
N 1510 -510 1510 -490 { lab=VPWR}
N 1960 -450 2030 -450 { lab=VGND}
N 2030 -470 2030 -450 { lab=VGND}
N 2030 -470 2050 -470 { lab=VGND}
N 790 -130 850 -130 { lab=#net2}
N 850 -190 850 -130 { lab=#net2}
N 500 -190 850 -190 { lab=#net2}
N 500 -300 500 -190 { lab=#net2}
N 790 -150 820 -150 { lab=#net4}
N 820 -300 820 -150 { lab=#net4}
N 790 -90 1120 -90 { lab=#net6}
N 1120 -300 1120 -90 { lab=#net6}
N 790 -110 1460 -110 { lab=#net8}
N 1460 -300 1460 -110 { lab=#net8}
N 250 -390 250 -160 { lab=VPWR}
N 250 -160 250 -150 { lab=VPWR}
N 250 -150 490 -150 { lab=VPWR}
N 1990 -500 2050 -500 { lab=#net10}
N 390 -130 490 -130 { lab=InjEnable}
N 470 -90 490 -90 { lab=VGND}
N 400 -110 490 -110 { lab=InjCLK}
N 1860 -160 1860 -130 { lab=#net10}
N 1860 -190 1890 -190 { lab=VPWR}
N 1890 -230 1890 -190 { lab=VPWR}
N 1860 -230 1890 -230 { lab=VPWR}
N 1860 -230 1860 -220 { lab=VPWR}
N 1860 -100 1880 -100 { lab=VGND}
N 1880 -100 1880 -60 { lab=VGND}
N 1860 -60 1880 -60 { lab=VGND}
N 1860 -70 1860 -60 { lab=VGND}
N 1800 -190 1820 -190 { lab=InjEnable}
N 1800 -190 1800 -100 { lab=InjEnable}
N 1800 -100 1820 -100 { lab=InjEnable}
N 1860 -140 1990 -140 { lab=#net10}
N 1990 -500 1990 -140 { lab=#net10}
N 1950 -260 1950 -60 { lab=VGND}
N 1880 -60 1950 -60 { lab=VGND}
C {/home/hilas/Documents/work/alice/xschem/sky130_hilas_StepUpDigital.sym} 260 830 0 0 {name=X1}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 350 -390 1 0 {name=D1
model=diode_pd2nw_11v0
area=6.831}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 430 -330 0 0 {name=C1 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 590 -330 2 0 {name=C2 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 660 -390 1 0 {name=D2
model=diode_pd2nw_11v0
area=6.831}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 740 -330 0 0 {name=C3 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 900 -330 2 0 {name=C4 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 980 -390 1 0 {name=D3
model=diode_pd2nw_11v0
area=6.831}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 1060 -330 0 0 {name=C5 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 1220 -330 2 0 {name=C6 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 1300 -390 1 0 {name=D4
model=diode_pd2nw_11v0
area=6.831}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 1380 -330 0 0 {name=C7 model=cap_mim_m3_1 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 1540 -330 2 0 {name=C8 model=cap_mim_m3_2 W=50 L=5 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 1620 -390 1 0 {name=D5
model=diode_pd2nw_11v0
area=6.831}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 1700 -330 0 0 {name=C9 model=cap_mim_m3_1 W=96 L=26 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 1860 -330 2 0 {name=C10 model=cap_mim_m3_2 W=96 L=26 MF=1 spiceprefix=X}
C {/home/hilas/Documents/work/alice/xschem/sky130_hilas_InjectionTgate.sym} 1680 -470 0 1 {name=x1}
C {/home/hilas/Documents/work/alice/xschem/devices/opin.sym} 2030 -390 0 0 {name=p1 lab=VINJ_out}
C {/home/hilas/Documents/work/alice/xschem/devices/ipin.sym} 1790 -260 0 0 {name=p2 lab=VGND}
C {/home/hilas/Documents/work/alice/xschem/sky130_hilas_clk_gen.sym} 640 -120 0 0 {name=x2}
C {/home/hilas/Documents/work/alice/xschem/devices/ipin.sym} 390 -130 0 0 {name=p3 lab=InjEnable}
C {/home/hilas/Documents/work/alice/xschem/devices/ipin.sym} 470 -90 0 0 {name=p5 lab=VGND}
C {/home/hilas/Documents/work/alice/xschem/devices/ipin.sym} 400 -110 0 0 {name=p6 lab=InjCLK}
C {/home/hilas/Documents/work/alice/xschem/sky130_fd_pr/nfet_01v8.sym} 1840 -100 0 0 {name=M1
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/Documents/work/alice/xschem/sky130_fd_pr/pfet_01v8_hvt.sym} 1840 -190 0 0 {name=M2
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_hvt
spiceprefix=X
}
C {/home/hilas/Documents/work/alice/xschem/devices/ipin.sym} 250 -190 0 0 {name=p4 lab=VPWR}
C {/home/hilas/Documents/work/alice/xschem/devices/ipin.sym} 1860 -230 0 0 {name=p7 lab=VPWR}
C {/home/hilas/Documents/work/alice/xschem/devices/ipin.sym} 1800 -150 0 0 {name=p8 lab=InjEnable}
