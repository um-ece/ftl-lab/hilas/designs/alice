v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
T {V+
} 2050 -1320 0 0 0.4 0.4 {}
T {V+
} 2440 -1130 0 0 0.4 0.4 {}
T {V-} 2610 -1300 0 0 0.4 0.4 {}
T {V-} 2040 -1540 0 0 0.4 0.4 {}
N 2120 -1160 2160 -1160 { lab=GATE2}
N 2000 -1160 2040 -1160 { lab=VTUN}
N 2000 -1170 2000 -1160 { lab=VTUN}
N 1970 -1220 2190 -1220 { lab=VIN12}
N 1930 -1270 1930 -1250 { lab=#net1}
N 2230 -1190 2230 -1110 { lab=#net2}
N 1930 -1190 1930 -1130 { lab=DRAIN2}
N 1930 -1130 2310 -1130 { lab=DRAIN2}
N 1860 -1140 1860 -1090 { lab=VINJ}
N 2120 -1420 2160 -1420 { lab=GATE2}
N 2000 -1420 2040 -1420 { lab=VTUN}
N 2000 -1430 2000 -1420 { lab=VTUN}
N 1970 -1480 2190 -1480 { lab=VIN11}
N 1930 -1530 1930 -1510 { lab=#net3}
N 2120 -1420 2120 -1160 { lab=GATE2}
N 2040 -1430 2040 -1420 { lab=VTUN}
N 2040 -1420 2040 -1160 { lab=VTUN}
N 1890 -1610 1890 -1090 { lab=COLSEL2}
N 1930 -1450 1930 -1390 { lab=DRAIN1}
N 1930 -1390 2310 -1390 { lab=DRAIN1}
N 1860 -1400 1860 -1330 { lab=VINJ}
N 1610 -1160 1650 -1160 { lab=GATE1}
N 1490 -1160 1530 -1160 { lab=VTUN}
N 1490 -1170 1490 -1160 { lab=VTUN}
N 1460 -1220 1680 -1220 { lab=#net4}
N 1420 -1270 1420 -1250 { lab=#net5}
N 1760 -1250 1760 -1090 { lab=VPWR}
N 1720 -1190 1720 -1080 { lab=#net6}
N 1610 -1160 1610 -1090 { lab=GATE1}
N 1530 -1160 1530 -1070 { lab=VTUN}
N 1420 -1190 1420 -1130 { lab=DRAIN2}
N 1420 -1130 1930 -1130 { lab=DRAIN2}
N 1350 -1140 1350 -1090 { lab=VINJ}
N 1610 -1420 1650 -1420 { lab=GATE1}
N 1490 -1420 1530 -1420 { lab=VTUN}
N 1490 -1430 1490 -1420 { lab=VTUN}
N 1460 -1480 1680 -1480 { lab=#net7}
N 1420 -1530 1420 -1510 { lab=#net8}
N 1760 -1510 1760 -1250 { lab=VPWR}
N 1720 -1450 1720 -1370 { lab=#net9}
N 1610 -1420 1610 -1160 { lab=GATE1}
N 1530 -1420 1530 -1160 { lab=VTUN}
N 1420 -1450 1420 -1390 { lab=DRAIN1}
N 1420 -1390 1930 -1390 { lab=DRAIN1}
N 1350 -1400 1350 -1330 { lab=VINJ}
N 1350 -1590 1420 -1590 { lab=VINJ}
N 1280 -1390 1420 -1390 { lab=DRAIN1}
N 1350 -1620 1350 -1590 { lab=VINJ}
N 1340 -1640 1350 -1620 { lab=VINJ}
N 1420 -1570 1420 -1560 { lab=VINJ}
N 1420 -1570 1430 -1570 { lab=VINJ}
N 1430 -1590 1430 -1570 { lab=VINJ}
N 1420 -1590 1430 -1590 { lab=VINJ}
N 1350 -1480 1420 -1480 { lab=VINJ}
N 1350 -1220 1420 -1220 { lab=VINJ}
N 1420 -1300 1430 -1300 { lab=VINJ}
N 1430 -1330 1430 -1300 { lab=VINJ}
N 1860 -1480 1930 -1480 { lab=VINJ}
N 1930 -1560 1940 -1560 { lab=VINJ}
N 1940 -1590 1940 -1560 { lab=VINJ}
N 1860 -1220 1940 -1220 { lab=VINJ}
N 1930 -1300 1940 -1300 { lab=VINJ}
N 1940 -1330 1940 -1300 { lab=VINJ}
N 1860 -1140 2250 -1140 { lab=VINJ}
N 2250 -1220 2250 -1140 { lab=VINJ}
N 2230 -1220 2250 -1220 { lab=VINJ}
N 2230 -1480 2250 -1480 { lab=VINJ}
N 2250 -1480 2250 -1400 { lab=VINJ}
N 1860 -1400 2250 -1400 { lab=VINJ}
N 1720 -1220 1740 -1220 { lab=VINJ}
N 1740 -1220 1740 -1140 { lab=VINJ}
N 1350 -1140 1740 -1140 { lab=VINJ}
N 1720 -1480 1740 -1480 { lab=VINJ}
N 1740 -1480 1740 -1400 { lab=VINJ}
N 1350 -1400 1740 -1400 { lab=VINJ}
N 1380 -1630 1380 -1090 { lab=COLSEL1}
N 1380 -1630 1410 -1630 { lab=COLSEL1}
N 1760 -1620 1780 -1620 { lab=VPWR}
N 1530 -1630 1530 -1420 { lab=VTUN}
N 1530 -1630 1540 -1630 { lab=VTUN}
N 1860 -1670 1860 -1590 { lab=VINJ}
N 1330 -1670 1860 -1670 { lab=VINJ}
N 1330 -1640 1330 -1610 { lab=VINJ}
N 1330 -1640 1340 -1640 { lab=VINJ}
N 2670 -1570 2690 -1570 { lab=#net10}
N 2730 -1570 2750 -1570 { lab=VPWR}
N 2750 -1600 2750 -1570 { lab=VPWR}
N 2600 -1600 2750 -1600 { lab=VPWR}
N 2600 -1600 2600 -1570 { lab=VPWR}
N 2600 -1570 2620 -1570 { lab=VPWR}
N 2730 -1530 2730 -1510 { lab=OUTPUT1}
N 2730 -1530 2770 -1530 { lab=OUTPUT1}
N 2620 -1520 2620 -1460 { lab=#net10}
N 2570 -1480 2690 -1480 { lab=#net11}
N 2570 -1510 2570 -1480 { lab=#net11}
N 2500 -1430 2580 -1430 { lab=#net2}
N 2500 -1460 2500 -1430 { lab=#net2}
N 2450 -1430 2460 -1430 { lab=VGND}
N 2450 -1430 2450 -1400 { lab=VGND}
N 2450 -1400 2520 -1400 { lab=VGND}
N 2520 -1400 2630 -1400 { lab=VGND}
N 2630 -1430 2630 -1400 { lab=VGND}
N 2620 -1430 2630 -1430 { lab=VGND}
N 2730 -1480 2740 -1480 { lab=VGND}
N 2740 -1480 2740 -1450 { lab=VGND}
N 2630 -1450 2740 -1450 { lab=VGND}
N 2630 -1450 2630 -1430 { lab=VGND}
N 2520 -1480 2530 -1480 { lab=VGND}
N 2520 -1450 2520 -1400 { lab=VGND}
N 2520 -1450 2530 -1450 { lab=VGND}
N 2780 -1290 2800 -1290 { lab=#net12}
N 2840 -1290 2860 -1290 { lab=VPWR}
N 2860 -1320 2860 -1290 { lab=VPWR}
N 2710 -1320 2860 -1320 { lab=VPWR}
N 2710 -1320 2710 -1290 { lab=VPWR}
N 2710 -1290 2730 -1290 { lab=VPWR}
N 2840 -1250 2840 -1230 { lab=OUTPUT2}
N 2840 -1250 2880 -1250 { lab=OUTPUT2}
N 2730 -1230 2730 -1180 { lab=#net12}
N 2680 -1200 2800 -1200 { lab=#net13}
N 2680 -1230 2680 -1200 { lab=#net13}
N 2610 -1150 2690 -1150 { lab=#net14}
N 2610 -1180 2610 -1150 { lab=#net14}
N 2560 -1150 2570 -1150 { lab=VGND}
N 2560 -1150 2560 -1120 { lab=VGND}
N 2560 -1120 2630 -1120 { lab=VGND}
N 2700 -1120 2740 -1120 { lab=VGND}
N 2740 -1150 2740 -1120 { lab=VGND}
N 2730 -1150 2740 -1150 { lab=VGND}
N 2840 -1200 2850 -1200 { lab=VGND}
N 2850 -1200 2850 -1170 { lab=VGND}
N 2740 -1170 2850 -1170 { lab=VGND}
N 2740 -1170 2740 -1150 { lab=VGND}
N 2630 -1200 2640 -1200 { lab=VGND}
N 2630 -1170 2630 -1120 { lab=VGND}
N 2630 -1170 2640 -1170 { lab=VGND}
N 1530 -1070 2040 -1070 { lab=VTUN}
N 1720 -1370 1720 -1360 { lab=#net9}
N 1720 -1080 2360 -1080 { lab=#net6}
N 2230 -1370 2230 -1250 { lab=#net9}
N 1720 -1370 2230 -1370 { lab=#net9}
N 2230 -1110 2330 -1110 { lab=#net2}
N 1760 -1650 1760 -1620 { lab=VPWR}
N 1760 -1650 2600 -1650 { lab=VPWR}
N 2600 -1650 2600 -1600 { lab=VPWR}
N 2330 -1460 2500 -1460 { lab=#net2}
N 2540 -1230 2680 -1230 { lab=#net13}
N 2460 -1180 2610 -1180 { lab=#net14}
N 2580 -1260 2590 -1260 { lab=VIN22}
N 2400 -1210 2420 -1210 { lab=VIN21}
N 2400 -1210 2400 -1150 { lab=VIN21}
N 2400 -1150 2410 -1150 { lab=VIN21}
N 2360 -1290 2360 -1080 { lab=#net6}
N 2460 -1290 2540 -1290 { lab=#net6}
N 2460 -1290 2460 -1240 { lab=#net6}
N 2250 -1400 2370 -1400 { lab=VINJ}
N 2370 -1400 2370 -1310 { lab=VINJ}
N 2480 -1310 2520 -1310 { lab=VINJ}
N 2520 -1310 2520 -1260 { lab=VINJ}
N 2520 -1260 2540 -1260 { lab=VINJ}
N 2460 -1210 2480 -1210 { lab=VINJ}
N 2480 -1310 2480 -1210 { lab=VINJ}
N 2860 -1320 2920 -1320 { lab=VPWR}
N 2920 -1600 2920 -1320 { lab=VPWR}
N 2750 -1600 2920 -1600 { lab=VPWR}
N 2620 -1520 2670 -1520 { lab=#net10}
N 2670 -1570 2670 -1520 { lab=#net10}
N 2730 -1230 2780 -1230 { lab=#net12}
N 2780 -1290 2780 -1230 { lab=#net12}
N 2020 -1560 2020 -1540 { lab=VIN11}
N 1980 -1330 1980 -1280 { lab=VIN12}
N 2700 -1120 2700 -1080 { lab=VGND}
N 2120 -1610 2120 -1420 { lab=GATE2}
N 1280 -1130 1420 -1130 { lab=DRAIN2}
N 1760 -1620 1760 -1510 { lab=VPWR}
N 1350 -1640 1350 -1620 { lab=VINJ}
N 1420 -1590 1420 -1570 { lab=VINJ}
N 1350 -1590 1350 -1480 { lab=VINJ}
N 1350 -1330 1350 -1220 { lab=VINJ}
N 1860 -1590 1860 -1480 { lab=VINJ}
N 1860 -1330 1860 -1220 { lab=VINJ}
N 1860 -1220 1860 -1140 { lab=VINJ}
N 1860 -1480 1860 -1400 { lab=VINJ}
N 1350 -1220 1350 -1140 { lab=VINJ}
N 1350 -1480 1350 -1400 { lab=VINJ}
N 1330 -1670 1330 -1640 { lab=VINJ}
N 2730 -1540 2730 -1530 { lab=OUTPUT1}
N 2520 -1480 2520 -1450 { lab=VGND}
N 2840 -1260 2840 -1250 { lab=OUTPUT2}
N 2630 -1200 2630 -1170 { lab=VGND}
N 2330 -1460 2330 -1110 { lab=#net2}
N 2360 -1290 2460 -1290 { lab=#net6}
N 2370 -1310 2480 -1310 { lab=VINJ}
N 2620 -1540 2620 -1520 { lab=#net10}
N 2660 -1570 2670 -1570 { lab=#net10}
N 2730 -1260 2730 -1230 { lab=#net12}
N 2770 -1290 2780 -1290 { lab=#net12}
N 2630 -1120 2700 -1120 { lab=VGND}
N 2040 -1160 2040 -1070 { lab=VTUN}
N 1860 -1330 1940 -1330 { lab=VINJ}
N 2230 -1450 2230 -1370 { lab=#net9}
N 2230 -1510 2570 -1510 { lab=#net11}
N 1860 -1590 1940 -1590 { lab=VINJ}
N 1350 -1330 1430 -1330 { lab=VINJ}
N 1610 -1620 1610 -1420 { lab=GATE1}
N 2020 -1540 2020 -1480 { lab=VIN11}
N 1980 -1280 2020 -1280 { lab=VIN12}
N 2020 -1280 2020 -1220 { lab=VIN12}
N 1720 -1670 1720 -1510 { lab=VINJ}
N 1720 -1250 1750 -1250 { lab=VINJ}
N 1750 -1670 1750 -1250 { lab=VINJ}
N 2600 -1350 2600 -1260 { lab=#net15}
N 2600 -1350 2890 -1350 { lab=#net15}
N 2890 -1350 2890 -1250 { lab=#net15}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 2210 -1220 0 0 {name=M3
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 2160 -1190 0 0 {name=C1 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1950 -1220 0 1 {name=M5
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 2000 -1190 0 1 {name=C2 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1910 -1300 0 0 {name=M1
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 2210 -1480 0 0 {name=M2
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 2160 -1450 0 0 {name=C3 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1950 -1480 0 1 {name=M4
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 2000 -1450 0 1 {name=C4 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1910 -1560 0 0 {name=M6
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1700 -1220 0 0 {name=M7
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 1650 -1190 0 0 {name=C5 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1440 -1220 0 1 {name=M8
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 1490 -1190 0 1 {name=C6 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1400 -1300 0 0 {name=M9
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1700 -1480 0 0 {name=M10
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 1650 -1450 0 0 {name=C7 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1440 -1480 0 1 {name=M11
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/cap_var_hvt.sym} 1490 -1450 0 1 {name=C8 model=cap_var_hvt W=0.5 L=0.5 VM=1 spiceprefix=X}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1400 -1560 0 0 {name=M12
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/iopin.sym} 1780 -1620 0 0 {name=VPWR lab=VPWR}
C {devices/iopin.sym} 1610 -1620 0 0 {name=GATE1 lab=GATE1}
C {devices/iopin.sym} 1540 -1630 0 0 {name=VTUN lab=VTUN}
C {devices/iopin.sym} 1410 -1630 0 0 {name=COLSEL1 lab=COLSEL1}
C {devices/iopin.sym} 1340 -1640 0 0 {name=VINJ lab=VINJ}
C {devices/iopin.sym} 1890 -1610 0 0 {name=COLSEL2 lab=COLSEL2}
C {devices/iopin.sym} 2120 -1610 0 0 {name=GATE2 lab=GATE2}
C {sky130_fd_pr/pfet_01v8.sym} 2710 -1570 0 0 {name=M18
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2640 -1570 0 1 {name=M19
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2710 -1480 0 0 {name=M20
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2600 -1430 0 0 {name=M21
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2480 -1430 0 1 {name=M22
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2550 -1480 0 1 {name=M23
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/iopin.sym} 2770 -1530 0 0 {name=OUTPUT1 lab=OUTPUT1}
C {sky130_fd_pr/pfet_01v8.sym} 2820 -1290 0 0 {name=M24
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2750 -1290 0 1 {name=M25
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2820 -1200 0 0 {name=M26
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2710 -1150 0 0 {name=M27
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2590 -1150 0 1 {name=M28
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 2660 -1200 0 1 {name=M29
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/iopin.sym} 2880 -1250 0 0 {name=OUTPUT2 lab=OUTPUT2}
C {devices/iopin.sym} 2020 -1560 0 0 {name=VIN11 lab=VIN11}
C {devices/iopin.sym} 2410 -1150 0 0 {name=VIN21 lab=VIN21}
C {devices/iopin.sym} 1980 -1330 0 0 {name=VIN12 lab=VIN12}
C {devices/iopin.sym} 2590 -1260 0 0 {name=VIN22 lab=VIN22}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 2440 -1210 0 0 {name=M13
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 2560 -1260 0 1 {name=M14
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/gnd.sym} 2660 -1120 0 0 {name=l1 lab=VGND}
C {devices/gnd.sym} 2570 -1400 0 0 {name=l2 lab=VGND}
C {devices/iopin.sym} 2700 -1080 0 0 {name=p1 lab=VGND}
C {devices/iopin.sym} 1290 -1130 2 0 {name=DRAIN2 lab=DRAIN2}
C {devices/iopin.sym} 1290 -1390 2 0 {name=DRAIN1 lab=DRAIN1}
