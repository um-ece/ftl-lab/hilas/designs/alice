v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 1750 -1370 1770 -1370 { lab=INPUT}
N 1750 -1370 1750 -1250 { lab=INPUT}
N 1750 -1250 1770 -1250 { lab=INPUT}
N 1810 -1340 1810 -1280 { lab=#net1}
N 1940 -1340 1940 -1280 { lab=#net2}
N 1890 -1370 1900 -1370 { lab=#net1}
N 1890 -1370 1890 -1250 { lab=#net1}
N 1890 -1250 1900 -1250 { lab=#net1}
N 2280 -1360 2280 -1310 { lab=VINJ}
N 2150 -1360 2150 -1310 { lab=#net3}
N 2150 -1330 2240 -1390 { lab=#net3}
N 2190 -1390 2280 -1330 { lab=VINJ}
N 2150 -1430 2150 -1420 { lab=VINJ}
N 2150 -1430 2280 -1430 { lab=VINJ}
N 2280 -1430 2280 -1420 { lab=VINJ}
N 2130 -1390 2150 -1390 { lab=VINJ}
N 2130 -1430 2130 -1390 { lab=VINJ}
N 2130 -1430 2150 -1430 { lab=VINJ}
N 2280 -1390 2300 -1390 { lab=VINJ}
N 2300 -1430 2300 -1390 { lab=VINJ}
N 2280 -1430 2300 -1430 { lab=VINJ}
N 2100 -1430 2130 -1430 { lab=VINJ}
N 2150 -1280 2170 -1280 { lab=VGND}
N 2170 -1280 2170 -1240 { lab=VGND}
N 2150 -1240 2170 -1240 { lab=VGND}
N 2150 -1250 2150 -1240 { lab=VGND}
N 2280 -1280 2300 -1280 { lab=VGND}
N 2300 -1280 2300 -1240 { lab=VGND}
N 2280 -1240 2300 -1240 { lab=VGND}
N 2280 -1250 2280 -1240 { lab=VGND}
N 2220 -1280 2240 -1280 { lab=#net1}
N 2220 -1280 2220 -1180 { lab=#net1}
N 2170 -1240 2280 -1240 { lab=VGND}
N 2120 -1240 2150 -1240 { lab=VGND}
N 1810 -1310 1890 -1310 { lab=#net1}
N 1940 -1310 2010 -1310 { lab=#net2}
N 2010 -1310 2010 -1280 { lab=#net2}
N 2010 -1280 2110 -1280 { lab=#net2}
N 1860 -1310 1860 -1180 { lab=#net1}
N 1860 -1180 2220 -1180 { lab=#net1}
N 1940 -1250 1960 -1250 { lab=VGND}
N 1960 -1250 1960 -1210 { lab=VGND}
N 1940 -1210 1960 -1210 { lab=VGND}
N 1940 -1220 1940 -1210 { lab=VGND}
N 1810 -1250 1830 -1250 { lab=VGND}
N 1830 -1250 1830 -1210 { lab=VGND}
N 1810 -1210 1830 -1210 { lab=VGND}
N 1810 -1220 1810 -1210 { lab=VGND}
N 1830 -1210 1940 -1210 { lab=VGND}
N 2120 -1240 2120 -1210 { lab=VGND}
N 1960 -1210 2120 -1210 { lab=VGND}
N 1810 -1370 1830 -1370 { lab=VPWR}
N 1830 -1410 1830 -1370 { lab=VPWR}
N 1810 -1410 1830 -1410 { lab=VPWR}
N 1810 -1410 1810 -1400 { lab=VPWR}
N 1940 -1370 1960 -1370 { lab=VPWR}
N 1960 -1410 1960 -1370 { lab=VPWR}
N 1940 -1410 1960 -1410 { lab=VPWR}
N 1940 -1410 1940 -1400 { lab=VPWR}
N 1830 -1410 1940 -1410 { lab=VPWR}
N 1810 -1420 1810 -1410 { lab=VPWR}
N 1800 -1420 1810 -1420 { lab=VPWR}
N 2280 -1330 2300 -1330 { lab=VINJ}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 1790 -1250 0 0 {name=M1
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8.sym} 1790 -1370 0 0 {name=M2
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 2170 -1390 0 1 {name=M3
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 2130 -1280 0 0 {name=M4
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 1920 -1250 0 0 {name=M5
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8.sym} 1920 -1370 0 0 {name=M6
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 2260 -1390 0 0 {name=M7
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 2260 -1280 0 0 {name=M8
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 1750 -1310 0 0 {name=p1 lab=INPUT}
C {ipin.sym} 1800 -1420 0 0 {name=p2 lab=VPWR}
C {ipin.sym} 2100 -1430 0 0 {name=p3 lab=VINJ}
C {ipin.sym} 2120 -1230 0 0 {name=p4 lab=VGND}
C {opin.sym} 2300 -1330 0 0 {name=p5 lab=OUTPUT}
