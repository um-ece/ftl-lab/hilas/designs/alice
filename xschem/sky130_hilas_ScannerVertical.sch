v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -70 -1540 -70 -1520 { lab=#net1}
N -70 -1540 -20 -1540 { lab=#net1}
N 190 -1520 190 -1430 { lab=#net2}
N -330 -1430 190 -1430 { lab=#net2}
N -330 -1430 -330 -1330 { lab=#net2}
N -330 -1330 -280 -1330 { lab=#net2}
N -70 -1310 -50 -1310 { lab=#net3}
N -50 -1330 -50 -1310 { lab=#net3}
N -50 -1330 -20 -1330 { lab=#net3}
N -170 -1450 90 -1450 { lab=#net4}
N -170 -1240 90 -1240 { lab=#net4}
N -130 -1390 -130 -1380 { lab=#net5}
N -130 -1390 130 -1390 { lab=#net5}
N 130 -1390 130 -1380 { lab=#net5}
N -130 -1610 -130 -1590 { lab=#net5}
N -130 -1610 130 -1610 { lab=#net5}
N 130 -1610 130 -1590 { lab=#net5}
N -50 -1540 -50 -1420 { lab=#net1}
N -50 -1420 240 -1420 { lab=#net1}
N 240 -1450 240 -1420 { lab=#net1}
N 240 -1450 360 -1450 { lab=#net1}
N 190 -1430 360 -1430 { lab=#net2}
N -50 -1400 -50 -1330 { lab=#net3}
N -50 -1400 310 -1400 { lab=#net3}
N 310 -1410 310 -1400 { lab=#net3}
N 310 -1410 360 -1410 { lab=#net3}
N 190 -1310 240 -1310 { lab=#net6}
N 240 -1390 240 -1310 { lab=#net6}
N 240 -1390 360 -1390 { lab=#net6}
N -80 -1450 -80 -1240 { lab=#net4}
N 360 -1310 360 -1200 { lab=#net4}
N -10 -1200 360 -1200 { lab=#net4}
N -10 -1240 -10 -1200 { lab=#net4}
N -10 -1610 -10 -1390 { lab=#net5}
N 660 -1430 710 -1430 { lab=#net7}
N 710 -1430 710 -1250 { lab=#net7}
N 660 -1310 710 -1310 { lab=#net7}
N 660 -1350 710 -1350 { lab=#net7}
N 660 -1390 710 -1390 { lab=#net7}
N -30 -1500 -20 -1500 {}
N 20 -1590 50 -1590 {}
N 20 -1590 20 -1410 {}
N -210 -1410 20 -1410 {}
N -210 -1410 -210 -1380 {}
N -210 -1590 -190 -1590 {}
N -190 -1590 -190 -1370 {}
N -190 -1370 30 -1370 {}
N 30 -1380 30 -1370 {}
N 30 -1380 50 -1380 {}
N -150 -1500 -30 -1500 {}
N -150 -1500 -150 -1290 {}
N -280 -1290 -150 -1290 {}
N -280 -1500 -280 -1480 {}
N -280 -1480 -30 -1480 {}
N -30 -1480 -30 -1290 {}
N -30 -1290 -20 -1290 {}
C {sky130_hilas_Tgate4Single01.sym} 510 -1370 0 0 {name=x1}
C {sky130_fd_sc_hd__dfrtp.sym} -1340 180 0 0 {}
C {sky130_fd_sc_hd__dfrtp.sym} -1080 180 0 0 {}
C {sky130_fd_sc_hd__dfrtp.sym} -1340 390 0 0 {}
C {sky130_fd_sc_hd__dfrtp.sym} -1080 390 0 0 {}
