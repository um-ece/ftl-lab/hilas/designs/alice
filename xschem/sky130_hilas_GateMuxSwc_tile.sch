v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 410 -80 450 -80 { lab=#net1}
N 410 -80 410 30 { lab=#net1}
N 510 -80 510 30 { lab=SIGOUT1}
N 490 30 510 30 { lab=SIGOUT1}
N 410 30 430 30 { lab=#net1}
N 230 -180 270 -180 { lab=VGSEL}
N 230 -180 230 -70 { lab=VGSEL}
N 330 -180 330 -70 { lab=#net1}
N 310 -70 330 -70 { lab=#net1}
N 230 -70 250 -70 { lab=VGSEL}
N 240 90 280 90 { lab=VGUNSEL}
N 240 90 240 200 { lab=VGUNSEL}
N 340 90 340 200 { lab=#net1}
N 320 200 340 200 { lab=#net1}
N 240 200 260 200 { lab=VGUNSEL}
N 700 -100 700 -90 { lab=SIGIN1}
N 700 -100 810 -100 { lab=SIGIN1}
N 700 -30 810 -30 { lab=SIGOUT1}
N 810 -40 810 -30 { lab=SIGOUT1}
N 750 -250 750 -100 { lab=SIGIN1}
N 510 0 750 -0 { lab=SIGOUT1}
N 750 -30 750 -0 { lab=SIGOUT1}
N 750 0 750 140 { lab=SIGOUT1}
N 330 -110 370 -110 { lab=#net1}
N 370 -110 370 -20 { lab=#net1}
N 370 -20 410 -20 { lab=#net1}
N 340 140 370 140 { lab=#net1}
N 370 -20 370 140 { lab=#net1}
N 70 -130 220 -130 { lab=VGSEL}
N 220 -130 230 -130 { lab=VGSEL}
N 80 150 240 150 { lab=VGUNSEL}
N 300 -180 300 -160 { lab=VINJ}
N 300 -160 790 -160 { lab=VINJ}
N 790 -160 790 -70 { lab=VINJ}
N 790 -70 810 -70 { lab=VINJ}
N 480 -80 480 -50 { lab=VINJ}
N 480 -50 560 -50 { lab=VINJ}
N 560 -160 560 -50 { lab=VINJ}
N 610 -160 610 90 { lab=VINJ}
N 280 -90 280 -70 { lab=VGND}
N 180 -90 280 -90 { lab=VGND}
N 180 -90 180 280 { lab=VGND}
N 180 280 700 280 { lab=VGND}
N 700 280 710 280 { lab=VGND}
N 710 -60 710 280 { lab=VGND}
N 700 -60 710 -60 { lab=VGND}
N 460 10 460 30 { lab=VGND}
N 400 10 460 10 { lab=VGND}
N 400 10 400 280 { lab=VGND}
N 290 180 290 200 { lab=VGND}
N 290 180 360 180 { lab=VGND}
N 360 180 360 280 { lab=VGND}
N 310 90 310 100 { lab=VINJ}
N 310 100 610 100 { lab=VINJ}
N 610 90 610 100 { lab=VINJ}
N 280 -30 280 10 { lab=SelControl1}
N 280 10 310 10 { lab=SelControl1}
N 310 10 310 50 { lab=SelControl1}
N 140 10 280 10 { lab=SelControl1}
N 460 70 870 70 { lab=PROG}
N 870 -70 870 70 { lab=PROG}
N 850 -70 870 -70 { lab=PROG}
N 480 -120 660 -120 { lab=RUN}
N 660 -120 660 -60 { lab=RUN}
N 660 -120 870 -120 { lab=RUN}
N 140 -220 140 10 { lab=SelControl1}
N 110 -220 140 -220 { lab=SelControl1}
N 1440 -70 1480 -70 { lab=#net2}
N 1440 -70 1440 40 { lab=#net2}
N 1540 -70 1540 40 { lab=SIGOUT}
N 1520 40 1540 40 { lab=SIGOUT}
N 1440 40 1460 40 { lab=#net2}
N 1260 -170 1300 -170 { lab=VGSEL}
N 1260 -170 1260 -60 { lab=VGSEL}
N 1360 -170 1360 -60 { lab=#net2}
N 1340 -60 1360 -60 { lab=#net2}
N 1260 -60 1280 -60 { lab=VGSEL}
N 1270 100 1310 100 { lab=VGUNSEL}
N 1270 100 1270 210 { lab=VGUNSEL}
N 1370 100 1370 210 { lab=#net2}
N 1350 210 1370 210 { lab=#net2}
N 1270 210 1290 210 { lab=VGUNSEL}
N 1730 -90 1730 -80 { lab=SIGIN2}
N 1730 -90 1840 -90 { lab=SIGIN2}
N 1730 -20 1840 -20 { lab=SIGOUT}
N 1840 -30 1840 -20 { lab=SIGOUT}
N 1780 -240 1780 -90 { lab=SIGIN2}
N 1540 10 1780 10 { lab=SIGOUT}
N 1780 -20 1780 10 { lab=SIGOUT}
N 1780 10 1780 150 { lab=SIGOUT}
N 1360 -100 1400 -100 { lab=#net2}
N 1400 -100 1400 -10 { lab=#net2}
N 1400 -10 1440 -10 { lab=#net2}
N 1370 150 1400 150 { lab=#net2}
N 1400 -10 1400 150 { lab=#net2}
N 1100 -120 1250 -120 { lab=VGSEL}
N 1250 -120 1260 -120 { lab=VGSEL}
N 1110 160 1270 160 { lab=VGUNSEL}
N 1330 -170 1330 -150 { lab=VINJ}
N 1330 -150 1820 -150 { lab=VINJ}
N 1820 -150 1820 -60 { lab=VINJ}
N 1820 -60 1840 -60 { lab=VINJ}
N 1510 -70 1510 -40 { lab=VINJ}
N 1510 -40 1590 -40 { lab=VINJ}
N 1590 -150 1590 -40 { lab=VINJ}
N 1640 -150 1640 100 { lab=VINJ}
N 1310 -80 1310 -60 { lab=VGND}
N 1210 -80 1310 -80 { lab=VGND}
N 1210 -80 1210 290 { lab=VGND}
N 1210 290 1730 290 { lab=VGND}
N 1730 290 1740 290 { lab=VGND}
N 1740 -50 1740 290 { lab=VGND}
N 1730 -50 1740 -50 { lab=VGND}
N 1490 20 1490 40 { lab=VGND}
N 1430 20 1490 20 { lab=VGND}
N 1430 20 1430 290 { lab=VGND}
N 1320 190 1320 210 { lab=VGND}
N 1320 190 1390 190 { lab=VGND}
N 1390 190 1390 290 { lab=VGND}
N 1340 100 1340 110 { lab=VINJ}
N 1340 110 1640 110 { lab=VINJ}
N 1640 100 1640 110 { lab=VINJ}
N 1310 -20 1310 20 { lab=SelControl2}
N 1310 20 1340 20 { lab=SelControl2}
N 1340 20 1340 60 { lab=SelControl2}
N 1170 20 1310 20 { lab=SelControl2}
N 1490 80 1900 80 { lab=PROG}
N 1900 -60 1900 80 { lab=PROG}
N 1880 -60 1900 -60 { lab=PROG}
N 1510 -110 1690 -110 { lab=RUN}
N 1690 -110 1690 -50 { lab=RUN}
N 1690 -110 1900 -110 { lab=RUN}
N 1170 -210 1170 20 { lab=SelControl2}
N 1140 -210 1170 -210 { lab=SelControl2}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 480 -100 1 0 {name=M1
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 460 50 3 0 {name=M6
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 300 -200 1 0 {name=M2
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 280 -50 3 0 {name=M3
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 310 70 1 0 {name=M4
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 290 220 3 0 {name=M5
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 830 -70 2 0 {name=M7
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 680 -60 0 0 {name=M8
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 70 -130 0 0 {name=p1 lab=VGSEL}
C {ipin.sym} 80 150 0 0 {name=p2 lab=VGUNSEL}
C {ipin.sym} 110 -220 0 0 {name=p3 lab=SelControl1}
C {ipin.sym} 290 240 0 0 {name=p4 lab=SelControlB1}
C {ipin.sym} 300 -220 0 0 {name=p5 lab=SelControlB1}
C {ipin.sym} 870 -50 2 0 {name=p6 lab=PROG}
C {ipin.sym} 870 -120 2 0 {name=p7 lab=RUN}
C {ipin.sym} 750 -250 0 0 {name=p8 lab=SIGIN1}
C {opin.sym} 750 140 0 0 {name=p9 lab=SIGOUT1}
C {ipin.sym} 560 -140 0 0 {name=p10 lab=VINJ}
C {ipin.sym} 710 250 0 0 {name=p11 lab=VGND}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1510 -90 1 0 {name=M9
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 1490 60 3 0 {name=M10
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1330 -190 1 0 {name=M11
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 1310 -40 3 0 {name=M12
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1340 80 1 0 {name=M13
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 1320 230 3 0 {name=M14
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1860 -60 2 0 {name=M15
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 1710 -50 0 0 {name=M16
L=0.5
W=5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 1100 -120 0 0 {name=p12 lab=VGSEL}
C {ipin.sym} 1110 160 0 0 {name=p13 lab=VGUNSEL}
C {ipin.sym} 1140 -210 0 0 {name=p14 lab=SelControl2}
C {ipin.sym} 1320 250 0 0 {name=p15 lab=SelControlB2}
C {ipin.sym} 1330 -210 0 0 {name=p16 lab=SelControlB2}
C {ipin.sym} 1900 -40 2 0 {name=p17 lab=PROG}
C {ipin.sym} 1900 -110 2 0 {name=p18 lab=RUN}
C {ipin.sym} 1780 -240 0 0 {name=p19 lab=SIGIN2}
C {opin.sym} 1780 150 0 0 {name=p20 lab=SIGOUT2}
C {ipin.sym} 1590 -130 0 0 {name=p21 lab=VINJ}
C {ipin.sym} 1740 260 0 0 {name=p22 lab=VGND}
