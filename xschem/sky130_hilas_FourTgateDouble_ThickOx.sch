v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 960 -1780 960 -1770 { lab=#net1}
N 960 -1780 1070 -1780 { lab=#net1}
N 960 -1710 1070 -1710 { lab=#net2}
N 1070 -1720 1070 -1710 { lab=#net2}
N 960 -1680 960 -1670 { lab=#net3}
N 960 -1680 1070 -1680 { lab=#net3}
N 960 -1610 1070 -1610 { lab=#net4}
N 1070 -1620 1070 -1610 { lab=#net4}
N 960 -1580 960 -1570 { lab=#net5}
N 960 -1580 1070 -1580 { lab=#net5}
N 960 -1510 1070 -1510 { lab=#net6}
N 1070 -1520 1070 -1510 { lab=#net6}
N 960 -1480 960 -1470 { lab=#net7}
N 960 -1480 1070 -1480 { lab=#net7}
N 960 -1410 1070 -1410 { lab=#net8}
N 1070 -1420 1070 -1410 { lab=#net8}
N 880 -1740 920 -1740 { lab=#net9}
N 880 -1740 880 -1440 { lab=#net9}
N 880 -1440 920 -1440 { lab=#net9}
N 880 -1640 920 -1640 { lab=#net9}
N 880 -1540 920 -1540 { lab=#net9}
N 820 -1480 960 -1480 { lab=#net7}
N 1110 -1750 1140 -1750 { lab=#net10}
N 1140 -1750 1140 -1450 { lab=#net10}
N 1110 -1450 1140 -1450 { lab=#net10}
N 1110 -1550 1140 -1550 { lab=#net10}
N 1110 -1650 1140 -1650 { lab=#net10}
N 830 -1680 960 -1680 { lab=#net3}
N 830 -1580 960 -1580 { lab=#net5}
N 840 -1780 960 -1780 { lab=#net1}
N 1070 -1710 1200 -1710 { lab=#net2}
N 1070 -1610 1200 -1610 { lab=#net4}
N 1070 -1510 1200 -1510 { lab=#net6}
N 1070 -1410 1200 -1410 { lab=#net8}
N 960 -1440 980 -1440 { lab=#net11}
N 980 -1800 980 -1440 { lab=#net11}
N 960 -1740 980 -1740 { lab=#net11}
N 960 -1640 980 -1640 { lab=#net11}
N 960 -1540 980 -1540 { lab=#net11}
N 1030 -1750 1070 -1750 { lab=#net12}
N 1030 -1750 1030 -1380 { lab=#net12}
N 1030 -1450 1070 -1450 { lab=#net12}
N 1030 -1550 1070 -1550 { lab=#net12}
N 1030 -1650 1070 -1650 { lab=#net12}
N 1460 -1780 1460 -1770 { lab=#net1}
N 1460 -1780 1570 -1780 { lab=#net1}
N 1460 -1710 1570 -1710 { lab=#net13}
N 1570 -1720 1570 -1710 { lab=#net13}
N 1460 -1680 1460 -1670 { lab=#net3}
N 1460 -1680 1570 -1680 { lab=#net3}
N 1460 -1610 1570 -1610 { lab=#net14}
N 1570 -1620 1570 -1610 { lab=#net14}
N 1460 -1580 1460 -1570 { lab=#net5}
N 1460 -1580 1570 -1580 { lab=#net5}
N 1460 -1510 1570 -1510 { lab=#net15}
N 1570 -1520 1570 -1510 { lab=#net15}
N 1460 -1480 1460 -1470 { lab=#net7}
N 1460 -1480 1570 -1480 { lab=#net7}
N 1460 -1410 1570 -1410 { lab=#net16}
N 1570 -1420 1570 -1410 { lab=#net16}
N 1380 -1740 1420 -1740 { lab=#net17}
N 1380 -1740 1380 -1440 { lab=#net17}
N 1380 -1440 1420 -1440 { lab=#net17}
N 1380 -1640 1420 -1640 { lab=#net17}
N 1380 -1540 1420 -1540 { lab=#net17}
N 1320 -1480 1460 -1480 { lab=#net7}
N 1610 -1750 1640 -1750 { lab=#net18}
N 1640 -1750 1640 -1450 { lab=#net18}
N 1610 -1450 1640 -1450 { lab=#net18}
N 1610 -1550 1640 -1550 { lab=#net18}
N 1610 -1650 1640 -1650 { lab=#net18}
N 1330 -1680 1460 -1680 { lab=#net3}
N 1330 -1580 1460 -1580 { lab=#net5}
N 1340 -1780 1460 -1780 { lab=#net1}
N 1570 -1710 1700 -1710 { lab=#net13}
N 1570 -1610 1700 -1610 { lab=#net14}
N 1570 -1510 1700 -1510 { lab=#net15}
N 1570 -1410 1700 -1410 { lab=#net16}
N 1460 -1440 1480 -1440 { lab=#net19}
N 1480 -1800 1480 -1440 { lab=#net19}
N 1460 -1740 1480 -1740 { lab=#net19}
N 1460 -1640 1480 -1640 { lab=#net19}
N 1460 -1540 1480 -1540 { lab=#net19}
N 1530 -1750 1570 -1750 { lab=#net20}
N 1530 -1750 1530 -1380 { lab=#net20}
N 1530 -1450 1570 -1450 { lab=#net20}
N 1530 -1550 1570 -1550 { lab=#net20}
N 1530 -1650 1570 -1650 { lab=#net20}
N 1070 -1780 1340 -1780 { lab=#net1}
N 1070 -1680 1330 -1680 { lab=#net3}
N 1070 -1580 1330 -1580 { lab=#net5}
N 1070 -1480 1330 -1480 { lab=#net7}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 940 -1740 0 0 {name=M1
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1090 -1750 2 0 {name=M2
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 940 -1640 0 0 {name=M3
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1090 -1650 2 0 {name=M4
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 940 -1540 0 0 {name=M5
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1090 -1550 2 0 {name=M6
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 940 -1440 0 0 {name=M7
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1090 -1450 2 0 {name=M8
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 980 -1800 0 0 {name=p1 lab=VINJ}
C {ipin.sym} 840 -1780 0 0 {name=p2 lab=IN1}
C {ipin.sym} 830 -1680 0 0 {name=p3 lab=IN2}
C {ipin.sym} 830 -1580 0 0 {name=p4 lab=IN3}
C {ipin.sym} 820 -1480 0 0 {name=p5 lab=IN4}
C {ipin.sym} 1030 -1380 0 0 {name=p6 lab=VGND}
C {opin.sym} 1200 -1710 0 0 {name=p7 lab=OUT1P}
C {opin.sym} 1200 -1610 0 0 {name=p8 lab=OUT2P}
C {opin.sym} 1200 -1510 0 0 {name=p9 lab=OUT3P}
C {opin.sym} 1200 -1410 0 0 {name=p10 lab=OUT4P}
C {ipin.sym} 880 -1740 0 0 {name=p11 lab=PROG}
C {ipin.sym} 1140 -1750 2 0 {name=p12 lab=RUN1}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 1440 -1740 0 0 {name=M9
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1590 -1750 2 0 {name=M10
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 1440 -1640 0 0 {name=M11
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1590 -1650 2 0 {name=M12
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 1440 -1540 0 0 {name=M13
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1590 -1550 2 0 {name=M14
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 1440 -1440 0 0 {name=M15
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1590 -1450 2 0 {name=M16
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 1480 -1800 0 0 {name=p13 lab=VINJ}
C {ipin.sym} 1530 -1380 0 0 {name=p18 lab=VGND}
C {opin.sym} 1700 -1710 0 0 {name=p19 lab=OUT1R}
C {opin.sym} 1700 -1610 0 0 {name=p20 lab=OUT2R}
C {opin.sym} 1700 -1510 0 0 {name=p21 lab=OUT3R}
C {opin.sym} 1700 -1410 0 0 {name=p22 lab=OUT4R}
C {ipin.sym} 1380 -1740 0 0 {name=p23 lab=RUN2}
C {ipin.sym} 1640 -1750 2 0 {name=p24 lab=PROG}
