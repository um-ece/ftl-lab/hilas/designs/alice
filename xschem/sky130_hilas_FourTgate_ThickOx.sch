v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 960 -1780 960 -1770 { lab=IN1}
N 960 -1780 1070 -1780 { lab=IN1}
N 960 -1710 1070 -1710 { lab=OUT1}
N 1070 -1720 1070 -1710 { lab=OUT1}
N 960 -1680 960 -1670 { lab=IN2}
N 960 -1680 1070 -1680 { lab=IN2}
N 960 -1610 1070 -1610 { lab=OUT2}
N 1070 -1620 1070 -1610 { lab=OUT2}
N 960 -1580 960 -1570 { lab=IN3}
N 960 -1580 1070 -1580 { lab=IN3}
N 960 -1510 1070 -1510 { lab=OUT3}
N 1070 -1520 1070 -1510 { lab=OUT3}
N 960 -1480 960 -1470 { lab=IN4}
N 960 -1480 1070 -1480 { lab=IN4}
N 960 -1410 1070 -1410 { lab=OUT4}
N 1070 -1420 1070 -1410 { lab=OUT4}
N 880 -1740 920 -1740 { lab=PROG}
N 880 -1740 880 -1440 { lab=PROG}
N 880 -1440 920 -1440 { lab=PROG}
N 880 -1640 920 -1640 { lab=PROG}
N 880 -1540 920 -1540 { lab=PROG}
N 820 -1480 960 -1480 { lab=IN4}
N 1110 -1750 1140 -1750 { lab=RUN}
N 1140 -1750 1140 -1450 { lab=RUN}
N 1110 -1450 1140 -1450 { lab=RUN}
N 1110 -1550 1140 -1550 { lab=RUN}
N 1110 -1650 1140 -1650 { lab=RUN}
N 830 -1680 960 -1680 { lab=IN2}
N 830 -1580 960 -1580 { lab=IN3}
N 840 -1780 960 -1780 { lab=IN1}
N 1070 -1710 1200 -1710 { lab=OUT1}
N 1070 -1610 1200 -1610 { lab=OUT2}
N 1070 -1510 1200 -1510 { lab=OUT3}
N 1070 -1410 1200 -1410 { lab=OUT4}
N 960 -1440 980 -1440 { lab=VINJ}
N 980 -1800 980 -1440 { lab=VINJ}
N 960 -1740 980 -1740 { lab=VINJ}
N 960 -1640 980 -1640 { lab=VINJ}
N 960 -1540 980 -1540 { lab=VINJ}
N 1030 -1750 1070 -1750 { lab=VGND}
N 1030 -1750 1030 -1380 { lab=VGND}
N 1030 -1450 1070 -1450 { lab=VGND}
N 1030 -1550 1070 -1550 { lab=VGND}
N 1030 -1650 1070 -1650 { lab=VGND}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 940 -1740 0 0 {name=M1
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1090 -1750 2 0 {name=M2
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 940 -1640 0 0 {name=M3
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1090 -1650 2 0 {name=M4
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 940 -1540 0 0 {name=M5
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1090 -1550 2 0 {name=M6
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_g5v0d10v5.sym} 940 -1440 0 0 {name=M7
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_g5v0d10v5.sym} 1090 -1450 2 0 {name=M8
L=0.5
W=0.5
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {ipin.sym} 980 -1800 0 0 {name=p1 lab=VINJ}
C {ipin.sym} 840 -1780 0 0 {name=p2 lab=IN1}
C {ipin.sym} 830 -1680 0 0 {name=p3 lab=IN2}
C {ipin.sym} 830 -1580 0 0 {name=p4 lab=IN3}
C {ipin.sym} 820 -1480 0 0 {name=p5 lab=IN4}
C {ipin.sym} 1030 -1380 0 0 {name=p6 lab=VGND}
C {opin.sym} 1200 -1710 0 0 {name=p7 lab=OUT1}
C {opin.sym} 1200 -1610 0 0 {name=p8 lab=OUT2}
C {opin.sym} 1200 -1510 0 0 {name=p9 lab=OUT3}
C {opin.sym} 1200 -1410 0 0 {name=p10 lab=OUT4}
C {ipin.sym} 880 -1740 0 0 {name=p11 lab=PROG}
C {ipin.sym} 1140 -1750 2 0 {name=p12 lab=RUN}
