v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 770 -300 770 -220 { lab=VPWR}
N 770 -220 790 -220 { lab=VPWR}
N 840 -300 860 -300 { lab=OUTPUT}
N 860 -300 860 -220 { lab=OUTPUT}
N 850 -220 860 -220 { lab=OUTPUT}
N 770 -300 780 -300 { lab=VPWR}
N 900 -300 900 -220 { lab=OUTPUT}
N 900 -220 920 -220 { lab=OUTPUT}
N 970 -300 990 -300 { lab=VGND}
N 990 -300 990 -220 { lab=VGND}
N 980 -220 990 -220 { lab=VGND}
N 900 -300 910 -300 { lab=OUTPUT}
N 570 -270 570 -230 { lab=#net1}
N 860 -260 900 -260 { lab=OUTPUT}
N 510 -300 530 -300 { lab=ENABLE}
N 510 -300 510 -200 { lab=ENABLE}
N 510 -200 530 -200 { lab=ENABLE}
N 570 -300 590 -300 { lab=VPWR}
N 590 -340 590 -300 { lab=VPWR}
N 570 -340 590 -340 { lab=VPWR}
N 570 -340 570 -330 { lab=VPWR}
N 570 -200 590 -200 { lab=VGND}
N 590 -200 590 -170 { lab=VGND}
N 570 -170 590 -170 { lab=VGND}
N 950 -230 950 -220 { lab=VGND}
N 820 -230 950 -230 { lab=VGND}
N 820 -230 820 -220 { lab=VGND}
N 660 -230 820 -230 { lab=VGND}
N 660 -230 660 -190 { lab=VGND}
N 590 -190 660 -190 { lab=VGND}
N 950 -250 950 -230 { lab=VGND}
N 950 -250 990 -250 { lab=VGND}
N 940 -300 940 -280 { lab=VPWR}
N 810 -280 940 -280 { lab=VPWR}
N 810 -300 810 -280 { lab=VPWR}
N 770 -280 810 -280 { lab=VPWR}
N 650 -280 770 -280 { lab=VPWR}
N 650 -340 650 -280 { lab=VPWR}
N 590 -340 650 -340 { lab=VPWR}
N 570 -250 710 -250 { lab=#net1}
N 710 -340 710 -250 { lab=#net1}
N 710 -250 710 -140 { lab=#net1}
N 710 -140 950 -140 { lab=#net1}
N 950 -180 950 -140 { lab=#net1}
N 820 -180 820 -160 { lab=ENABLE}
N 480 -160 820 -160 { lab=ENABLE}
N 480 -240 480 -160 { lab=ENABLE}
N 480 -240 510 -240 { lab=ENABLE}
N 480 -360 480 -240 { lab=ENABLE}
N 480 -360 940 -360 { lab=ENABLE}
N 940 -360 940 -340 { lab=ENABLE}
N 880 -260 880 -170 { lab=OUTPUT}
N 880 -170 890 -170 { lab=OUTPUT}
N 710 -340 810 -340 { lab=#net1}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 550 -200 0 0 {name=M1
L=0.2
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8.sym} 550 -300 0 0 {name=M2
L=0.2
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 820 -200 3 0 {name=M3
L=0.2
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8.sym} 810 -320 1 0 {name=M4
L=0.2
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/nfet_01v8.sym} 950 -200 3 0 {name=M5
L=0.2
W=0.42
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/pfet_01v8.sym} 940 -320 1 0 {name=M6
L=0.2
W=0.42
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {ipin.sym} 480 -270 0 0 {name=p1 lab=ENABLE}
C {ipin.sym} 570 -340 0 0 {name=p2 lab=VPWR}
C {ipin.sym} 660 -230 0 0 {name=p3 lab=VGND}
C {opin.sym} 890 -170 0 0 {name=p4 lab=OUTPUT}
