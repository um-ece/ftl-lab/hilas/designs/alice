v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 1110 -1430 1330 -1430 { lab=VIN12}
N 1370 -1400 1370 -1320 { lab=#net1}
N 900 -1460 900 -1300 { lab=VPWR}
N 900 -1720 900 -1460 { lab=VPWR}
N 1390 -1430 1390 -1350 { lab=VPWR}
N 1370 -1430 1390 -1430 { lab=VPWR}
N 1370 -1690 1390 -1690 { lab=VPWR}
N 1390 -1690 1390 -1610 { lab=VPWR}
N 900 -1830 920 -1830 { lab=VPWR}
N 1810 -1780 1830 -1780 { lab=#net2}
N 1870 -1780 1890 -1780 { lab=VPWR}
N 1890 -1810 1890 -1780 { lab=VPWR}
N 1740 -1810 1890 -1810 { lab=VPWR}
N 1740 -1810 1740 -1780 { lab=VPWR}
N 1740 -1780 1760 -1780 { lab=VPWR}
N 1870 -1740 1870 -1720 { lab=OUTPUT1}
N 1870 -1740 1910 -1740 { lab=OUTPUT1}
N 1760 -1730 1760 -1670 { lab=#net2}
N 1710 -1690 1830 -1690 { lab=#net3}
N 1710 -1720 1710 -1690 { lab=#net3}
N 1640 -1640 1720 -1640 { lab=#net1}
N 1640 -1670 1640 -1640 { lab=#net1}
N 1590 -1640 1600 -1640 { lab=VGND}
N 1590 -1640 1590 -1610 { lab=VGND}
N 1590 -1610 1660 -1610 { lab=VGND}
N 1660 -1610 1770 -1610 { lab=VGND}
N 1770 -1640 1770 -1610 { lab=VGND}
N 1760 -1640 1770 -1640 { lab=VGND}
N 1870 -1690 1880 -1690 { lab=VGND}
N 1880 -1690 1880 -1660 { lab=VGND}
N 1770 -1660 1880 -1660 { lab=VGND}
N 1770 -1660 1770 -1640 { lab=VGND}
N 1660 -1690 1670 -1690 { lab=VGND}
N 1660 -1660 1660 -1610 { lab=VGND}
N 1660 -1660 1670 -1660 { lab=VGND}
N 1920 -1500 1940 -1500 { lab=#net4}
N 1980 -1500 2000 -1500 { lab=VPWR}
N 2000 -1530 2000 -1500 { lab=VPWR}
N 1850 -1530 2000 -1530 { lab=VPWR}
N 1850 -1530 1850 -1500 { lab=VPWR}
N 1850 -1500 1870 -1500 { lab=VPWR}
N 1980 -1460 1980 -1440 { lab=OUTPUT2}
N 1980 -1460 2020 -1460 { lab=OUTPUT2}
N 1870 -1440 1870 -1390 { lab=#net4}
N 1820 -1410 1940 -1410 { lab=#net5}
N 1820 -1440 1820 -1410 { lab=#net5}
N 1750 -1360 1830 -1360 { lab=#net6}
N 1750 -1390 1750 -1360 { lab=#net6}
N 1700 -1360 1710 -1360 { lab=VGND}
N 1700 -1360 1700 -1330 { lab=VGND}
N 1700 -1330 1770 -1330 { lab=VGND}
N 1840 -1330 1880 -1330 { lab=VGND}
N 1880 -1360 1880 -1330 { lab=VGND}
N 1870 -1360 1880 -1360 { lab=VGND}
N 1980 -1410 1990 -1410 { lab=VGND}
N 1990 -1410 1990 -1380 { lab=VGND}
N 1880 -1380 1990 -1380 { lab=VGND}
N 1880 -1380 1880 -1360 { lab=VGND}
N 1770 -1410 1780 -1410 { lab=VGND}
N 1770 -1380 1770 -1330 { lab=VGND}
N 1770 -1380 1780 -1380 { lab=VGND}
N 1370 -1580 1370 -1460 { lab=#net7}
N 1370 -1320 1470 -1320 { lab=#net1}
N 900 -1860 900 -1830 { lab=VPWR}
N 900 -1860 1740 -1860 { lab=VPWR}
N 1740 -1860 1740 -1810 { lab=VPWR}
N 1470 -1670 1640 -1670 { lab=#net1}
N 1680 -1440 1820 -1440 { lab=#net5}
N 1600 -1390 1750 -1390 { lab=#net6}
N 1720 -1470 1730 -1470 { lab=VIN22}
N 1540 -1420 1560 -1420 { lab=VIN21}
N 1540 -1420 1540 -1360 { lab=VIN21}
N 1540 -1360 1550 -1360 { lab=VIN21}
N 1600 -1500 1680 -1500 { lab=#net8}
N 1600 -1500 1600 -1450 { lab=#net8}
N 1390 -1610 1510 -1610 { lab=VPWR}
N 1510 -1610 1510 -1520 { lab=VPWR}
N 1620 -1520 1660 -1520 { lab=VPWR}
N 1660 -1520 1660 -1470 { lab=VPWR}
N 1660 -1470 1680 -1470 { lab=VPWR}
N 1600 -1420 1620 -1420 { lab=VPWR}
N 1620 -1520 1620 -1420 { lab=VPWR}
N 2000 -1530 2060 -1530 { lab=VPWR}
N 2060 -1810 2060 -1530 { lab=VPWR}
N 1890 -1810 2060 -1810 { lab=VPWR}
N 1760 -1730 1810 -1730 { lab=#net2}
N 1810 -1780 1810 -1730 { lab=#net2}
N 1870 -1440 1920 -1440 { lab=#net4}
N 1920 -1500 1920 -1440 { lab=#net4}
N 1160 -1770 1160 -1750 { lab=VIN11}
N 1840 -1330 1840 -1290 { lab=VGND}
N 900 -1830 900 -1720 { lab=VPWR}
N 1870 -1750 1870 -1740 { lab=OUTPUT1}
N 1660 -1690 1660 -1660 { lab=VGND}
N 1980 -1470 1980 -1460 { lab=OUTPUT2}
N 1770 -1410 1770 -1380 { lab=VGND}
N 1470 -1670 1470 -1320 { lab=#net1}
N 1500 -1500 1600 -1500 { lab=#net8}
N 1510 -1520 1620 -1520 { lab=VPWR}
N 1760 -1750 1760 -1730 { lab=#net2}
N 1800 -1780 1810 -1780 { lab=#net2}
N 1870 -1470 1870 -1440 { lab=#net4}
N 1910 -1500 1920 -1500 { lab=#net4}
N 1770 -1330 1840 -1330 { lab=VGND}
N 1370 -1660 1370 -1580 { lab=#net7}
N 1370 -1720 1710 -1720 { lab=#net3}
N 1160 -1750 1160 -1690 { lab=VIN11}
N 1160 -1690 1330 -1690 { lab=VIN11}
N 900 -1300 900 -1290 { lab=VPWR}
N 1110 -1540 1110 -1430 { lab=VIN12}
N 1110 -1540 1120 -1540 { lab=VIN12}
N 910 -1610 1390 -1610 { lab=VPWR}
N 1390 -1350 1390 -1290 { lab=VPWR}
N 900 -1610 910 -1610 { lab=VPWR}
N 1740 -1550 2030 -1550 { lab=VIN22}
N 2030 -1550 2030 -1460 { lab=VIN22}
N 1920 -1740 1920 -1570 { lab=VIN12}
N 900 -1290 1390 -1290 { lab=VPWR}
N 1500 -1500 1500 -1260 { lab=#net8}
N 1000 -1260 1500 -1260 { lab=#net8}
N 1000 -1430 1000 -1260 { lab=#net8}
N 1250 -1570 1920 -1570 { lab=VIN12}
N 1250 -1570 1250 -1430 { lab=VIN12}
N 1730 -1550 1740 -1550 { lab=VIN22}
N 1720 -1550 1730 -1550 { lab=VIN22}
N 1720 -1550 1720 -1470 { lab=VIN22}
N 1000 -1670 1000 -1610 { lab=VPWR}
N 1000 -1610 1000 -1490 { lab=VPWR}
N 1040 -1770 1040 -1710 { lab=VBias}
N 820 -1770 1040 -1770 { lab=VBias}
N 1040 -1710 1040 -1700 { lab=VBias}
N 730 -1610 820 -1610 { lab=VBias}
N 940 -1590 1370 -1590 { lab=#net7}
N 940 -1730 940 -1590 { lab=#net7}
N 940 -1730 1000 -1730 { lab=#net7}
N 920 -1460 960 -1460 { lab=VBias}
N 920 -1770 920 -1460 { lab=VBias}
N 820 -1770 820 -1610 { lab=VBias}
N 1000 -1460 1050 -1460 { lab=VPWR}
N 1050 -1460 1050 -1360 { lab=VPWR}
N 900 -1360 1050 -1360 { lab=VPWR}
N 900 -1700 1000 -1700 { lab=VPWR}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1350 -1430 0 0 {name=M3
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1350 -1690 0 0 {name=M2
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/iopin.sym} 920 -1830 0 0 {name=VPWR lab=VPWR}
C {sky130_fd_pr/pfet_01v8.sym} 1850 -1780 0 0 {name=M18
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1780 -1780 0 1 {name=M19
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1850 -1690 0 0 {name=M20
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1740 -1640 0 0 {name=M21
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1620 -1640 0 1 {name=M22
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1690 -1690 0 1 {name=M23
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/iopin.sym} 1910 -1740 0 0 {name=OUTPUT1 lab=OUTPUT1}
C {sky130_fd_pr/pfet_01v8.sym} 1960 -1500 0 0 {name=M24
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1890 -1500 0 1 {name=M25
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1960 -1410 0 0 {name=M26
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1850 -1360 0 0 {name=M27
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1730 -1360 0 1 {name=M28
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1800 -1410 0 1 {name=M29
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/iopin.sym} 2020 -1460 0 0 {name=OUTPUT2 lab=OUTPUT2}
C {devices/iopin.sym} 1160 -1770 0 0 {name=VIN11 lab=VIN11}
C {devices/iopin.sym} 1550 -1360 0 0 {name=VIN21 lab=VIN21}
C {devices/iopin.sym} 1120 -1540 0 0 {name=VIN12 lab=VIN12}
C {devices/iopin.sym} 1730 -1470 0 0 {name=VIN22 lab=VIN22}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1580 -1420 0 0 {name=M13
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1700 -1470 0 1 {name=M14
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/gnd.sym} 1800 -1330 0 0 {name=l1 lab=VGND}
C {devices/gnd.sym} 1710 -1610 0 0 {name=l2 lab=VGND}
C {devices/iopin.sym} 1840 -1290 0 0 {name=p1 lab=VGND}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 980 -1460 0 0 {name=M1
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1020 -1700 2 0 {name=M4
L=0.5
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/iopin.sym} 730 -1610 2 0 {name=VBias lab=VBias}
