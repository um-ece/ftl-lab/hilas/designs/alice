v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -120 -60 -90 -60 { lab=VPWR}
N -30 -60 -10 -60 { lab=#net1}
N 30 -60 60 -60 { lab=#net1}
N -10 -60 30 -60 { lab=#net1}
N 60 -60 80 -60 { lab=#net1}
N 80 -60 80 -30 { lab=#net1}
N 200 -60 230 -60 { lab=#net1}
N 400 -60 400 -30 { lab=#net2}
N 80 -60 200 -60 { lab=#net1}
N 590 -60 610 -60 { lab=#net3}
N 650 -60 680 -60 { lab=#net3}
N 610 -60 650 -60 { lab=#net3}
N 680 -60 700 -60 { lab=#net3}
N 700 -60 700 -30 { lab=#net3}
N 400 -60 530 -60 { lab=#net2}
N 890 -60 910 -60 { lab=#net4}
N 950 -60 980 -60 { lab=#net4}
N 910 -60 950 -60 { lab=#net4}
N 980 -60 1000 -60 { lab=#net4}
N 1000 -60 1000 -30 { lab=#net4}
N 700 -60 830 -60 { lab=#net3}
N 400 -60 400 -30 { lab=#net2}
N 290 -60 400 -60 { lab=#net2}
N 1160 -60 1180 -60 { lab=VINJ}
N 1220 -60 1250 -60 { lab=VINJ}
N 1180 -60 1220 -60 { lab=VINJ}
N 1250 -60 1270 -60 { lab=VINJ}
N 1270 -60 1270 -30 { lab=VINJ}
N 1000 -60 1100 -60 { lab=#net4}
N 1270 -60 1300 -60 { lab=VINJ}
N -130 -60 -120 -60 { lab=VPWR}
N 90 60 120 60 { lab=CLK1}
N 700 60 740 60 { lab=CLK2}
N 1000 70 1030 70 { lab=CLK2B}
N 1270 70 1310 70 { lab=VGND}
N 80 60 90 60 { lab=CLK1}
N 400 60 420 60 { lab=CLK1B}
N 420 60 450 60 { lab=CLK1B}
N -10 -30 -10 -20 { lab=#net1}
N -10 -30 190 -30 { lab=#net1}
N 190 -30 190 -20 { lab=#net1}
N -10 40 -10 50 { lab=CLK1}
N -10 50 190 50 { lab=CLK1}
N 190 40 190 50 { lab=CLK1}
N 80 50 80 60 { lab=CLK1}
N 300 -30 300 -20 { lab=#net2}
N 300 -30 500 -30 { lab=#net2}
N 500 -30 500 -20 { lab=#net2}
N 300 40 300 50 { lab=CLK1B}
N 300 50 500 50 { lab=CLK1B}
N 500 40 500 50 { lab=CLK1B}
N 400 50 400 60 { lab=CLK1B}
N 610 -30 610 -20 { lab=#net3}
N 610 -30 810 -30 { lab=#net3}
N 810 -30 810 -20 { lab=#net3}
N 610 40 610 50 { lab=CLK2}
N 610 50 810 50 { lab=CLK2}
N 810 40 810 50 { lab=CLK2}
N 700 50 700 60 { lab=CLK2}
N 900 -30 900 -20 { lab=#net4}
N 900 -30 1100 -30 { lab=#net4}
N 1100 -30 1100 -20 { lab=#net4}
N 900 40 900 50 { lab=CLK2B}
N 900 50 1100 50 { lab=CLK2B}
N 1100 40 1100 50 { lab=CLK2B}
N 1180 -30 1180 -20 { lab=VINJ}
N 1180 -30 1380 -30 { lab=VINJ}
N 1380 -30 1380 -20 { lab=VINJ}
N 1180 40 1180 50 { lab=VGND}
N 1180 50 1380 50 { lab=VGND}
N 1380 40 1380 50 { lab=VGND}
N 1000 50 1000 70 { lab=CLK2B}
N 1270 50 1270 70 { lab=VGND}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} -60 -60 1 0 {name=D1
model=diode_pd2nw_11v0
area=6.831}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 260 -60 1 0 {name=D2
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 560 -60 1 0 {name=D3
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 860 -60 1 0 {name=D4
model=diode_pd2nw_11v0
area=6.831
}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/diode.sym} 1130 -60 1 0 {name=D5
model=diode_pd2nw_11v0
area=6.831
}
C {opin.sym} 1290 -60 0 0 {name=p1 lab=VINJ}
C {ipin.sym} -120 -60 0 0 {name=p2 lab=VPWR}
C {ipin.sym} 120 60 2 0 {name=p3 lab=CLK1}
C {ipin.sym} 440 60 2 0 {name=p4 lab=CLK1B}
C {ipin.sym} 740 60 2 0 {name=p5 lab=CLK2}
C {ipin.sym} 1030 70 2 0 {name=p6 lab=CLK2B}
C {ipin.sym} 1310 70 2 0 {name=p7 lab=VGND}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} -10 10 0 0 {name=C1 model=cap_mim_m3_1 W=50 L=4.61 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 190 10 2 0 {name=C2 model=cap_mim_m3_2 W=50 L=4.61 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 300 10 0 0 {name=C3 model=cap_mim_m3_1 W=50 L=4.61 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 500 10 2 0 {name=C4 model=cap_mim_m3_2 W=50 L=4.61 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 610 10 0 0 {name=C5 model=cap_mim_m3_1 W=50 L=4.61 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 810 10 2 0 {name=C6 model=cap_mim_m3_2 W=50 L=4.61 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 900 10 0 0 {name=C7 model=cap_mim_m3_1 W=50 L=4.61 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 1100 10 2 0 {name=C8 model=cap_mim_m3_2 W=50 L=4.61 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_1.sym} 1180 10 0 0 {name=C9 model=cap_mim_m3_1 W=96 L=26 MF=1 spiceprefix=X}
C {/home/hilas/fastlane/pdks/open_pdks/sky130/sky130A/libs.tech/xschem/sky130_fd_pr/cap_mim_m3_2.sym} 1380 10 2 0 {name=C10 model=cap_mim_m3_2 W=96 L=26 MF=1 spiceprefix=X}
