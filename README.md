# ALICE: A listening caraval
# Single Neuromorphic Keyword Recognition IC

This Skywater 130nm implementation project will build a programmable end-to-end microphone-to-symbol keyword classifier. This full system classifier extends the large-scale Field Programmable Analog Array  (FPAA) command-word classifier [1] to allow for larger vocabularies (100-500) by accounting for temporal dynamics in the classification.   

This design utilizes recent machine learning modeling based on Linear delay lines incorporates linear temporal dynamics for neurally-based classifier approaches [2].  The compiled SoC FPAA classifier required 20-30μW for a range of classification tasks [1,3-5].    We expect this new custom classifier component to require 20-40μW when the μP is turned off.   The additional general linear dynamics block which allows for the detection of patterns over time.  This block effectively allows each output category to have a preferred temporal signal instead of being classified based on a single spectral pattern. 

This effort looks to compile this acoustic classifier network in a single Caraval Skywater platform utilizing and modifying the analog Floating-Gate (FG) standard cell library to place and route the final structure.   The analog FG standard cell library, based upon experiences in FPAA devices [6], is currently in fabrication through Skywater 130nm CMOS.   The following sections work through the architecture and circuit description, circuit schematics, target design goals, and development team.  

## Table of Content
1. [Motivation](#motivation)
2. [Description](#description-of-circuit)
3. [Block Design](#block-design)
4. [Standard Cells](#standard-cells)
5. [ALICE Chip](#alice-chip)


## Motivation

1. Pattern recognition in temporal signal (speech) requires inference on edge with limited power budget in real-world conditions (low SNR)
2. The gap between the energy efficiency and effectiveness of animal audio processing pipeline, and the AI counterparts 
3. Lack of open-source analog standard cell libraries and building blocks dedicated to AI inference

## Description of circuit

The keyword recognition IC is a microphone-to-digital output, full end-to-end classifier processing that we expect will require 20-40μW of processing power.   The architecture (Fig. 1) comprises front-end filterbanks and amplitude detection, an LMU processing unit, and a universal approximator classification block.  

The IC starts with the raw microphone input being passed to a set of programmable parallel bandpass filters (16-24) with amplitude detection (Fig. 1).  These bandpass filters will typically be programmed with similar resonances and exponentially increasing corner frequencies over the main human hearing range (50Hz to 5kHz), typical of neurally-based acoustic classifiers (e.g. [1,4]) or high-end hearing aid devices.   The amplitude detection produces a set of analog signals indicating a filtered magnitude of a set of frequencies.  

The filtered frequency channel values are passed to a general linear dynamics block which computes an output x given the input u, where dxdt+Ax = Bu.  For different A and B matrices (configurable using floating gates), this creates a new temporal basis space capable of capturing dynamics on the time scale of τ.  

The output from the linear dynamics block is then passed through a classifier comprising a Vector-Matrix Multiplier (VMM) and a Winner-Take-All (WTA) block.  This classifier is a universal approximator, where adding the linear dynamics (LMU) expands the class of functions that it can approximate given its temporal dynamics.  Without the dynamics, the classifier would only be able to find patterns based on the current magnitudes of various spectrums.  With the dynamics, each category can define a temporal pattern across those inputs.  This approach is the basis of the LMU [2] which outperforms other recurrent methods (LSTMs, GRUs, etc) for detecting temporal patterns.  It can also be seen as an efficient implementation of a large collection of various synaptic kernels, which has also been shown to have advantages for temporal classification [7].   These outputs can be directly passed to the on-board Caravan RISC-V μP or asynchronously transmitted through a verilog-compiled Address-Event Representation (AER) block [8].

Floating-Gate (FG) devices used throughout this design provide large-scale programmability for this custom IC design.  The programming infrastructure will parallel the programming infrastructure used in the SoC FPAA device [5,9], where this IC will utilize the RISC-V μP that is included in the Caraval system.  As this IC design does not require general FG programming (e.g. not fully on switches), the programming infrastructure simplifies from the general approach [9].  The programming structure is capable of precisely programming bias currents from pA to μΑ levels (~7-8 orders of magnitude) with better than 1% accuracy at all levels [9].  The analog FG standard-cell library shows initial designs for these programmable cells, showing these programmable elements are implementable in the Skywater 130nm CMOS process and  reducing the risk of this IC design.

## Block Design

![Figure](Images/block_diagram.png)

![Figure](Images/ALICE_FG_Prog_Structure.png)

The keyword recognition IC is developed in the Caravan frame  where the primarily analog computation will fit into the 10mm2  user area.   The initial analog FG cell library gives some initial estimates for the size of these blocks.    A programmable bandpass filter array and amplitude detection (24 filters) is roughly 0.01mm2, and a VMM+WTA classifier block with 10,000 VMM matrix values and 100 WTA outputs is roughly  0.15mm2. The programming infrastructure,     mostly requiring several 6bit DACs and a ramp ADC [9], would be roughly 0.005mm2.  One expects the charge-pump blocks and other voltage handling blocks will be a similar size to the programming infrastructure size.  The temporal dynamics are based on ladder filter [11]

We expect the final classifier structure to enable vocabularies initially of 100 words that potentially could expand to larger vocabularies.  This structure utilizes testing from the TI digits [12] database (10-20 words) as well as keyword spotting [13] and other databases.  The WTA module will be designed for 1,000 outputs to enable a large number of null outputs that would not be significant events to be transmitted past the classifier.  Only a recognized result would be given.   The number of null elements and the number of valid outputs can be programmed through the FG elements.  We will use a [software simulation](https://github.com/neuromorphs/ant-lmu-benchmark) of the classification system to explore the optimal hyperparameters (e.g. size of the A matrix) to achieve the best accuracy on these datasets given a fixed area.

Design Goals
- Demonstrate ultra low-power recognition of a moderate vocabulary (100) of keyword detection on a single IC utilizing LMU [2] to classify sequences of spectral dynamics. 
- Develop a single microphone-to-symbol classifier as a Caravan IC utilizing an array of programmable bandpass filters [5], programmable amplitude detection [5], programmable linear dynamics [10], and a programmable VMM+WTA classifier block [1,3].      
- Demonstrate on-chip classification of the TI DIGIT dataset [12] and keyword spotting [13]. 
- Demonstration of large-scale programmable analog computation and reduction of device mismatch compiled through analog FG standard cell elements that may include new components added to the library.
- Develop and demonstrate using this hardware platform for different vocabularies through either on-chip (e.g. similar to the approach in [1,3]) or off-chip training. 
- Event-driven output and storage through the on-chip memory using the  RISC-V processor and interrupt handling. 


### Floating-Gate Based Design and Simulation Impact 
This design utilizes Floating-Gate (FG) based analog design, enabling wide range of programmability in these designs.  The programmability includes wide-range programming of bias currents (e.g. 1pA to 10uA) to better than 1% accuracy throughout that range [9], as well as programming out of offset voltages as required.  FG techniques also allow tuning particular circuit linear ranges as required without adding additional noise sources.   The core datapath concept was experimentally demonstrated in multiple places (e.g. [16,17]).  The FG elements enable programming a parameter that will be stored for 10 years with minimal change (e.g. 1-10uV as in [18]).  

Programmability relaxes the amount of required simulation, particularly for computations previously experimentally demonstrated.   Most of the difficult analog circuit issues involve dealing with mismatch and a variety of other setup or environmental issues, and these FG techniques either directly deal with these issues, such as mismatch (e.g. [18]), or techniques have been developed to reduce the effect of these issues, such as temperature (e.g. [19]).   As a result, the required system simulation for such a chip and its datapath are small, particularly as many of the issues are tuned (and retuned / reprogrammed) after fabrication.

### Standard Cell Approach
This design uses the initial FG-enabled Analog Standard Cell library developed at GT and U. of Mississippi [20] built around the initial philosophy seen through generations of FPAA device development and deployment in educational settings [6].    This standard cell library was designed and submitted for fabrication on Skywater 130nm CMOS with the hope of these components to be widely used.   This project, while not explicitly synthesizing the resulting computation, has enabled working through a range of synthesis questions using these components.   The experience gained by some on this team in taping out these devices in summer 2021 provides confidence of finishing this resulting design project.  

<p style="color:red">Note: These designs are in progress and subject to change as things iterate </p>

Layout Progress:  

<img src="Images/alice_full.PNG" alt="ALICE Chip Layout" width="1000px"><br>

## Standard Cells
### Acoustic Front End
The front structure of the datapath consists of 16 Bandpass filters which break up the input signal into constituent frequencies. They are passed to the amplitude detect blocks and the resulting output is delayed 7 times for each frequency band.

Bandpass filters and Amplitude Detect layout:  
<img src="Images/zoomed_front_end.png" alt="zoomed in image of just BPF and AMP detect" width="500px"><br>
Unexpanded Front End Layout (Including Delay blocks):  
<img src="Images/unexpand_front_end_overview.png" alt="all front end layout cells unexpanded" width="500px"><br>
Front end Layout:  
<img src="Images/front_end_to_delay_overview.png" alt="all front end layout cells" width="1000px"><br>




### Charge Pump
5-Stage Charge pump for 6V Hot electron injection. <br>
Schematic:  
<img src="Images/injection_pump.png" alt="injection pump schematic" width="1000px"><br>
Layout:  
<img src="Images/injection_layout.png" alt="injection pump layout" width="1000px"><br>
Tunneling and Injection Pump simulation:  
<img src="Images/charge_pump_sim.png" alt="LTSpice charge pump simulation" width="500px"><br>
Non overlapping clock generator. <br>
Schematic:  
<img src="Images/clock_generator.png" alt="clock generator schematic" width="1000px"><br>
Layout:   
<img src="Images/clock_gen_layout.png" alt="clock generator layout" width="1000px"><br>

The charge pump design for this project is a standard dickson pump to reach the 6V for injection and 12V for tunneling. The standard cell for the injection pump was designed to be 1 stage of the pump, a diode and a capactior in the pitch of 6.5um. To save on area, two parallel capacitors on the metal 3 to mimcap layer and metal 4 to mimcap 2 layer were used. This has the added benefit of shielding the charge stored on layer 4 which acts as our top plate while metal 3 and metal 5 are connected together as the bottom plate. The injection pump uses the same diode-cap standard cell but with 8 intermediate stages instead of 4.  <br>
For the clock generator, we used the skywater high density digital cell library. We chose to add in well ties and substrate ties to the cells as they did not come with that. The individual cells have a pitch of 3.2um with the abutting design having a pitch of 5.92um. Both the charge pump and clock generator designs were adapted from existing FPAA chip designs which implemented these circuits.

### Ramp ADC (In-Progress)
Ramp generator layout:
<img src="Images/ramp_layout.PNG" alt="Ramp generator layout" width="1000px"><br>
Ramp simulation:  
<img src="Images/ramp_simulation.png" alt="LTSpice approximate ramp simulation" width="500px"><br>
ADC comparator and buffer layout:  
<img src="Images/ramp_secondstage.PNG" alt="LTSpice approximate ramp simulation" width="500px"><br>
The ADC design for this project is a ramp adc with a resolution of 16 bits. This module is used to program floating gates by reading the actual voltage the transistor is programmed to vs the target, and sending this to the microprocessor. Existing Bootstrap, 9T OTA, and TX gate designs from the HiLAS standard cell library were used as well as latches and inverters from the skywater high density digital cell library. 

### Scanner Cell
Scan Chain operation  
<img src="Images/scanner_block.png" width="500x">  
 To measure internal nodes, a scanner cell was created. Each cell has a D flip flop and Tgate pair. These chained together form our scan chain. Due to the VMM+WTA structure stacking vertically and the Acoustic Front End stakcing horizontally, two standard cells were created to allow tiling in both directions.

Vertical Scanner Cell  
 The VMM WTA structure has 4 outputs per row which requires 4 scanner cells per row. The dfrtp cell from the skywater high density digital library was used as our D flip flop. The schematic was generated from the layout and is seen below.  
 D flip flop Schematic:  
<img src="Images/dff_sky130.png" width="1000x">  
 To fit 4 scanner cell elements in pitch, a 4 Tgate standard cell was created to fit within pitch  
 4 Tgates:  
<img src="Images/Four_T-gates.png" width="500x">  
  They were then connected in a chain configuration with the output of one cell feeding the input of the next.  
  Vertical Scanner Schematic  
 <img src="Images/Vertical_Scanner_Schematic.png" width="1000x">  
 The cells were designed to ensure that they can be abutted to have the output of the whole chain feed the input of the next chain (where a chain refers to a set of scanner cells). WHile the outputs of the WTA connect to the inputs of the tgates on the left.  
 Vertical Scanner   
  <img src="Images/Vertical_Scanner_Cell_Layout.png" width="1000x">  


### Winner-Take-All (WTA)
WTA Layout:  
<img src="Images/WTA_layout.PNG" alt="WTA layout" width="1000px"><br>

The winner take all block is the final stage of our classifier, taking outputs from the VMM and choosing K-outputs. Each block corresponds to one row of the VMM. 


## ALICE Chip
### Pins (in-progress)
<!---
| Name     | Analog Wrapper Pin |Desciption |
| ----------- | ----------- | ----------- |
| Input      | io_analog[0] | Microphone speech input|
| WTA Outputs   | io_out[0-9]  | Digital address of winner|
|Scanners | io_analog[5-10] | Debugging |
|Bootstrap Resist| io_analog[1] | External resist|
|Charge pumps | io_analog[2-3] | Tunneling and external clock|
|Vinj | io_analog[4] | Injection voltage|
-->
<img src="Images/InitialPinStructure.png" alt="pin structure" width="1000px"><br>
|Pin          | Name        | Operating Voltage | Description  |
| ----------- | ----------- | -----------       | -----------  |
|analog[0]    | SubScanOut  |          1.8V         |   Scans output           |
|analog[3]    | PumpVtun  |          11-12V         |           |
|analog[4]  |   WTAScanOut     |      1.8V             |       Scan WTA output       |   
|analog[6]  |  ResistPin      |       1.8V            |       Bootstrap resist pin       |   
|analog[9]  |  PumpVinj      |        5.5-6V           |              |  
|analog[10] | SigInput       |        1.8V           |       Signal input       |     


## Team

 
Praveen Raj, University of Nottingham Malaysia Campus <praveenraj19802@gmail.com>                                     
Pranav Mathews, Georgia Institute of Technology  
Afolabi Ige, Georgia Institute of Technology   
Professor Jennifer Hasler, Georgia Institute of Technology  
Professor Barry Muldrey, University of Mississippi <muldrey@olemiss.edu>  
Terrence C Stewart, National Research Council Canada terrence.stewart@nrc-cnrc.gc.ca  



## References
[1] J. Hasler and S. Shah, “SoC FPAA Hardware Implementation of a VMM+WTA Embedded Learning Classifier,” IEEE Journal on Emerging and Selected Topics in Circuits and Systems, vol. 8, no. 1, March 2018. pp. 28-37.

[2] Voelker, Aaron R., Ivana Kajic and C. Eliasmith. “Legendre Memory Units: Continuous-Time Representation in Recurrent Neural Networks.” NeurIPS (2019).
[3] S. Shah and J. Hasler, “VMM + WTA Embedded Classifiers Learning Algorithm implementable on SoC FPAA devices,” IEEE Journal on Emerging and Selected Topics in Circuits and Systems, vol. 8, no. 1, March 2018. pp. 65-76.
[4]  J. Hasler, "Large-Scale Field-Programmable Analog Arrays," Proceedings of IEEE, 2020. 

[5] S. George, S. Kim, S. Shah, et. al, "A Programmable and Configurable Mixed-Mode FPAA SOC,” IEEE Transactions on VLSI, vol. 24, no. 6, 2016. pp. 2253-2261. 

[6] J. Hasler, “Defining Analog Standard Cell Libraries for Mixed-Signal Computing enabled through Educational Directions,” IEEE ISCAS, 2020.

[7] Tapson, J. C., Cohen, G. K., Afshar, S., Stiefel, K. M., Buskila, Y., Wang, R. M., Hamilton, T. J., & van Schaik, A. (2013). Synthesis of neural networks for spatio-temporal spike pattern recognition and processing. Frontiers in neuroscience, 7, 153. https://doi.org/10.3389/fnins.2013.00153.  

[8] S. Brink, S. Nease, Hasler, S. Ramakrishnan, R. Wunderlich, A. Basu, and B. Degnan, “A learning- enabled neuron array IC based upon transistor channel models of biological phenomena,” IEEE Transactions on Biomedical Circuits and Systems, vol. 7, no. 1, pp. 71–81, 2013.  

[9] S. Kim, J. Hasler, and S. George, "Integrated Floating-Gate Programming Environment for System-Level Ics," IEEE Transactions on VLSI , 2016.   

[10]  J. Hasler, and A. Natarajan, “Continuous-time, Configurable Analog Linear System Solutions with Transconductance Amplifiers,” IEEE Circuits and Systems I, Vol. 68, no. 2, 2021. pp. 765-775. http://hasler.ece.gatech.edu/AnalogLinearSystemSolutionFeb2021.pdf

[11] J. Hasler and S. Shah, "An SoC FPAA Based Programmable, Ladder-Filter Based, Linear-Phase Analog Filter," IEEE CAS I, vol. 68, no. 2, 2021.

[12] R. Gary Leonard, and George Doddington. TIDIGITS LDC93S10. Web Download. Philadelphia: Linguistic Data Consortium, 1993.

[13] Warden, Pete. “Speech Commands: A Dataset for Limited-Vocabulary Speech Recognition.” ArXiv abs/1804.03209 (2018)

[14] D.W. Graham, Hasler, R. Chawla, and P.D. Smith, “A low-power, programmable bandpass filter section for higher-order filter applications,” IEEE Transactions on Circuits and Systems I, Vol. 54, no. 6, pp. 1165 - 1176, June 2007.

[15] J. Hasler, S. Kim, and A. Natarajan, “Enabling Energy-Efficient Physical Computing through Analog Abstraction and IP Reuse,” Journal of Low Power Electronics Applications, published December 2018. pp. 1-23.  

[16] J. Hasler,  ``Large-Scale Field Programmable Analog Arrays,''  IEEE Proceedings,  vol. 108. no. 8. August 2020.  pp. 1283-1302.

[17] J. Hasler and S. Shah, “An SoC FPAA based Programmamble, Ladder-Filter Based, Linear-Phase Analog Filter,” IEEE Circuits and Systems I, Vol. 68, no. 2, 2021.  pp 592-602.

[18] V. Srinivasan, G. J. Serrano, J. Gray, and Hasler, ``A precision CMOS amplifier using floating-gate transistors for offset cancellation,''IEEE Journal of Solid-State Circuits, vol. 42, no. 2, pp. 280-291, Feb. 2007.

[19] S. Shah, H. Toreyin, J. Hasler, and A. Natarajan, “Models and Techniques For Temperature Robust Systems On A Reconfigurable Platform,” Journal of Low Power Electronics Applications, vol. 7, no. 21,  August 2017. pp. 1-14.

[20] J. Hasler, B. Muldrey and  P. Hardy, “A CMOS Programmable Analog Standard Cell Library in Skywater 130nm Open-Source Process,” WOSET, Nov. 2021.


# Caravel Analog User

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![CI](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml) [![Caravan Build](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml)

---

| :exclamation: Important Note            |
|-----------------------------------------|

## Please fill in your project documentation in this README.md file 


:warning: | Use this sample project for analog user projects. 
:---: | :---

---

Refer to [README](docs/source/index.rst) for this sample project documentation. 
